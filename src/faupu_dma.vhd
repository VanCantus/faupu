library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_dma is
    generic (
        C_NUM_IOTABLE_ROWS          : integer   := 16;
        DATA_BUFFER_NUM_BUFFERS     : integer   := 2;
        C_PE_ID_WIDTH               : integer   := 16;
        C_NUM_PE_ARRAYS             : integer   := 2;

        -- Parameters of Axi Slave Bus Interface AXI_LITE
        C_AXI_LITE_ADDR_WIDTH       : integer   := 5;

        -- Parameters of Axi Master Bus Interface IOTABLE_FILL
        C_AXI_MASTER_BURST_LEN      : integer   := 16;
        C_AXI_MASTER_ID_WIDTH       : integer   := 1;
        C_AXI_MASTER_ADDR_WIDTH     : integer   := 32;
        C_AXI_MASTER_AWUSER_WIDTH   : integer   := 0;
        C_AXI_MASTER_ARUSER_WIDTH   : integer   := 0;
        C_AXI_MASTER_WUSER_WIDTH    : integer   := 0;
        C_AXI_MASTER_RUSER_WIDTH    : integer   := 0;
        C_AXI_MASTER_BUSER_WIDTH    : integer   := 0;

		-- Parameters of Axi Slave Bus Interface S_AXI_INTR
		C_S_AXI_INTR_DATA_WIDTH	    : integer	:= 32;
		C_S_AXI_INTR_ADDR_WIDTH	    : integer	:= 5;
		C_NUM_OF_INTR	            : integer	:= 1;
		C_INTR_SENSITIVITY	        : std_logic_vector	:= x"FFFFFFFF";
		C_INTR_ACTIVE_STATE	        : std_logic_vector	:= x"FFFFFFFF";
		C_IRQ_SENSITIVITY	        : integer	:= 1;
		C_IRQ_ACTIVE_STATE	        : integer	:= 1
    );
    port (
        axi_aclk         : in  std_logic;
        axi_aresetn      : in  std_logic;

        -- Ports of AXI Slave Bus Interface CONFIG (AXI Slave Lite)
        dma_config_awaddr       : in  std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
        dma_config_awprot       : in  std_logic_vector(2 downto 0);
        dma_config_awvalid      : in  std_logic;
        dma_config_awready      : out std_logic;
        dma_config_wdata        : in  std_logic_vector(31 downto 0);
        dma_config_wstrb        : in  std_logic_vector((32/8)-1 downto 0);
        dma_config_wvalid       : in  std_logic;
        dma_config_wready       : out std_logic;
        dma_config_bresp        : out std_logic_vector(1 downto 0);
        dma_config_bvalid       : out std_logic;
        dma_config_bready       : in  std_logic;
        dma_config_araddr       : in  std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
        dma_config_arprot       : in  std_logic_vector(2 downto 0);
        dma_config_arvalid      : in  std_logic;
        dma_config_arready      : out std_logic;
        dma_config_rdata        : out std_logic_vector(31 downto 0);
        dma_config_rresp        : out std_logic_vector(1 downto 0);
        dma_config_rvalid       : out std_logic;
        dma_config_rready       : in  std_logic;

        -- Ports of AXI Master Bus Interface IOTABLE_FILL (AXI Master Full)
        iotable_fill_init_axi_txn   : in  std_logic;
        iotable_fill_txn_done   : out std_logic;
        iotable_fill_error      : out std_logic;
        iotable_fill_awid       : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        iotable_fill_awaddr     : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
        iotable_fill_awlen      : out std_logic_vector(7 downto 0);
        iotable_fill_awsize     : out std_logic_vector(2 downto 0);
        iotable_fill_awburst    : out std_logic_vector(1 downto 0);
        iotable_fill_awlock     : out std_logic;
        iotable_fill_awcache    : out std_logic_vector(3 downto 0);
        iotable_fill_awprot     : out std_logic_vector(2 downto 0);
        iotable_fill_awqos      : out std_logic_vector(3 downto 0);
        iotable_fill_awuser     : out std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
        iotable_fill_awvalid    : out std_logic;
        iotable_fill_awready    : in  std_logic;
        iotable_fill_wid        : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        iotable_fill_wdata      : out std_logic_vector(31 downto 0);
        iotable_fill_wstrb      : out std_logic_vector((32/8)-1 downto 0);
        iotable_fill_wlast      : out std_logic;
        iotable_fill_wuser      : out std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
        iotable_fill_wvalid     : out std_logic;
        iotable_fill_wready     : in  std_logic;
        iotable_fill_bid        : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        iotable_fill_bresp      : in  std_logic_vector(1 downto 0);
        iotable_fill_buser      : in  std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
        iotable_fill_bvalid     : in  std_logic;
        iotable_fill_bready     : out std_logic;
        iotable_fill_arid       : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        iotable_fill_araddr     : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
        iotable_fill_arlen      : out std_logic_vector(7 downto 0);
        iotable_fill_arsize     : out std_logic_vector(2 downto 0);
        iotable_fill_arburst    : out std_logic_vector(1 downto 0);
        iotable_fill_arlock     : out std_logic;
        iotable_fill_arcache    : out std_logic_vector(3 downto 0);
        iotable_fill_arprot     : out std_logic_vector(2 downto 0);
        iotable_fill_arqos      : out std_logic_vector(3 downto 0);
        iotable_fill_aruser     : out std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
        iotable_fill_arvalid    : out std_logic;
        iotable_fill_arready    : in  std_logic;
        iotable_fill_rid        : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        iotable_fill_rdata      : in  std_logic_vector(31 downto 0);
        iotable_fill_rresp      : in  std_logic_vector(1 downto 0);
        iotable_fill_rlast      : in  std_logic;
        iotable_fill_ruser      : in  std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
        iotable_fill_rvalid     : in  std_logic;
        iotable_fill_rready     : out std_logic;

        -- Ports of AXI Master Bus Interface DATASTREAM (AXI Master Full)
        datastream_init_axi_txn : in  std_logic;
        datastream_txn_done     : out std_logic;
        datastream_error        : out std_logic;
        datastream_awid         : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        datastream_awaddr       : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
        datastream_awlen        : out std_logic_vector(7 downto 0);
        datastream_awsize       : out std_logic_vector(2 downto 0);
        datastream_awburst      : out std_logic_vector(1 downto 0);
        datastream_awlock       : out std_logic;
        datastream_awcache      : out std_logic_vector(3 downto 0);
        datastream_awprot       : out std_logic_vector(2 downto 0);
        datastream_awqos        : out std_logic_vector(3 downto 0);
        datastream_awuser       : out std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
        datastream_awvalid      : out std_logic;
        datastream_awready      : in  std_logic;
        datastream_wid          : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        datastream_wdata        : out std_logic_vector(31 downto 0);
        datastream_wstrb        : out std_logic_vector((32/8)-1 downto 0);
        datastream_wlast        : out std_logic;
        datastream_wuser        : out std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
        datastream_wvalid       : out std_logic;
        datastream_wready       : in  std_logic;
        datastream_bid          : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        datastream_bresp        : in  std_logic_vector(1 downto 0);
        datastream_buser        : in  std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
        datastream_bvalid       : in  std_logic;
        datastream_bready       : out std_logic;
        datastream_arid         : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        datastream_araddr       : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
        datastream_arlen        : out std_logic_vector(7 downto 0);
        datastream_arsize       : out std_logic_vector(2 downto 0);
        datastream_arburst      : out std_logic_vector(1 downto 0);
        datastream_arlock       : out std_logic;
        datastream_arcache      : out std_logic_vector(3 downto 0);
        datastream_arprot       : out std_logic_vector(2 downto 0);
        datastream_arqos        : out std_logic_vector(3 downto 0);
        datastream_aruser       : out std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
        datastream_arvalid      : out std_logic;
        datastream_arready      : in  std_logic;
        datastream_rid          : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
        datastream_rdata        : in  std_logic_vector(31 downto 0);
        datastream_rresp        : in  std_logic_vector(1 downto 0);
        datastream_rlast        : in  std_logic;
        datastream_ruser        : in  std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
        datastream_rvalid       : in  std_logic;
        datastream_rready       : out std_logic;

        -- Ports of AXI Lite Master Bus Interface pea_config
        pea_config_awaddr       : out std_logic_vector(31 downto 0);
        pea_config_awprot       : out std_logic_vector(2 downto 0);
        pea_config_awvalid      : out std_logic;
        pea_config_awready      : in  std_logic;
        pea_config_wdata        : out std_logic_vector(31 downto 0);
        pea_config_wstrb        : out std_logic_vector(32/8-1 downto 0);
        pea_config_wvalid       : out std_logic;
        pea_config_wready       : in  std_logic;
        pea_config_bresp        : in  std_logic_vector(1 downto 0);
        pea_config_bvalid       : in  std_logic;
        pea_config_bready       : out std_logic;
        pea_config_araddr       : out std_logic_vector(31 downto 0);
        pea_config_arprot       : out std_logic_vector(2 downto 0);
        pea_config_arvalid      : out std_logic;
        pea_config_arready      : in  std_logic;
        pea_config_rdata        : in  std_logic_vector(31 downto 0);
        pea_config_rresp        : in  std_logic_vector(1 downto 0);
        pea_config_rvalid       : in  std_logic;
        pea_config_rready       : out std_logic;

        -- Ports of AXI Master Bus Interface to_pea
        to_pea_tvalid           : out std_logic;
        to_pea_tdata            : out std_logic_vector(31 downto 0);
        to_pea_tstrb            : out std_logic_vector((32/8)-1 downto 0);
        to_pea_tlast            : out std_logic;
        to_pea_tready           : in  std_logic;
        to_pea_tdest            : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

        -- Ports of AXI Slave Bus Interface from_pea
        from_pea_tready         : out std_logic;
        from_pea_tdata          : in  std_logic_vector(31 downto 0);
        from_pea_tstrb          : in  std_logic_vector((32/8)-1 downto 0);
        from_pea_tlast          : in  std_logic;
        from_pea_tvalid         : in  std_logic;
        from_pea_tdest          : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

        -- Ports of Axi Slave Bus Interface S_AXI_INTR
        s_axi_intr_awaddr       : in  std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
        s_axi_intr_awprot       : in  std_logic_vector(2 downto 0);
        s_axi_intr_awvalid      : in  std_logic;
        s_axi_intr_awready      : out std_logic;
        s_axi_intr_wdata        : in  std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
        s_axi_intr_wstrb        : in  std_logic_vector((C_S_AXI_INTR_DATA_WIDTH/8)-1 downto 0);
        s_axi_intr_wvalid       : in  std_logic;
        s_axi_intr_wready       : out std_logic;
        s_axi_intr_bresp        : out std_logic_vector(1 downto 0);
        s_axi_intr_bvalid       : out std_logic;
        s_axi_intr_bready       : in  std_logic;
        s_axi_intr_araddr       : in  std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
        s_axi_intr_arprot       : in  std_logic_vector(2 downto 0);
        s_axi_intr_arvalid      : in  std_logic;
        s_axi_intr_arready      : out std_logic;
        s_axi_intr_rdata        : out std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
        s_axi_intr_rresp        : out std_logic_vector(1 downto 0);
        s_axi_intr_rvalid       : out std_logic;
        s_axi_intr_rready       : in  std_logic;
        irq                     : out std_logic
    );
end faupu_dma;

architecture arch_imp of faupu_dma is

    -- component declaration
    component faupu_dma_config is
        generic (
            C_S_AXI_DATA_WIDTH  : integer   := 32;
            C_S_AXI_ADDR_WIDTH  : integer   := 4
        );
        port (
            pea_debug_in        : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            dstr_debug_in0      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            dstr_debug_in1      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            read_enable         : out std_logic;
            read_start_addr     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            read_len            : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            read_ack            : in  std_logic;
            TXNS_DONE           : in  std_logic;
            TXNS_DONE_ACK       : out std_logic;
            debug_enable        : out std_logic;
            debug_addr          : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            debug_select        : out std_logic_vector((C_S_AXI_DATA_WIDTH/2)-1 downto 0);
            debug_read_sel      : out std_logic_vector(1 downto 0);
            axis_debug_sel      : out std_logic;
            dstr_debug_sel      : out std_logic_vector(1 downto 0);
            debug_data          : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_ACLK          : in  std_logic;
            S_AXI_ARESETN       : in  std_logic;
            S_AXI_AWADDR        : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_AWPROT        : in  std_logic_vector(2 downto 0);
            S_AXI_AWVALID       : in  std_logic;
            S_AXI_AWREADY       : out std_logic;
            S_AXI_WDATA         : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_WSTRB         : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
            S_AXI_WVALID        : in  std_logic;
            S_AXI_WREADY        : out std_logic;
            S_AXI_BRESP         : out std_logic_vector(1 downto 0);
            S_AXI_BVALID        : out std_logic;
            S_AXI_BREADY        : in  std_logic;
            S_AXI_ARADDR        : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_ARPROT        : in  std_logic_vector(2 downto 0);
            S_AXI_ARVALID       : in  std_logic;
            S_AXI_ARREADY       : out std_logic;
            S_AXI_RDATA         : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_RRESP         : out std_logic_vector(1 downto 0);
            S_AXI_RVALID        : out std_logic;
            S_AXI_RREADY        : in  std_logic
        );
    end component faupu_dma_config;

    component faupu_iotable_fill is
        generic (
            C_NUM_IOTABLE_ROWS      : integer   := 16;
            C_M_AXI_BURST_LEN       : integer   := 16;
            C_M_AXI_ID_WIDTH        : integer   := 1;
            C_M_AXI_ADDR_WIDTH      : integer   := 32;
            C_M_AXI_DATA_WIDTH      : integer   := 32;
            C_M_AXI_AWUSER_WIDTH    : integer   := 0;
            C_M_AXI_ARUSER_WIDTH    : integer   := 0;
            C_M_AXI_WUSER_WIDTH     : integer   := 0;
            C_M_AXI_RUSER_WIDTH     : integer   := 0;
            C_M_AXI_BUSER_WIDTH     : integer   := 0
        );
        port (
            iotable_filled      : out std_logic;
            iotable_filled_ack  : in  std_logic;
            table_txns_done     : in  std_logic;
            read_enable         : in  std_logic;
            read_ack            : out std_logic;
            read_start_addr     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            read_len            : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            table_write_enable  : out std_logic;
            table_write_addr    : out std_logic_vector(31 downto 0);
            table_write_data    : out std_logic_vector(31 downto 0);
            TXNS_DONE           : out std_logic;
            TXNS_DONE_ACK       : in  std_logic;
            M_AXI_ACLK          : in  std_logic;
            M_AXI_ARESETN       : in  std_logic;
            M_AXI_AWID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_AWADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_AWLEN         : out std_logic_vector(7 downto 0);
            M_AXI_AWSIZE        : out std_logic_vector(2 downto 0);
            M_AXI_AWBURST       : out std_logic_vector(1 downto 0);
            M_AXI_AWLOCK        : out std_logic;
            M_AXI_AWCACHE       : out std_logic_vector(3 downto 0);
            M_AXI_AWPROT        : out std_logic_vector(2 downto 0);
            M_AXI_AWQOS         : out std_logic_vector(3 downto 0);
            M_AXI_AWUSER        : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
            M_AXI_AWVALID       : out std_logic;
            M_AXI_AWREADY       : in std_logic;
            M_AXI_WID           : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_WDATA         : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_WSTRB         : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
            M_AXI_WLAST         : out std_logic;
            M_AXI_WUSER         : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
            M_AXI_WVALID        : out std_logic;
            M_AXI_WREADY        : in std_logic;
            M_AXI_BID           : in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_BRESP         : in std_logic_vector(1 downto 0);
            M_AXI_BUSER         : in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
            M_AXI_BVALID        : in std_logic;
            M_AXI_BREADY        : out std_logic;
            M_AXI_ARID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_ARADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_ARLEN         : out std_logic_vector(7 downto 0);
            M_AXI_ARSIZE        : out std_logic_vector(2 downto 0);
            M_AXI_ARBURST       : out std_logic_vector(1 downto 0);
            M_AXI_ARLOCK        : out std_logic;
            M_AXI_ARCACHE       : out std_logic_vector(3 downto 0);
            M_AXI_ARPROT        : out std_logic_vector(2 downto 0);
            M_AXI_ARQOS         : out std_logic_vector(3 downto 0);
            M_AXI_ARUSER        : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
            M_AXI_ARVALID       : out std_logic;
            M_AXI_ARREADY       : in std_logic;
            M_AXI_RID           : in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_RDATA         : in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_RRESP         : in std_logic_vector(1 downto 0);
            M_AXI_RLAST         : in std_logic;
            M_AXI_RUSER         : in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
            M_AXI_RVALID        : in std_logic;
            M_AXI_RREADY        : out std_logic
        );
    end component faupu_iotable_fill;

    component faupu_datastream is
        generic (
            C_BUFFER_SELECT_LENGTH      : integer   := 1;
            C_NUM_BUFFERS               : integer   := 2;
            C_BUFFER_ADDR_WIDTH         : integer   := 16;
            C_NUM_PE_ARRAYS             : integer   := 2;
            C_NUM_IOTABLE_ROWS          : integer   := 16;
            C_M_AXI_BURST_LEN           : integer   := 16;
            C_M_AXI_ID_WIDTH            : integer   := 1;
            C_M_AXI_ADDR_WIDTH          : integer   := 32;
            C_M_AXI_DATA_WIDTH          : integer   := 32;
            C_M_AXI_AWUSER_WIDTH        : integer   := 0;
            C_M_AXI_ARUSER_WIDTH        : integer   := 0;
            C_M_AXI_WUSER_WIDTH         : integer   := 0;
            C_M_AXI_RUSER_WIDTH         : integer   := 0;
            C_M_AXI_BUSER_WIDTH         : integer   := 0
        );
        port (
            debug_select                : in  std_logic_vector(1 downto 0);
            debug_addr                  : in  std_logic_vector(31 downto 0);
            debug_out0                  : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            debug_out1                  : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            pea_config_write_addr       : out std_logic_vector(31 downto 0);
            pea_config_write_data       : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            pea_config_write_init_txn   : out std_logic;
            pea_config_write_txn_done   : in  std_logic;
            pea_config_read_addr        : out std_logic_vector(31 downto 0);
            pea_config_read_data        : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            pea_config_read_init_txn    : out std_logic;
            pea_config_read_txn_done    : in  std_logic;
            iotable_filled              : in  std_logic;
            iotable_filled_ack          : out std_logic;
            read_txns_done              : out std_logic;
            write_buffer_ready          : in  std_logic;
            read_buffer_ready           : out std_logic;
            write_buffer_free           : out std_logic;
            read_buffer_free            : in  std_logic;
            write_buffer_select         : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            write_buffer_addr           : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            write_buffer_data           : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            write_buffer_pe_id          : in  std_logic_vector(15 downto 0); -- TODO: length of the signal
            write_buffer_length         : in  std_logic_vector(31 downto 0);
            write_buffer_last           : in  std_logic;
            read_buffer_select          : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            read_buffer_addr            : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            read_buffer_data            : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            read_buffer_pe_id           : out std_logic_vector(15 downto 0); -- TODO: length of the signal
            read_buffer_length          : out std_logic_vector(31 downto 0);
            read_buffer_prog            : out std_logic;
            read_buffer_last            : out std_logic;
            read_buffer_wren            : out std_logic;
            iotable_row                 : out std_logic_vector(28 downto 0);
            CONFIG                      : in  std_logic_vector(31 downto 0);
            R_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
            R_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
            R_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);
            W_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
            W_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
            W_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);
            M_AXI_ACLK                  : in  std_logic;
            M_AXI_ARESETN               : in  std_logic;
            M_AXI_AWID                  : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_AWADDR                : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_AWLEN                 : out std_logic_vector(7 downto 0);
            M_AXI_AWSIZE                : out std_logic_vector(2 downto 0);
            M_AXI_AWBURST               : out std_logic_vector(1 downto 0);
            M_AXI_AWLOCK                : out std_logic;
            M_AXI_AWCACHE               : out std_logic_vector(3 downto 0);
            M_AXI_AWPROT                : out std_logic_vector(2 downto 0);
            M_AXI_AWQOS                 : out std_logic_vector(3 downto 0);
            M_AXI_AWUSER                : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
            M_AXI_AWVALID               : out std_logic;
            M_AXI_AWREADY               : in  std_logic;
            M_AXI_WID                   : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_WDATA                 : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_WSTRB                 : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
            M_AXI_WLAST                 : out std_logic;
            M_AXI_WUSER                 : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
            M_AXI_WVALID                : out std_logic;
            M_AXI_WREADY                : in  std_logic;
            M_AXI_BID                   : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_BRESP                 : in  std_logic_vector(1 downto 0);
            M_AXI_BUSER                 : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
            M_AXI_BVALID                : in  std_logic;
            M_AXI_BREADY                : out std_logic;
            M_AXI_ARID                  : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_ARADDR                : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_ARLEN                 : out std_logic_vector(7 downto 0);
            M_AXI_ARSIZE                : out std_logic_vector(2 downto 0);
            M_AXI_ARBURST               : out std_logic_vector(1 downto 0);
            M_AXI_ARLOCK                : out std_logic;
            M_AXI_ARCACHE               : out std_logic_vector(3 downto 0);
            M_AXI_ARPROT                : out std_logic_vector(2 downto 0);
            M_AXI_ARQOS                 : out std_logic_vector(3 downto 0);
            M_AXI_ARUSER                : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
            M_AXI_ARVALID               : out std_logic;
            M_AXI_ARREADY               : in  std_logic;
            M_AXI_RID                   : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_RDATA                 : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_RRESP                 : in  std_logic_vector(1 downto 0);
            M_AXI_RLAST                 : in  std_logic;
            M_AXI_RUSER                 : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
            M_AXI_RVALID                : in  std_logic;
            M_AXI_RREADY                : out std_logic
        );
    end component faupu_datastream;

    component faupu_pea_config is
        generic (
            C_M_AXI_ADDR_WIDTH  : integer   := 32;
            C_M_AXI_DATA_WIDTH  : integer   := 32
        );
        port (
            write_addr      : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            write_data      : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            read_addr       : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            read_data       : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            write_init_txn  : in  std_logic;
            read_init_txn   : in  std_logic;
            ERROR           : out std_logic;
            write_txn_done  : out std_logic;
            read_txn_done   : out std_logic;
            M_AXI_ACLK      : in  std_logic;
            M_AXI_ARESETN   : in  std_logic;
            M_AXI_AWADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_AWPROT    : out std_logic_vector(2 downto 0);
            M_AXI_AWVALID   : out std_logic;
            M_AXI_AWREADY   : in  std_logic;
            M_AXI_WDATA     : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_WSTRB     : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
            M_AXI_WVALID    : out std_logic;
            M_AXI_WREADY    : in  std_logic;
            M_AXI_BRESP     : in  std_logic_vector(1 downto 0);
            M_AXI_BVALID    : in  std_logic;
            M_AXI_BREADY    : out std_logic;
            M_AXI_ARADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_ARPROT    : out std_logic_vector(2 downto 0);
            M_AXI_ARVALID   : out std_logic;
            M_AXI_ARREADY   : in  std_logic;
            M_AXI_RDATA     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_RRESP     : in  std_logic_vector(1 downto 0);
            M_AXI_RVALID    : in  std_logic;
            M_AXI_RREADY    : out std_logic
        );
    end component faupu_pea_config;

    component faupu_axis_to_pea is
        generic (
            C_BUFFER_SELECT_LENGTH  : integer   := 1;
            C_NUM_BUFFERS           : integer   := 2;
            C_BUFFER_ADDR_WIDTH     : integer   := 4;
            C_BURST_LEN             : integer   := 16;
            C_PE_ID_WIDTH           : integer   := 16;
            C_NUM_PE_ARRAYS         : integer   := 2;
            C_M_AXIS_TDATA_WIDTH    : integer   := 32
        );
        port (
            debug_out           : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
            read_buffer_ready   : in  std_logic;
            read_buffer_free    : out std_logic;
            read_buffer_select  : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            read_buffer_addr    : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            read_buffer_data    : in  std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
            read_buffer_pe_id   : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0);
            read_buffer_length  : in  std_logic_vector(31 downto 0);
            read_buffer_prog    : in  std_logic;
            read_buffer_last    : in  std_logic;
            M_AXIS_ACLK         : in  std_logic;
            M_AXIS_ARESETN      : in  std_logic;
            M_AXIS_TVALID       : out std_logic;
            M_AXIS_TDATA        : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
            M_AXIS_TSTRB        : out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
            M_AXIS_TLAST        : out std_logic;
            M_AXIS_TREADY       : in  std_logic;
            M_AXIS_TDEST        : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0)
        );
    end component faupu_axis_to_pea;

    component faupu_axis_from_pea is
        generic (
            C_BUFFER_SELECT_LENGTH  : integer   := 1;
            C_NUM_BUFFERS           : integer   := 2;
            C_BUFFER_ADDR_WIDTH     : integer   := 4;
            C_BURST_LEN             : integer   := 16;
            C_PE_ID_WIDTH           : integer   := 16;
            C_S_AXIS_TDATA_WIDTH    : integer   := 32
        );
        port (
            debug_out           : out std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
            write_buffer_ready  : out std_logic;
            write_buffer_free   : in  std_logic;
            write_buffer_select : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            write_buffer_addr   : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            write_buffer_data   : out std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
            write_buffer_pe_id  : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0);
            write_buffer_length : out std_logic_vector(31 downto 0);
            write_buffer_last   : out std_logic;
            write_buffer_wren   : out std_logic;
            S_AXIS_ACLK         : in  std_logic;
            S_AXIS_ARESETN      : in  std_logic;
            S_AXIS_TREADY       : out std_logic;
            S_AXIS_TDATA        : in  std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
            S_AXIS_TSTRB        : in  std_logic_vector((C_S_AXIS_TDATA_WIDTH/8)-1 downto 0);
            S_AXIS_TLAST        : in  std_logic;
            S_AXIS_TVALID       : in  std_logic;
            S_AXIS_TDEST        : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0)
        );
    end component faupu_axis_from_pea;

    component inout_table is
        generic(
            C_ROWS          : integer   := 16
        );
        port(
            -- the clock
            clk             : in  std_logic;

            -- reset (active low)
            resetn          : in  std_logic;

            -- if we = '1', then the table will be filled
            we              : in  std_logic;
            ADDR_IN         : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
            DATA_IN         : in  std_logic_vector(31 downto 0);

            -- if re = '1', data will be read from the table
            re              : in  std_logic;
            ADDR_OUT        : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
            DATA_OUT        : out std_logic_vector(31 downto 0);

            -- the row of the table entries, that should be read
            ROW             : in  std_logic_vector(28 downto 0);

            -- output of the table row that is refered to with ROW
            CONFIG                      : out std_logic_vector(31 downto 0);
            R_PARTITION_START_ADDR      : out std_logic_vector(31 downto 0);
            R_IMAGE_WIDTH               : out std_logic_vector(31 downto 0);
            R_PARTITION_DIMENSIONS      : out std_logic_vector(31 downto 0);
            W_PARTITION_START_ADDR      : out std_logic_vector(31 downto 0);
            W_IMAGE_WIDTH               : out std_logic_vector(31 downto 0);
            W_PARTITION_DIMENSIONS      : out std_logic_vector(31 downto 0)
        );
    end component inout_table;

    component multi_buffer is
        generic (
            DATA_WIDTH              : integer   := 32;
            ADDR_WIDTH              : integer   := 4;
            BUFFER_SELECT_LENGTH    : integer   := 1;
            NUM_BUFFERS             : integer   := 2
        );
        port (
            clk             : in  std_logic;
            resetn          : in  std_logic;

            -- BRAM Port A
            a_buffer_select : in  std_logic_vector(BUFFER_SELECT_LENGTH-1 downto 0);
            a_pe_arr_id_in  : in  std_logic_vector(15 downto 0);
            a_length_in     : in  std_logic_vector(31 downto 0);
            a_prog_in       : in  std_logic;
            a_last_in       : in  std_logic;
            a_wr            : in  std_logic;
            a_addr          : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            a_din           : in  std_logic_vector(DATA_WIDTH-1 downto 0);

            -- BRAM Port B
            b_buffer_select : in  std_logic_vector(BUFFER_SELECT_LENGTH-1 downto 0);
            b_addr          : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            b_dout          : out std_logic_vector(DATA_WIDTH-1 downto 0);
            b_pe_arr_id_out : out std_logic_vector(15 downto 0);
            b_length_out    : out std_logic_vector(31 downto 0);
            b_prog_out      : out std_logic;
            b_last_out      : out std_logic
        );
    end component multi_buffer;

    component pea_datamover is
        generic (
            C_BUFFER_SELECT_LENGTH      : integer   := 1;
            C_NUM_BUFFERS               : integer   := 2;
            C_BUFFER_ADDR_WIDTH         : integer   := 4;
            C_BURST_LEN                 : integer   := 16;
            C_DATA_WIDTH                : integer   := 32
        );
        port (
            debug_out                   : out std_logic_vector(C_DATA_WIDTH-1 downto 0);

            axi_aclk                    : in  std_logic;
            axi_aresetn                 : in  std_logic;

            write_buffer_ready          : out std_logic;
            read_buffer_ready           : in  std_logic;
            write_buffer_free           : in  std_logic;
            read_buffer_free            : out std_logic;

            write_buffer_select         : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            write_buffer_addr           : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            write_buffer_data           : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
            write_buffer_pe_id          : out std_logic_vector(15 downto 0);
            write_buffer_length         : out std_logic_vector(31 downto 0);
            write_buffer_last           : out std_logic;
            write_buffer_wren           : out std_logic;

            read_buffer_select          : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
            read_buffer_addr            : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
            read_buffer_data            : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
            read_buffer_pe_id           : in  std_logic_vector(15 downto 0);
            read_buffer_length          : in  std_logic_vector(31 downto 0);
            read_buffer_prog            : in  std_logic;
            read_buffer_last            : in  std_logic;
            read_buffer_rden            : out std_logic
        );
    end component pea_datamover;

    component faupu_dma_irq_v1_0_S_AXI_INTR is
        generic (
            C_S_AXI_DATA_WIDTH  : integer           := 32;
            C_S_AXI_ADDR_WIDTH  : integer           := 5;
            C_NUM_OF_INTR       : integer           := 1;
            C_INTR_SENSITIVITY  : std_logic_vector  := x"FFFFFFFF";
            C_INTR_ACTIVE_STATE : std_logic_vector  := x"FFFFFFFF";
            C_IRQ_SENSITIVITY   : integer           := 1;
            C_IRQ_ACTIVE_STATE  : integer           := 1
        );
        port (
            table_txns_done : in  std_logic;
            S_AXI_ACLK      : in  std_logic;
            S_AXI_ARESETN   : in  std_logic;
            S_AXI_AWADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_AWPROT    : in  std_logic_vector(2 downto 0);
            S_AXI_AWVALID   : in  std_logic;
            S_AXI_AWREADY   : out std_logic;
            S_AXI_WDATA     : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_WSTRB     : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
            S_AXI_WVALID    : in  std_logic;
            S_AXI_WREADY    : out std_logic;
            S_AXI_BRESP     : out std_logic_vector(1 downto 0);
            S_AXI_BVALID    : out std_logic;
            S_AXI_BREADY    : in  std_logic;
            S_AXI_ARADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_ARPROT    : in  std_logic_vector(2 downto 0);
            S_AXI_ARVALID   : in  std_logic;
            S_AXI_ARREADY   : out std_logic;
            S_AXI_RDATA     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_RRESP     : out std_logic_vector(1 downto 0);
            S_AXI_RVALID    : out std_logic;
            S_AXI_RREADY    : in  std_logic;
            irq             : out std_logic
        );
    end component faupu_dma_irq_v1_0_S_AXI_INTR;

    constant TABLE_ADDR_WIDTH           : integer   := log2(C_NUM_IOTABLE_ROWS*8*4);
    constant C_AXI_LITE_DATA_WIDTH      : integer   := 32;
    constant C_AXI_MASTER_DATA_WIDTH    : integer   := 32;

    signal dma_config_debug_enable          : std_logic;
    signal dma_config_debug_addr            : std_logic_vector(C_AXI_LITE_DATA_WIDTH-1 downto 0);
    signal dma_config_debug_select          : std_logic_vector((C_AXI_LITE_DATA_WIDTH/2)-1 downto 0);
    signal dma_config_debug_read_sel        : std_logic_vector(1 downto 0);
    signal dma_config_axis_debug_sel        : std_logic;
    signal dma_config_dstr_debug_sel        : std_logic_vector(1 downto 0);
    signal dma_config_debug_data            : std_logic_vector(C_AXI_LITE_DATA_WIDTH-1 downto 0);

    signal iotable_fill_read_enable         : std_logic;
    signal iotable_fill_read_start_addr     : std_logic_vector(C_AXI_LITE_DATA_WIDTH-1 downto 0);
    signal iotable_fill_read_len            : std_logic_vector(C_AXI_LITE_DATA_WIDTH-1 downto 0);
    signal iotable_fill_iotable_filled      : std_logic;
    signal iotable_fill_iotable_filled_ack  : std_logic;
    signal iotable_fill_read_ack            : std_logic;
    signal iotable_fill_table_txns_done     : std_logic;
    signal iotable_fill_table_write_enable  : std_logic;
    signal iotable_fill_table_write_addr    : std_logic_vector(31 downto 0);
    signal iotable_fill_table_write_data    : std_logic_vector(31 downto 0);

    signal iotab_we                         : std_logic;
    signal iotab_addr_in                    : std_logic_vector(TABLE_ADDR_WIDTH-1 downto 0);
    signal iotab_data_in                    : std_logic_vector(31 downto 0);
    signal iotab_re                         : std_logic;
    signal iotab_addr_out                   : std_logic_vector(TABLE_ADDR_WIDTH-1 downto 0);
    signal iotab_data_out                   : std_logic_vector(31 downto 0);
    signal iotab_row                        : std_logic_vector(28 downto 0);
    signal iotab_config                     : std_logic_vector(31 downto 0);
    signal iotab_r_partition_start_addr     : std_logic_vector(31 downto 0);
    signal iotab_r_image_width              : std_logic_vector(31 downto 0);
    signal iotab_r_partition_dimensions     : std_logic_vector(31 downto 0);
    signal iotab_w_partition_start_addr     : std_logic_vector(31 downto 0);
    signal iotab_w_image_width              : std_logic_vector(31 downto 0);
    signal iotab_w_partition_dimensions     : std_logic_vector(31 downto 0);

    constant DATA_BUFFER_ADDR_WIDTH         : integer := clogb2(C_AXI_MASTER_BURST_LEN-1); -- amount of burst data words
    constant DATA_BUFFER_SELECT_LENGTH      : integer := clogb2(DATA_BUFFER_NUM_BUFFERS);

    signal dbaw : integer;
    signal dbsl : integer;

    -- read and write data buffer signals
    signal read_data_buffer_a_buffer_select     : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal read_data_buffer_a_pe_arr_id_in      : std_logic_vector(15 downto 0);
    signal read_data_buffer_a_length_in         : std_logic_vector(31 downto 0);
    signal read_data_buffer_a_prog_in           : std_logic;
    signal read_data_buffer_a_last_in           : std_logic;
    signal read_data_buffer_a_wr                : std_logic;
    signal read_data_buffer_a_addr              : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal read_data_buffer_a_din               : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal read_data_buffer_b_buffer_select     : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal buf_rdb_b_buffer_select              : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal read_data_buffer_b_addr              : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal buf_rdb_b_addr                       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal read_data_buffer_b_dout              : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal read_data_buffer_b_pe_arr_id_out     : std_logic_vector(15 downto 0);
    signal read_data_buffer_b_length_out        : std_logic_vector(31 downto 0);
    signal read_data_buffer_b_prog_out          : std_logic;
    signal read_data_buffer_b_last_out          : std_logic;
    signal write_data_buffer_a_buffer_select    : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal buf_wdb_a_buffer_select              : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal write_data_buffer_a_addr             : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal buf_wdb_a_addr                       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal write_data_buffer_a_dout             : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal write_data_buffer_a_pe_arr_id_out    : std_logic_vector(15 downto 0);
    signal write_data_buffer_a_length_out       : std_logic_vector(31 downto 0);
    signal write_data_buffer_a_last_out         : std_logic;
    signal write_data_buffer_b_buffer_select    : std_logic_vector(DATA_BUFFER_SELECT_LENGTH-1 downto 0);
    signal write_data_buffer_b_pe_arr_id_in     : std_logic_vector(15 downto 0);
    signal write_data_buffer_b_length_in        : std_logic_vector(31 downto 0);
    signal write_data_buffer_b_last_in          : std_logic;
    signal write_data_buffer_b_wr               : std_logic;
    signal write_data_buffer_b_addr             : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal write_data_buffer_b_din              : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);

    signal write_buffer_ready                   : std_logic;
    signal read_buffer_ready                    : std_logic;
    signal write_buffer_free                    : std_logic;
    signal read_buffer_free                     : std_logic;

    signal datastream_debug_out0                : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal datastream_debug_out1                : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal pea_dm_debug_out                     : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal axis_to_pea_debug_out                : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal axis_from_pea_debug_out              : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal txns_done_debug                      : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);

    signal pea_config_write_addr                : std_logic_vector(31 downto 0);
    signal pea_config_write_data                : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal pea_config_write_init_txn            : std_logic;
    signal pea_config_write_txn_done            : std_logic;
    signal pea_config_read_addr                 : std_logic_vector(31 downto 0);
    signal pea_config_read_data                 : std_logic_vector(C_AXI_MASTER_DATA_WIDTH-1 downto 0);
    signal pea_config_read_init_txn             : std_logic;
    signal pea_config_read_txn_done             : std_logic;

    signal TXNS_DONE_BUF                        : std_logic;
    signal TXNS_DONE_ACK                        : std_logic;

begin

    dbaw <= DATA_BUFFER_ADDR_WIDTH;
    dbsl <= DATA_BUFFER_SELECT_LENGTH;

    --TXNS_DONE <= TXNS_DONE_BUF;

    -- TODO: remove
    datastream_txn_done     <= '0';
    datastream_error        <= '0';
    iotable_fill_txn_done   <= '0';
    iotable_fill_error      <= '0';

    pea_dm_debug_out <= axis_to_pea_debug_out when dma_config_axis_debug_sel = '0' else axis_from_pea_debug_out;
    txns_done_debug  <= (others => TXNS_DONE_BUF);

    -- Instantiation of Axi Bus Interface FAUPU_CONFIG (AXI Slave Lite)
    FAUPU_CONFIG_INST: faupu_dma_config
        generic map (
            C_S_AXI_DATA_WIDTH  => C_AXI_LITE_DATA_WIDTH,
            C_S_AXI_ADDR_WIDTH  => C_AXI_LITE_ADDR_WIDTH
        )
        port map (
            pea_debug_in        => pea_dm_debug_out,
            dstr_debug_in0      => datastream_debug_out0,
            dstr_debug_in1      => txns_done_debug, --datastream_debug_out1,
            read_enable         => iotable_fill_read_enable,
            read_start_addr     => iotable_fill_read_start_addr,
            read_len            => iotable_fill_read_len,
            read_ack            => iotable_fill_read_ack,
            TXNS_DONE           => TXNS_DONE_BUF,
            TXNS_DONE_ACK       => TXNS_DONE_ACK,
            debug_enable        => dma_config_debug_enable,
            debug_addr          => dma_config_debug_addr,
            debug_select        => dma_config_debug_select,
            debug_read_sel      => dma_config_debug_read_sel,
            axis_debug_sel      => dma_config_axis_debug_sel,
            dstr_debug_sel      => dma_config_dstr_debug_sel,
            debug_data          => dma_config_debug_data,
            S_AXI_ACLK          => axi_aclk,
            S_AXI_ARESETN       => axi_aresetn,
            S_AXI_AWADDR        => dma_config_awaddr,
            S_AXI_AWPROT        => dma_config_awprot,
            S_AXI_AWVALID       => dma_config_awvalid,
            S_AXI_AWREADY       => dma_config_awready,
            S_AXI_WDATA         => dma_config_wdata,
            S_AXI_WSTRB         => dma_config_wstrb,
            S_AXI_WVALID        => dma_config_wvalid,
            S_AXI_WREADY        => dma_config_wready,
            S_AXI_BRESP         => dma_config_bresp,
            S_AXI_BVALID        => dma_config_bvalid,
            S_AXI_BREADY        => dma_config_bready,
            S_AXI_ARADDR        => dma_config_araddr,
            S_AXI_ARPROT        => dma_config_arprot,
            S_AXI_ARVALID       => dma_config_arvalid,
            S_AXI_ARREADY       => dma_config_arready,
            S_AXI_RDATA         => dma_config_rdata,
            S_AXI_RRESP         => dma_config_rresp,
            S_AXI_RVALID        => dma_config_rvalid,
            S_AXI_RREADY        => dma_config_rready
        );

    -- Instantiation of Axi Bus Interface IOTABLE_FILL (AXI Master Full)
    IOTABLE_FILL_INST: faupu_iotable_fill
        generic map (
            C_NUM_IOTABLE_ROWS      => C_NUM_IOTABLE_ROWS,
            C_M_AXI_BURST_LEN       => C_AXI_MASTER_BURST_LEN,
            C_M_AXI_ID_WIDTH        => C_AXI_MASTER_ID_WIDTH,
            C_M_AXI_ADDR_WIDTH      => C_AXI_MASTER_ADDR_WIDTH,
            C_M_AXI_DATA_WIDTH      => C_AXI_MASTER_DATA_WIDTH,
            C_M_AXI_AWUSER_WIDTH    => C_AXI_MASTER_AWUSER_WIDTH,
            C_M_AXI_ARUSER_WIDTH    => C_AXI_MASTER_ARUSER_WIDTH,
            C_M_AXI_WUSER_WIDTH     => C_AXI_MASTER_WUSER_WIDTH,
            C_M_AXI_RUSER_WIDTH     => C_AXI_MASTER_RUSER_WIDTH,
            C_M_AXI_BUSER_WIDTH     => C_AXI_MASTER_BUSER_WIDTH
        )
        port map (
            iotable_filled      => iotable_fill_iotable_filled,
            iotable_filled_ack  => iotable_fill_iotable_filled_ack,
            table_txns_done     => iotable_fill_table_txns_done,
            read_enable         => iotable_fill_read_enable,
            read_ack            => iotable_fill_read_ack,
            read_start_addr     => iotable_fill_read_start_addr,
            read_len            => iotable_fill_read_len,
            table_write_enable  => iotable_fill_table_write_enable,
            table_write_addr    => iotable_fill_table_write_addr,
            table_write_data    => iotable_fill_table_write_data,
            TXNS_DONE           => TXNS_DONE_BUF,
            TXNS_DONE_ACK       => TXNS_DONE_ACK,
            M_AXI_ACLK          => axi_aclk,
            M_AXI_ARESETN       => axi_aresetn,
            M_AXI_AWID          => iotable_fill_awid,
            M_AXI_AWADDR        => iotable_fill_awaddr,
            M_AXI_AWLEN         => iotable_fill_awlen,
            M_AXI_AWSIZE        => iotable_fill_awsize,
            M_AXI_AWBURST       => iotable_fill_awburst,
            M_AXI_AWLOCK        => iotable_fill_awlock,
            M_AXI_AWCACHE       => iotable_fill_awcache,
            M_AXI_AWPROT        => iotable_fill_awprot,
            M_AXI_AWQOS         => iotable_fill_awqos,
            M_AXI_AWUSER        => iotable_fill_awuser,
            M_AXI_AWVALID       => iotable_fill_awvalid,
            M_AXI_AWREADY       => iotable_fill_awready,
            M_AXI_WID           => iotable_fill_wid,
            M_AXI_WDATA         => iotable_fill_wdata,
            M_AXI_WSTRB         => iotable_fill_wstrb,
            M_AXI_WLAST         => iotable_fill_wlast,
            M_AXI_WUSER         => iotable_fill_wuser,
            M_AXI_WVALID        => iotable_fill_wvalid,
            M_AXI_WREADY        => iotable_fill_wready,
            M_AXI_BID           => iotable_fill_bid,
            M_AXI_BRESP         => iotable_fill_bresp,
            M_AXI_BUSER         => iotable_fill_buser,
            M_AXI_BVALID        => iotable_fill_bvalid,
            M_AXI_BREADY        => iotable_fill_bready,
            M_AXI_ARID          => iotable_fill_arid,
            M_AXI_ARADDR        => iotable_fill_araddr,
            M_AXI_ARLEN         => iotable_fill_arlen,
            M_AXI_ARSIZE        => iotable_fill_arsize,
            M_AXI_ARBURST       => iotable_fill_arburst,
            M_AXI_ARLOCK        => iotable_fill_arlock,
            M_AXI_ARCACHE       => iotable_fill_arcache,
            M_AXI_ARPROT        => iotable_fill_arprot,
            M_AXI_ARQOS         => iotable_fill_arqos,
            M_AXI_ARUSER        => iotable_fill_aruser,
            M_AXI_ARVALID       => iotable_fill_arvalid,
            M_AXI_ARREADY       => iotable_fill_arready,
            M_AXI_RID           => iotable_fill_rid,
            M_AXI_RDATA         => iotable_fill_rdata,
            M_AXI_RRESP         => iotable_fill_rresp,
            M_AXI_RLAST         => iotable_fill_rlast,
            M_AXI_RUSER         => iotable_fill_ruser,
            M_AXI_RVALID        => iotable_fill_rvalid,
            M_AXI_RREADY        => iotable_fill_rready
        );


    DATASTREAM_INST: faupu_datastream
        generic map (
            C_BUFFER_SELECT_LENGTH      => DATA_BUFFER_SELECT_LENGTH,
            C_NUM_BUFFERS               => DATA_BUFFER_NUM_BUFFERS,
            C_BUFFER_ADDR_WIDTH         => DATA_BUFFER_ADDR_WIDTH,
            C_NUM_PE_ARRAYS             => C_NUM_PE_ARRAYS,
            C_NUM_IOTABLE_ROWS          => C_NUM_IOTABLE_ROWS,
            C_M_AXI_BURST_LEN           => C_AXI_MASTER_BURST_LEN,
            C_M_AXI_ID_WIDTH            => C_AXI_MASTER_ID_WIDTH,
            C_M_AXI_ADDR_WIDTH          => C_AXI_MASTER_ADDR_WIDTH,
            C_M_AXI_DATA_WIDTH          => C_AXI_MASTER_DATA_WIDTH,
            C_M_AXI_AWUSER_WIDTH        => C_AXI_MASTER_AWUSER_WIDTH,
            C_M_AXI_ARUSER_WIDTH        => C_AXI_MASTER_ARUSER_WIDTH,
            C_M_AXI_WUSER_WIDTH         => C_AXI_MASTER_WUSER_WIDTH,
            C_M_AXI_RUSER_WIDTH         => C_AXI_MASTER_RUSER_WIDTH,
            C_M_AXI_BUSER_WIDTH         => C_AXI_MASTER_BUSER_WIDTH
        )
        port map (
            debug_select                => dma_config_dstr_debug_sel,
            debug_addr                  => dma_config_debug_addr,
            debug_out0                  => datastream_debug_out0,
            debug_out1                  => datastream_debug_out1,
            pea_config_write_addr       => pea_config_write_addr,
            pea_config_write_data       => pea_config_write_data,
            pea_config_write_init_txn   => pea_config_write_init_txn,
            pea_config_write_txn_done   => pea_config_write_txn_done,
            pea_config_read_addr        => pea_config_read_addr,
            pea_config_read_data        => pea_config_read_data,
            pea_config_read_init_txn    => pea_config_read_init_txn,
            pea_config_read_txn_done    => pea_config_read_txn_done,
            iotable_filled              => iotable_fill_iotable_filled,
            iotable_filled_ack          => iotable_fill_iotable_filled_ack,
            read_txns_done              => iotable_fill_table_txns_done,
            write_buffer_ready          => write_buffer_ready,
            read_buffer_ready           => read_buffer_ready,
            write_buffer_free           => write_buffer_free,
            read_buffer_free            => read_buffer_free,
            write_buffer_select         => write_data_buffer_a_buffer_select,
            write_buffer_addr           => write_data_buffer_a_addr,
            write_buffer_data           => write_data_buffer_a_dout,
            write_buffer_pe_id          => write_data_buffer_a_pe_arr_id_out,
            write_buffer_length         => write_data_buffer_a_length_out,
            write_buffer_last           => write_data_buffer_a_last_out,
            read_buffer_select          => read_data_buffer_a_buffer_select,
            read_buffer_addr            => read_data_buffer_a_addr,
            read_buffer_data            => read_data_buffer_a_din,
            read_buffer_pe_id           => read_data_buffer_a_pe_arr_id_in,
            read_buffer_length          => read_data_buffer_a_length_in,
            read_buffer_prog            => read_data_buffer_a_prog_in,
            read_buffer_last            => read_data_buffer_a_last_in,
            read_buffer_wren            => read_data_buffer_a_wr,
            iotable_row                 => iotab_row,
            CONFIG                      => iotab_config,
            R_PARTITION_START_ADDR      => iotab_r_partition_start_addr,
            R_IMAGE_WIDTH               => iotab_r_image_width,
            R_PARTITION_DIMENSIONS      => iotab_r_partition_dimensions,
            W_PARTITION_START_ADDR      => iotab_w_partition_start_addr,
            W_IMAGE_WIDTH               => iotab_w_image_width,
            W_PARTITION_DIMENSIONS      => iotab_w_partition_dimensions,
            M_AXI_ACLK                  => axi_aclk,
            M_AXI_ARESETN               => axi_aresetn,
            M_AXI_AWID                  => datastream_awid,
            M_AXI_AWADDR                => datastream_awaddr,
            M_AXI_AWLEN                 => datastream_awlen,
            M_AXI_AWSIZE                => datastream_awsize,
            M_AXI_AWBURST               => datastream_awburst,
            M_AXI_AWLOCK                => datastream_awlock,
            M_AXI_AWCACHE               => datastream_awcache,
            M_AXI_AWPROT                => datastream_awprot,
            M_AXI_AWQOS                 => datastream_awqos,
            M_AXI_AWUSER                => datastream_awuser,
            M_AXI_AWVALID               => datastream_awvalid,
            M_AXI_AWREADY               => datastream_awready,
            M_AXI_WID                   => datastream_wid,
            M_AXI_WDATA                 => datastream_wdata,
            M_AXI_WSTRB                 => datastream_wstrb,
            M_AXI_WLAST                 => datastream_wlast,
            M_AXI_WUSER                 => datastream_wuser,
            M_AXI_WVALID                => datastream_wvalid,
            M_AXI_WREADY                => datastream_wready,
            M_AXI_BID                   => datastream_bid,
            M_AXI_BRESP                 => datastream_bresp,
            M_AXI_BUSER                 => datastream_buser,
            M_AXI_BVALID                => datastream_bvalid,
            M_AXI_BREADY                => datastream_bready,
            M_AXI_ARID                  => datastream_arid,
            M_AXI_ARADDR                => datastream_araddr,
            M_AXI_ARLEN                 => datastream_arlen,
            M_AXI_ARSIZE                => datastream_arsize,
            M_AXI_ARBURST               => datastream_arburst,
            M_AXI_ARLOCK                => datastream_arlock,
            M_AXI_ARCACHE               => datastream_arcache,
            M_AXI_ARPROT                => datastream_arprot,
            M_AXI_ARQOS                 => datastream_arqos,
            M_AXI_ARUSER                => datastream_aruser,
            M_AXI_ARVALID               => datastream_arvalid,
            M_AXI_ARREADY               => datastream_arready,
            M_AXI_RID                   => datastream_rid,
            M_AXI_RDATA                 => datastream_rdata,
            M_AXI_RRESP                 => datastream_rresp,
            M_AXI_RLAST                 => datastream_rlast,
            M_AXI_RUSER                 => datastream_ruser,
            M_AXI_RVALID                => datastream_rvalid,
            M_AXI_RREADY                => datastream_rready
        );


    PEA_CONFIG_INST: faupu_pea_config
        generic map (
            C_M_AXI_ADDR_WIDTH  => 32,
            C_M_AXI_DATA_WIDTH  => C_AXI_MASTER_DATA_WIDTH
        )
        port map (
            write_addr      => pea_config_write_addr,
            write_data      => pea_config_write_data,
            read_addr       => pea_config_read_addr,
            read_data       => pea_config_read_data,
            write_init_txn  => pea_config_write_init_txn,
            read_init_txn   => pea_config_read_init_txn,
            ERROR           => open,
            write_txn_done  => pea_config_write_txn_done,
            read_txn_done   => pea_config_read_txn_done,
            M_AXI_ACLK      => axi_aclk,
            M_AXI_ARESETN   => axi_aresetn,
            M_AXI_AWADDR    => pea_config_awaddr,
            M_AXI_AWPROT    => pea_config_awprot,
            M_AXI_AWVALID   => pea_config_awvalid,
            M_AXI_AWREADY   => pea_config_awready,
            M_AXI_WDATA     => pea_config_wdata,
            M_AXI_WSTRB     => pea_config_wstrb,
            M_AXI_WVALID    => pea_config_wvalid,
            M_AXI_WREADY    => pea_config_wready,
            M_AXI_BRESP     => pea_config_bresp,
            M_AXI_BVALID    => pea_config_bvalid,
            M_AXI_BREADY    => pea_config_bready,
            M_AXI_ARADDR    => pea_config_araddr,
            M_AXI_ARPROT    => pea_config_arprot,
            M_AXI_ARVALID   => pea_config_arvalid,
            M_AXI_ARREADY   => pea_config_arready,
            M_AXI_RDATA     => pea_config_rdata,
            M_AXI_RRESP     => pea_config_rresp,
            M_AXI_RVALID    => pea_config_rvalid,
            M_AXI_RREADY    => pea_config_rready
        );


    iotab_we        <= iotable_fill_table_write_enable;-- when (dma_config_slave_write = '0') else axi_slave_table_write_enable;
    iotab_addr_in   <= iotable_fill_table_write_addr(TABLE_ADDR_WIDTH-1 downto 0);-- when (dma_config_slave_write = '0') else axi_slave_table_write_addr;
    iotab_data_in   <= iotable_fill_table_write_data;-- when (dma_config_slave_write = '0') else axi_slave_table_write_data;

    iotab_re        <= '1';
    iotab_addr_out  <= dma_config_debug_addr(TABLE_ADDR_WIDTH-1 downto 0);

    IO_TAB: inout_table
        generic map (
            C_ROWS  => C_NUM_IOTABLE_ROWS
        )
        port map (
            clk                         => axi_aclk,
            resetn                      => axi_aresetn,
            we                          => iotab_we,
            ADDR_IN                     => iotab_addr_in,
            DATA_IN                     => iotab_data_in,
            re                          => iotab_re,
            ADDR_OUT                    => iotab_addr_out,
            DATA_OUT                    => iotab_data_out,
            ROW                         => iotab_row,
            CONFIG                      => iotab_config,
            R_PARTITION_START_ADDR      => iotab_r_partition_start_addr,
            R_IMAGE_WIDTH               => iotab_r_image_width,
            R_PARTITION_DIMENSIONS      => iotab_r_partition_dimensions,
            W_PARTITION_START_ADDR      => iotab_w_partition_start_addr,
            W_IMAGE_WIDTH               => iotab_w_image_width,
            W_PARTITION_DIMENSIONS      => iotab_w_partition_dimensions
        );

    AXIS_TO_PEA_INST: faupu_axis_to_pea
        generic map (
            C_BUFFER_SELECT_LENGTH  => DATA_BUFFER_SELECT_LENGTH,
            C_NUM_BUFFERS           => DATA_BUFFER_NUM_BUFFERS,
            C_BUFFER_ADDR_WIDTH     => DATA_BUFFER_ADDR_WIDTH,
            C_BURST_LEN             => C_AXI_MASTER_BURST_LEN,
            C_PE_ID_WIDTH           => C_PE_ID_WIDTH,
            C_NUM_PE_ARRAYS         => C_NUM_PE_ARRAYS,
            C_M_AXIS_TDATA_WIDTH    => C_AXI_MASTER_DATA_WIDTH
        )
        port map (
            debug_out           => axis_to_pea_debug_out,
            read_buffer_ready   => read_buffer_ready,
            read_buffer_free    => read_buffer_free,
            read_buffer_select  => read_data_buffer_b_buffer_select,
            read_buffer_addr    => read_data_buffer_b_addr,
            read_buffer_data    => read_data_buffer_b_dout,
            read_buffer_pe_id   => read_data_buffer_b_pe_arr_id_out,
            read_buffer_length  => read_data_buffer_b_length_out,
            read_buffer_prog    => read_data_buffer_b_prog_out,
            read_buffer_last    => read_data_buffer_b_last_out,
            M_AXIS_ACLK         => axi_aclk,
            M_AXIS_ARESETN      => axi_aresetn,
            M_AXIS_TVALID       => to_pea_tvalid,
            M_AXIS_TDATA        => to_pea_tdata,
            M_AXIS_TSTRB        => to_pea_tstrb,
            M_AXIS_TLAST        => to_pea_tlast,
            M_AXIS_TREADY       => to_pea_tready,
            M_AXIS_TDEST        => to_pea_tdest
        );

    AXIS_FROM_PEA_INST: faupu_axis_from_pea
        generic map (
            C_BUFFER_SELECT_LENGTH  => DATA_BUFFER_SELECT_LENGTH,
            C_NUM_BUFFERS           => DATA_BUFFER_NUM_BUFFERS,
            C_BUFFER_ADDR_WIDTH     => DATA_BUFFER_ADDR_WIDTH,
            C_BURST_LEN             => C_AXI_MASTER_BURST_LEN,
            C_PE_ID_WIDTH           => C_PE_ID_WIDTH,
            C_S_AXIS_TDATA_WIDTH    => C_AXI_MASTER_DATA_WIDTH
        )
        port map (
            debug_out           => axis_from_pea_debug_out,
            write_buffer_ready  => write_buffer_ready,
            write_buffer_free   => write_buffer_free,
            write_buffer_select => write_data_buffer_b_buffer_select,
            write_buffer_addr   => write_data_buffer_b_addr,
            write_buffer_data   => write_data_buffer_b_din,
            write_buffer_pe_id  => write_data_buffer_b_pe_arr_id_in,
            write_buffer_length => write_data_buffer_b_length_in,
            write_buffer_last   => write_data_buffer_b_last_in,
            write_buffer_wren   => write_data_buffer_b_wr,
            S_AXIS_ACLK         => axi_aclk,
            S_AXIS_ARESETN      => axi_aresetn,
            S_AXIS_TREADY       => from_pea_tready,
            S_AXIS_TDATA        => from_pea_tdata,
            S_AXIS_TSTRB        => from_pea_tstrb,
            S_AXIS_TLAST        => from_pea_tlast,
            S_AXIS_TVALID       => from_pea_tvalid,
            S_AXIS_TDEST        => from_pea_tdest
        );

    -- debug
    buf_rdb_b_buffer_select <= dma_config_debug_select(DATA_BUFFER_SELECT_LENGTH-1 downto 0) when dma_config_debug_read_sel = "01" and dma_config_debug_enable = '1' else read_data_buffer_b_buffer_select;
    buf_rdb_b_addr          <= dma_config_debug_addr(DATA_BUFFER_ADDR_WIDTH-1 downto 0) when dma_config_debug_read_sel = "01" and dma_config_debug_enable = '1' else read_data_buffer_b_addr;
    buf_wdb_a_buffer_select <= dma_config_debug_select(DATA_BUFFER_SELECT_LENGTH-1 downto 0) when dma_config_debug_read_sel = "00" and dma_config_debug_enable = '1' else write_data_buffer_a_buffer_select;
    buf_wdb_a_addr          <= dma_config_debug_addr(DATA_BUFFER_ADDR_WIDTH-1 downto 0) when dma_config_debug_read_sel = "00" and dma_config_debug_enable= '1' else write_data_buffer_a_addr;
    dma_config_debug_data   <= read_data_buffer_b_dout when dma_config_debug_read_sel = "01" else
                               write_data_buffer_a_dout when dma_config_debug_read_sel = "00" else
                               iotab_data_out;

    READ_DATA_BUFFER: multi_buffer
        generic map (
            DATA_WIDTH              => C_AXI_MASTER_DATA_WIDTH,
            ADDR_WIDTH              => DATA_BUFFER_ADDR_WIDTH,
            BUFFER_SELECT_LENGTH    => DATA_BUFFER_SELECT_LENGTH,
            NUM_BUFFERS             => DATA_BUFFER_NUM_BUFFERS
        )
        port map (
            clk             => axi_aclk,
            resetn          => axi_aresetn,
            a_buffer_select => read_data_buffer_a_buffer_select,
            a_pe_arr_id_in  => read_data_buffer_a_pe_arr_id_in,
            a_length_in     => read_data_buffer_a_length_in,
            a_prog_in       => read_data_buffer_a_prog_in,
            a_last_in       => read_data_buffer_a_last_in,
            a_wr            => read_data_buffer_a_wr,
            a_addr          => read_data_buffer_a_addr,
            a_din           => read_data_buffer_a_din,
            b_buffer_select => buf_rdb_b_buffer_select, --read_data_buffer_b_buffer_select,
            b_addr          => buf_rdb_b_addr, --read_data_buffer_b_addr,
            b_dout          => read_data_buffer_b_dout,
            b_pe_arr_id_out => read_data_buffer_b_pe_arr_id_out,
            b_length_out    => read_data_buffer_b_length_out,
            b_prog_out      => read_data_buffer_b_prog_out,
            b_last_out      => read_data_buffer_b_last_out
        );

    WRITE_DATA_BUFFER: multi_buffer
        generic map (
            DATA_WIDTH              => C_AXI_MASTER_DATA_WIDTH,
            ADDR_WIDTH              => DATA_BUFFER_ADDR_WIDTH,
            BUFFER_SELECT_LENGTH    => DATA_BUFFER_SELECT_LENGTH,
            NUM_BUFFERS             => DATA_BUFFER_NUM_BUFFERS
        )
        port map (
            clk             => axi_aclk,
            resetn          => axi_aresetn,
            a_buffer_select => write_data_buffer_b_buffer_select,
            a_pe_arr_id_in  => write_data_buffer_b_pe_arr_id_in,
            a_length_in     => write_data_buffer_b_length_in,
            a_prog_in       => '0',
            a_last_in       => write_data_buffer_b_last_in,
            a_wr            => write_data_buffer_b_wr,
            a_addr          => write_data_buffer_b_addr,
            a_din           => write_data_buffer_b_din,
            b_buffer_select => buf_wdb_a_buffer_select,
            b_addr          => buf_wdb_a_addr,
            b_dout          => write_data_buffer_a_dout,
            b_pe_arr_id_out => write_data_buffer_a_pe_arr_id_out,
            b_length_out    => write_data_buffer_a_length_out,
            b_prog_out      => open,
            b_last_out      => write_data_buffer_a_last_out
        );

    -- Instantiation of Axi Bus Interface S_AXI_INTR
    faupu_dma_irq_v1_0_S_AXI_INTR_inst : faupu_dma_irq_v1_0_S_AXI_INTR
        generic map (
            C_S_AXI_DATA_WIDTH  => C_S_AXI_INTR_DATA_WIDTH,
            C_S_AXI_ADDR_WIDTH  => C_S_AXI_INTR_ADDR_WIDTH,
            C_NUM_OF_INTR       => C_NUM_OF_INTR,
            C_INTR_SENSITIVITY  => C_INTR_SENSITIVITY,
            C_INTR_ACTIVE_STATE => C_INTR_ACTIVE_STATE,
            C_IRQ_SENSITIVITY   => C_IRQ_SENSITIVITY,
            C_IRQ_ACTIVE_STATE  => C_IRQ_ACTIVE_STATE
        )
        port map (
            table_txns_done => iotable_fill_table_txns_done,
            S_AXI_ACLK      => axi_aclk,
            S_AXI_ARESETN   => axi_aresetn,
            S_AXI_AWADDR    => s_axi_intr_awaddr,
            S_AXI_AWPROT    => s_axi_intr_awprot,
            S_AXI_AWVALID   => s_axi_intr_awvalid,
            S_AXI_AWREADY   => s_axi_intr_awready,
            S_AXI_WDATA     => s_axi_intr_wdata,
            S_AXI_WSTRB     => s_axi_intr_wstrb,
            S_AXI_WVALID    => s_axi_intr_wvalid,
            S_AXI_WREADY    => s_axi_intr_wready,
            S_AXI_BRESP     => s_axi_intr_bresp,
            S_AXI_BVALID    => s_axi_intr_bvalid,
            S_AXI_BREADY    => s_axi_intr_bready,
            S_AXI_ARADDR    => s_axi_intr_araddr,
            S_AXI_ARPROT    => s_axi_intr_arprot,
            S_AXI_ARVALID   => s_axi_intr_arvalid,
            S_AXI_ARREADY   => s_axi_intr_arready,
            S_AXI_RDATA     => s_axi_intr_rdata,
            S_AXI_RRESP     => s_axi_intr_rresp,
            S_AXI_RVALID    => s_axi_intr_rvalid,
            S_AXI_RREADY    => s_axi_intr_rready,
            irq             => irq
        );

end arch_imp;
