library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_datastream is
    generic (
        -- Users to add parameters here

        C_BUFFER_SELECT_LENGTH      : integer   := 1;
        C_NUM_BUFFERS               : integer   := 2;
        C_BUFFER_ADDR_WIDTH         : integer   := 16;
        C_NUM_PE_ARRAYS             : integer   := 2;
        C_NUM_IOTABLE_ROWS          : integer   := 16;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
        C_M_AXI_BURST_LEN           : integer   := 16;
        -- Thread ID Width
        C_M_AXI_ID_WIDTH            : integer   := 1;
        -- Width of Address Bus
        C_M_AXI_ADDR_WIDTH          : integer   := 32;
        -- Width of Data Bus
        C_M_AXI_DATA_WIDTH          : integer   := 32;
        -- Width of User Write Address Bus
        C_M_AXI_AWUSER_WIDTH        : integer   := 0;
        -- Width of User Read Address Bus
        C_M_AXI_ARUSER_WIDTH        : integer   := 0;
        -- Width of User Write Data Bus
        C_M_AXI_WUSER_WIDTH         : integer   := 0;
        -- Width of User Read Data Bus
        C_M_AXI_RUSER_WIDTH         : integer   := 0;
        -- Width of User Response Bus
        C_M_AXI_BUSER_WIDTH         : integer   := 0
    );
    port (
        -- Users to add ports here

        -- debug
        debug_select                : in  std_logic_vector(1 downto 0);
        debug_addr                  : in  std_logic_vector(31 downto 0);
        debug_out0                  : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        debug_out1                  : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

        pea_config_write_addr       : out std_logic_vector(31 downto 0);
        pea_config_write_data       : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        pea_config_write_init_txn   : out std_logic;
        pea_config_write_txn_done   : in  std_logic;
        pea_config_read_addr        : out std_logic_vector(31 downto 0);
        pea_config_read_data        : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        pea_config_read_init_txn    : out std_logic;
        pea_config_read_txn_done    : in  std_logic;

        iotable_filled              : in  std_logic;
        iotable_filled_ack          : out std_logic;
        read_txns_done              : out std_logic;

        -- indicates if there is data ready in one of the write data buffers for writing back to the RAM
        write_buffer_ready          : in  std_logic;
        -- indicates if data is done writing to one of the read data buffers and ready for read by the CU
        read_buffer_ready           : out std_logic;

        -- indicates if one of the write data buffers is able to accept a single burst from the CU
        write_buffer_free           : out std_logic;
        -- indicates if one of the read data buffers is able to accept a single burst from the RAM
        read_buffer_free            : in  std_logic;

        -- select either buffer 0 or 1
        write_buffer_select         : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        -- Address for getting data from the write data buffer
        write_buffer_addr           : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        -- Data from the write data buffer
        write_buffer_data           : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- the ID of the PE array from where the data is coming
        write_buffer_pe_id          : in  std_logic_vector(15 downto 0); -- TODO: length of the signal
        write_buffer_length         : in  std_logic_vector(31 downto 0);
        write_buffer_last           : in  std_logic;

        -- select either buffer 0 or 1
        read_buffer_select          : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        -- Address for writing data to the read data buffer
        read_buffer_addr            : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        -- Data to the read data buffer
        read_buffer_data            : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- the ID of the PE array to where the data has to be written
        read_buffer_pe_id           : out std_logic_vector(15 downto 0); -- TODO: length of the signal
        read_buffer_length          : out std_logic_vector(31 downto 0);
        read_buffer_prog            : out std_logic;
        read_buffer_last            : out std_logic;
        -- write enable for the read data buffer
        read_buffer_wren            : out std_logic;

        -- row from where to read from the IO table
        iotable_row                 : out std_logic_vector(28 downto 0);

        -- output of the table row that is refered to with iotable_row
        CONFIG                      : in  std_logic_vector(31 downto 0);
        R_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
        R_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
        R_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);
        W_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
        W_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
        W_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global Clock Signal.
        M_AXI_ACLK      : in  std_logic;
        -- Global Reset Singal. This Signal is Active Low
        M_AXI_ARESETN   : in  std_logic;
        -- Master Interface Write Address ID
        M_AXI_AWID      : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Address
        M_AXI_AWADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_AWLEN     : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_AWSIZE    : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_AWBURST   : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_AWLOCK    : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_AWCACHE   : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_AWPROT    : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each write transaction.
        M_AXI_AWQOS     : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the write address channel.
        M_AXI_AWUSER    : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid write address and control information.
        M_AXI_AWVALID   : out std_logic;
        -- Write address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_AWREADY   : in  std_logic;
        -- Master Interface Write ID
        M_AXI_WID       : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Data.
        M_AXI_WDATA     : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte
        -- lanes hold valid data. There is one write strobe
        -- bit for each eight bits of the write data bus.
        M_AXI_WSTRB     : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        -- Write last. This signal indicates the last transfer in a write burst.
        M_AXI_WLAST     : out std_logic;
        -- Optional User-defined signal in the write data channel.
        M_AXI_WUSER     : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available
        M_AXI_WVALID    : out std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        M_AXI_WREADY    : in  std_logic;
        -- Master Interface Write Response.
        M_AXI_BID       : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Write response. This signal indicates the status of the write transaction.
        M_AXI_BRESP     : in  std_logic_vector(1 downto 0);
        -- Optional User-defined signal in the write response channel
        M_AXI_BUSER     : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        -- Write response valid. This signal indicates that the
        -- channel is signaling a valid write response.
        M_AXI_BVALID    : in  std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        M_AXI_BREADY    : out std_logic;
        -- Master Interface Read Address.
        M_AXI_ARID      : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Read address. This signal indicates the initial
        -- address of a read burst transaction.
        M_AXI_ARADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_ARLEN     : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_ARSIZE    : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_ARBURST   : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_ARLOCK    : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_ARCACHE   : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_ARPROT    : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each read transaction
        M_AXI_ARQOS     : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the read address channel.
        M_AXI_ARUSER    : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid read address and control information
        M_AXI_ARVALID   : out std_logic;
        -- Read address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_ARREADY   : in  std_logic;
        -- Read ID tag. This signal is the identification tag
        -- for the read data group of signals generated by the slave.
        M_AXI_RID       : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Read Data
        M_AXI_RDATA     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the read transfer
        M_AXI_RRESP     : in  std_logic_vector(1 downto 0);
        -- Read last. This signal indicates the last transfer in a read burst
        M_AXI_RLAST     : in  std_logic;
        -- Optional User-defined signal in the read address channel.
        M_AXI_RUSER     : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        -- Read valid. This signal indicates that the channel
        -- is signaling the required read data.
        M_AXI_RVALID    : in  std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        M_AXI_RREADY    : out std_logic
    );
end faupu_datastream;

architecture implementation of faupu_datastream is

    ---------------
    -- CONSTANTS --
    ---------------

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
    constant C_TRANSACTIONS_NUM         : integer := clogb2(C_M_AXI_BURST_LEN-1);

    -- Width of the address for accessing the data in the output table
    constant OT_ADDR_WIDTH              : integer := clogb2(8 * C_NUM_PE_ARRAYS - 1);


    ----------------
    -- COMPONENTS --
    ----------------

    component bram is
        generic (
            DATA_WIDTH  : integer;
            ADDR_WIDTH  : integer
        );
        port (
            -- Port A
            a_clk   : in  std_logic;
            a_wr    : in  std_logic;
            a_addr  : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            a_din   : in  std_logic_vector(DATA_WIDTH-1 downto 0);
            a_dout  : out std_logic_vector(DATA_WIDTH-1 downto 0);

            -- Port B
            b_clk   : in  std_logic;
            b_wr    : in  std_logic;
            b_addr  : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            b_din   : in  std_logic_vector(DATA_WIDTH-1 downto 0);
            b_dout  : out std_logic_vector(DATA_WIDTH-1 downto 0)
        );
    end component bram;


    --------------------
    -- STATE MACHINES --
    --------------------

    type input_fsm_state_type is (  IDLE, LOAD_DESCRIPTOR, CHECK_DESCRIPTOR, CHECK_OT, CHECK_PEA_MODE,
                                    ACTIVATE_PROG_MODE, ACTIVATE_EXEC_MODE,
                                    WRITE_PART_START_ADDR_TO_OT, WRITE_IMAGE_WIDTH_TO_OT,
                                    WRITE_PART_DIMENSIONS_TO_OT, WRITE_X_TO_OT, WRITE_Y_TO_OT,
                                    WRITE_WADDR_TO_OT, WRITE_NLSA_TO_OT, WRITE_CONFIG_TO_OT,
                                    INIT_FIRST_TRANSFER, START_SINGLE_TRANSFER,
                                    WAIT_FOR_TRANSFER_COMPLETION, INIT_NEXT_TRANSFER);
    signal input_fsm_state              : input_fsm_state_type;

    type input_transfer_state_type is (IDLE, START_TRANSFER, READ_DATA, TXN_DONE);
    signal input_transfer_state         : input_transfer_state_type;

    type output_fsm_state_type is ( IDLE, GET_OT_ROW, INIT_TRANSFER, START_SINGLE_TRANSFER,
                                    WAIT_FOR_TRANSFER_COMPLETION, CHECK_PARTITION_DONE,
                                    UPDATE_OT, SELECT_NEXT_BUFFER, WAIT_FOR_BUFFER_DATA);
    signal output_fsm_state             : output_fsm_state_type;
    signal prev_output_fsm_state        : output_fsm_state_type;

    type output_transfer_state_type is (IDLE, START_TRANSFER, WRITE_ADDR, WRITE_DATA, GET_WRITE_RESPONSE, TXN_DONE);
    signal output_transfer_state        : output_transfer_state_type;

    -------------
    -- SIGNALS --
    -------------

    -- signals for accessing the output table
    signal ot_in_wr                     : std_logic;
    signal ot_in_addr                   : std_logic_vector(OT_ADDR_WIDTH-1 downto 0);
    signal ot_in_din                    : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal ot_in_dout                   : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal ot_out_wr                    : std_logic;
    signal ot_out_addr                  : std_logic_vector(OT_ADDR_WIDTH-1 downto 0);
    signal ot_out_din                   : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal ot_out_dout                  : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

    signal next_iotable_row             : std_logic_vector(28 downto 0);

    signal pea_configured               : std_logic_vector(C_NUM_PE_ARRAYS-1 downto 0);
    signal cur_pea_modes                : std_logic_vector(C_NUM_PE_ARRAYS-1 downto 0);

    signal input_x                      : integer;
    signal input_y                      : integer;
    signal input_addr                   : std_logic_vector(31 downto 0);
    signal input_nlsa                   : unsigned(31 downto 0);
    signal input_part_height            : std_logic_vector(15 downto 0);
    signal input_part_width             : std_logic_vector(31 downto 0);
    signal input_image_width_bytes      : std_logic_vector(31 downto 0);

    signal init_ot_row                  : unsigned(15 downto 0);

    signal read_buffer_ready_ff         : std_logic;
    signal read_buffer_ready_ff_prev    : std_logic;
    signal num_read_buffers_free        : integer range 0 to C_NUM_BUFFERS;

    signal write_buffer_free_ff         : std_logic;
    signal write_buffer_free_ff_prev    : std_logic;
    signal num_write_buffers_ready      : integer range 0 to C_NUM_BUFFERS;

    signal reads_done                   : std_logic;
    signal writes_done                  : std_logic;

    signal burst_size_bytes             : unsigned(31 downto 0);

    signal axi_arvalid                  : std_logic;
    signal axi_rready                   : std_logic;
    signal rnext                        : std_logic;
    signal axi_awvalid                  : std_logic;
    signal axi_wvalid                   : std_logic;
    signal axi_wlast                    : std_logic;
    signal axi_bready                   : std_logic;
    signal wnext                        : std_logic;

    signal read_buffer_select_buf       : unsigned(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal read_buffer_addr_buf         : unsigned(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal line_data_to_read            : integer;
    signal read_index                   : unsigned(C_TRANSACTIONS_NUM downto 0);

    signal write_buffer_select_buf      : unsigned(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal write_buffer_addr_buf        : unsigned(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal write_index                  : unsigned(31 downto 0); --unsigned(C_TRANSACTIONS_NUM downto 0);
    signal row_write_index              : unsigned(31 downto 0); --unsigned(C_TRANSACTIONS_NUM downto 0);

    signal buffer_start_idx             : unsigned(31 downto 0); --unsigned(C_TRANSACTIONS_NUM downto 0);

    signal output_pe_id                 : std_logic_vector(OT_ADDR_WIDTH-4 downto 0);
    signal output_length                : std_logic_vector(31 downto 0);
    signal ot_out_col                   : std_logic_vector(2 downto 0);

    type output_row_type is array(7 downto 0) of std_logic_vector(31 downto 0);
    signal output_row                   : output_row_type;

    signal output_config                : std_logic_vector(31 downto 0);
    signal output_part_start_addr       : std_logic_vector(31 downto 0);
    signal output_image_width           : std_logic_vector(31 downto 0);
    signal output_image_width_bytes     : std_logic_vector(31 downto 0);
    signal output_part_width            : std_logic_vector(31 downto 0);
    signal output_part_height           : std_logic_vector(15 downto 0);
    signal output_x                     : unsigned(31 downto 0);
    signal output_y                     : unsigned(31 downto 0);
    signal output_addr                  : std_logic_vector(31 downto 0);
    signal output_nlsa                  : unsigned(31 downto 0);

    signal output_invalid               : std_logic;

    signal count_cycles                 : integer;
    signal col_count                    : integer;

    signal response                     : std_logic;


    constant C_NUM_MODE_REGS            : integer := 5;

    signal mode_reg_ctr                 : integer range 0 to C_NUM_MODE_REGS-1;

    type mode_reg_offset_type is array(0 to C_NUM_MODE_REGS-1) of std_logic_vector(15 downto 0);
    constant prog_mode_reg_offset       : mode_reg_offset_type := (x"0020", x"0018", x"001C", x"001C", x"0028");
    constant exec_mode_reg_offset       : mode_reg_offset_type := (x"0020", x"0028", x"0018", x"001C", x"001C");

    type mode_reg_data_type is array(0 to C_NUM_MODE_REGS-1) of std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    constant prog_mode_reg_data         : mode_reg_data_type := (
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(0, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH))
                                                                );
    constant exec_mode_reg_data         : mode_reg_data_type := (
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(0, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(0, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH)),
                                                                    std_logic_vector(to_unsigned(0, C_M_AXI_DATA_WIDTH))
                                                                );

    type switch_pea_mode_state_type is (IDLE, GET_PEA_STATUS_PRE, WRITE_PROG_MODE_REGISTER, PROG_MODE_TXN_DONE, WRITE_EXEC_MODE_REGISTER, EXEC_MODE_TXN_DONE, GET_PEA_STATUS_POST, MODE_SWITCHED);
    signal switch_pea_mode_state        : switch_pea_mode_state_type;

    signal pea_status_pre               : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal pea_status_post              : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

    signal debug_0_out0_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_1_out0_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_2_out0_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_3_out0_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_0_out1_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_1_out1_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_2_out1_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_3_out1_buf             : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);

    signal debug_out0_buf               : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal debug_out1_buf               : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

    signal axi_awlen                    : std_logic_vector(7 downto 0);

    type debug_out_arr_type is array(64 downto 0) of std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal debug_out_arr                : debug_out_arr_type;

    signal debug_out_arr_idx            : integer;

    signal read_txns_done_buf           : std_logic;
    signal read_txns_done_ff            : std_logic;
    signal read_txns_done_ff2           : std_logic;

    signal count_read_descr_done        : integer;
    signal count_write_descr_done       : integer;

begin

    --I/O Connections. Write Address (AW)
    M_AXI_AWID          <= (others => '0');
    --Burst LENgth is number of transaction beats, minus 1
    M_AXI_AWLEN         <= axi_awlen; --std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    M_AXI_AWSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8)-1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_AWBURST       <= "01";
    M_AXI_AWLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_AWCACHE       <= "0010";
    M_AXI_AWPROT        <= "000";
    M_AXI_AWQOS         <= x"0";
    M_AXI_AWUSER        <= (others => '1');
    M_AXI_AWVALID       <= axi_awvalid;
    --Write Data(W)
    M_AXI_WID           <= (others => '0');
    M_AXI_WDATA         <= write_buffer_data;
    --All bursts are complete and aligned in this example
    M_AXI_WSTRB         <= (others => '1');
    M_AXI_WLAST         <= axi_wlast and axi_wvalid;
    M_AXI_WUSER         <= (others => '0');
    M_AXI_WVALID        <= axi_wvalid;
    --Write Response (B)
    M_AXI_BREADY        <= axi_bready;
    --Read Address (AR)
    M_AXI_ARID          <= (others => '0');
    --Burst LENgth is number of transaction beats, minus 1
    --M_AXI_ARLEN         <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    M_AXI_ARSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8)-1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_ARBURST       <= "01";
    M_AXI_ARLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_ARCACHE       <= "0010";
    M_AXI_ARPROT        <= "000";
    M_AXI_ARQOS         <= x"0";
    M_AXI_ARUSER        <= (others => '1');
    M_AXI_ARVALID       <= axi_arvalid;
    --Read and Read Response (R)
    M_AXI_RREADY        <= axi_rready;

    burst_size_bytes    <= to_unsigned(C_M_AXI_BURST_LEN * (C_M_AXI_DATA_WIDTH / 8), 32);

    debug_out0          <= pea_status_pre when debug_select = "01" else debug_out0_buf when debug_select = "10" else debug_3_out0_buf & debug_2_out0_buf & debug_1_out0_buf & debug_0_out0_buf;
    debug_out1          <= pea_status_post when debug_select = "01" else debug_out1_buf when debug_select = "10" else debug_3_out1_buf & debug_2_out1_buf & debug_1_out1_buf & debug_0_out1_buf;

    DEBUG_PROC: process(M_AXI_ACLK)
        variable mem_index      : integer;
        variable tmp_u          : unsigned(31 downto 0);
        variable tmp_u_bytes    : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                debug_0_out0_buf <= (others => '0');
                debug_1_out0_buf <= (others => '0');
                debug_2_out0_buf <= (others => '0');
                debug_3_out0_buf <= (others => '0');

                debug_0_out1_buf <= (others => '0');
                debug_1_out1_buf <= (others => '0');
                debug_2_out1_buf <= (others => '0');
                debug_3_out1_buf <= (others => '0');

                debug_out_arr_idx   <= 2;
            else
                debug_out0_buf  <= debug_out_arr(conv_integer(debug_addr));
                debug_out1_buf  <= debug_out_arr(conv_integer(debug_addr));

                debug_0_out0_buf <= std_logic_vector(to_unsigned(num_write_buffers_ready, C_M_AXI_DATA_WIDTH/4));
                debug_1_out0_buf <= std_logic_vector(to_unsigned(num_read_buffers_free, C_M_AXI_DATA_WIDTH/4));

                if (reads_done = '1') then
                    debug_2_out0_buf <= std_logic_vector(unsigned(debug_2_out0_buf) + 1);
                end if;

                if (writes_done = '1') then
                    debug_3_out0_buf <= std_logic_vector(unsigned(debug_3_out0_buf) + 1);
                end if;

                if (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    debug_0_out1_buf <= std_logic_vector(unsigned(debug_0_out1_buf) + 1);
                end if;

                if (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    debug_1_out1_buf <= std_logic_vector(unsigned(debug_1_out1_buf) + 1);
                end if;

                if (input_transfer_state = START_TRANSFER) then
                    debug_2_out1_buf <= (others => '0');
                elsif (axi_rready = '1' and M_AXI_RVALID = '1') then
                    debug_2_out1_buf <= std_logic_vector(unsigned(debug_2_out1_buf) + 1);
                end if;

                if (output_transfer_state = START_TRANSFER) then
                    debug_3_out1_buf <= (others => '0');
                elsif (wnext = '1') then
                    debug_3_out1_buf <= std_logic_vector(unsigned(debug_3_out1_buf) + 1);
                end if;

    --            if (prev_output_fsm_state = GET_OT_ROW and output_fsm_state = INIT_TRANSFER) then
    --                if (debug_out_arr_idx + 6 < 65) then
    --                    debug_out_arr(0)                        <= output_part_width;
    --                    debug_out_arr(1)(15 downto 0)           <= output_part_height;
    --                    debug_out_arr(1)(31 downto 16)          <= (others => '0');
    --                    debug_out_arr(debug_out_arr_idx + 0)    <= output_length;
    --                    debug_out_arr(debug_out_arr_idx + 1)    <= output_row(4);
    --                    debug_out_arr(debug_out_arr_idx + 2)    <= output_row(5);
    --                    debug_out_arr(debug_out_arr_idx + 3)    <= output_row(6);
    --                    debug_out_arr(debug_out_arr_idx + 4)    <= output_row(7);
    --                    debug_out_arr(debug_out_arr_idx + 5)    <= (others => '0');
    --                    debug_out_arr(debug_out_arr_idx + 6)(31 downto C_BUFFER_SELECT_LENGTH+16) <= (others => '0');
    --                    debug_out_arr(debug_out_arr_idx + 6)(C_BUFFER_SELECT_LENGTH+15 downto 16) <= std_logic_vector(write_buffer_select_buf);
    --                    debug_out_arr(debug_out_arr_idx + 6)(15 downto OT_ADDR_WIDTH-3) <= (others => '0');
    --                    debug_out_arr(debug_out_arr_idx + 6)(OT_ADDR_WIDTH-4 downto 0) <= output_pe_id;

    --                    if (debug_out_arr_idx + 7 >= 65) then
    --                        debug_out_arr_idx <= 2;
    --                    else
    --                        debug_out_arr_idx <= debug_out_arr_idx + 7;
    --                    end if;
    --                end if;
    --            elsif (output_fsm_state = WAIT_FOR_TRANSFER_COMPLETION) then
    --                if (output_transfer_state = TXN_DONE) then
    --                    if (debug_out_arr_idx + 6 < 65) then
    --                        debug_out_arr(0)                        <= output_part_width;
    --                        debug_out_arr(1)(15 downto 0)           <= output_part_height;
    --                        debug_out_arr(1)(31 downto 16)          <= (others => '0');
    --                        debug_out_arr(debug_out_arr_idx + 0)    <= output_length;
    --                        debug_out_arr(debug_out_arr_idx + 1)    <= std_logic_vector(output_x);
    --                        debug_out_arr(debug_out_arr_idx + 2)(15 downto 0) <= std_logic_vector(output_y(15 downto 0));
    --                        debug_out_arr(debug_out_arr_idx + 2)(31) <= write_buffer_last;
    --                        debug_out_arr(debug_out_arr_idx + 3)    <= output_addr;
    --                        debug_out_arr(debug_out_arr_idx + 4)    <= std_logic_vector(output_nlsa);
    --                        debug_out_arr(debug_out_arr_idx + 5)    <= std_logic_vector(buffer_start_idx);
    --                        debug_out_arr(debug_out_arr_idx + 6)(31 downto C_BUFFER_SELECT_LENGTH+16) <= (others => '0');
    --                        debug_out_arr(debug_out_arr_idx + 6)(C_BUFFER_SELECT_LENGTH+15 downto 16) <= std_logic_vector(write_buffer_select_buf);
    --                        debug_out_arr(debug_out_arr_idx + 6)(15 downto OT_ADDR_WIDTH-3) <= (others => '0');
    --                        debug_out_arr(debug_out_arr_idx + 6)(OT_ADDR_WIDTH-4 downto 0) <= output_pe_id;

    --                        if (debug_out_arr_idx + 7 >= 65) then
    --                            debug_out_arr_idx <= 2;
    --                        else
    --                            debug_out_arr_idx <= debug_out_arr_idx + 7;
    --                        end if;

    --                        if (write_index < unsigned(output_length)) then
    --                            debug_out_arr(debug_out_arr_idx + 2)(30 downto 16) <= std_logic_vector(to_unsigned(1, 15));
    --                        else
    --                            tmp_u       := unsigned(output_length) - buffer_start_idx;
    --                            tmp_u_bytes := std_logic_vector(tmp_u(29 downto 0)) & "00";

    --                            if ((output_x + tmp_u >= unsigned(output_part_width)
    --                                    and output_y + 1 >= unsigned(output_part_height)) or output_config(0) = '0') then
    --                                debug_out_arr(debug_out_arr_idx + 2)(30 downto 16) <= std_logic_vector(to_unsigned(2, 15));
    --                            else
    --                                if (output_x + tmp_u >= unsigned(output_part_width)) then
    --                                    debug_out_arr(debug_out_arr_idx + 2)(30 downto 16) <= std_logic_vector(to_unsigned(3, 15));
    --                                else
    --                                    debug_out_arr(debug_out_arr_idx + 2)(30 downto 16) <= std_logic_vector(to_unsigned(4, 15));
    --                                end if;
    --                            end if;
    --                        end if;
    --                    end if;

    --                    if (output_length /= x"00000010") then
    --                        debug_2_out0_buf <= output_length((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    --                    end if;

    --                    if (write_index < unsigned(output_length)) then
    --                        debug_0_out1_buf <= std_logic_vector(unsigned(debug_0_out1_buf) + 1);
    --                    else
    --                        tmp_u       := unsigned(output_length) - buffer_start_idx;
    --                        tmp_u_bytes := std_logic_vector(tmp_u(29 downto 0)) & "00";

    --                        if ((output_x + tmp_u >= unsigned(output_part_width)
    --                                and output_y + 1 >= unsigned(output_part_height)) or output_config(0) = '0') then
    --                            debug_1_out1_buf <= std_logic_vector(unsigned(debug_1_out1_buf) + 1);
    --                        else
    --                            if (output_x + tmp_u >= unsigned(output_part_width)) then
    --                                debug_2_out1_buf <= std_logic_vector(unsigned(debug_2_out1_buf) + 1);
    --                            else
    --                                debug_3_out1_buf <= std_logic_vector(unsigned(debug_3_out1_buf) + 1);
    --                            end if;
    --                        end if;
    --                    end if;
    --                end if;
    --            end if;
    --            --if (M_AXI_RVALID = '1' and M_AXI_RLAST = '1' and axi_rready = '1') then
    --            --    debug_3_out1_buf <= std_logic_vector(unsigned(debug_3_out1_buf) + 1);
    --            --end if;
            end if;
        end if;
    end process;

    OUTPUT_TABLE: bram
        generic map (
            DATA_WIDTH  => C_M_AXI_DATA_WIDTH,
            ADDR_WIDTH  => OT_ADDR_WIDTH
        )
        port map (
            a_clk       => M_AXI_ACLK,
            a_wr        => ot_in_wr,
            a_addr      => ot_in_addr,
            a_din       => ot_in_din,
            a_dout      => ot_in_dout,
            b_clk       => M_AXI_ACLK,
            b_wr        => ot_out_wr,
            b_addr      => ot_out_addr,
            b_din       => ot_out_din,
            b_dout      => ot_out_dout
        );

    -----------------------------
    -- READING FROM DDR MEMORY --
    -----------------------------

    --read_txns_done <= read_txns_done_ff and (not read_txns_done_ff2);

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_txns_done_ff   <= '0';
                read_txns_done_ff2  <= '0';
                read_txns_done      <= '0';
            else
                if (read_txns_done_ff = '0' and read_txns_done_buf = '1' and count_read_descr_done = count_write_descr_done) then
                    read_txns_done <= '1';
                else
                    read_txns_done <= '0';
                end if;

                read_txns_done_ff2  <= read_txns_done_ff;

                if (count_read_descr_done = count_write_descr_done) then
                    read_txns_done_ff   <= read_txns_done_buf;
                else
                    read_txns_done_ff   <= '0';
                end if;
            end if;
        end if;
    end process;

    INPUT_FSM_PROC: process(M_AXI_ACLK)
        variable tmp_addr   : std_logic_vector(18 downto 0);
        variable pea_id     : integer;
        variable tmp_arlen  : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                input_fsm_state         <= IDLE;
                iotable_row             <= (others => '0');
                next_iotable_row        <= (others => '0');
                ot_in_addr              <= (others => '0');
                ot_in_wr                <= '0';
                ot_in_din               <= (others => '0');
                pea_configured          <= (others => '0');
                cur_pea_modes           <= (others => '0');
                iotable_filled_ack      <= '0';
                input_x                 <= 0;
                input_y                 <= 0;
                input_addr              <= (others => '0');
                input_nlsa              <= (others => '0');
                input_image_width_bytes <= (others => '0');
                input_part_height       <= (others => '0');
                input_part_width        <= (others => '0');
                init_ot_row             <= (others => '0');
                read_txns_done_buf      <= '0';
                count_read_descr_done   <= 0;
                M_AXI_ARLEN             <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
            else
                case (input_fsm_state) is
                    when IDLE =>
                        if (init_ot_row < C_NUM_PE_ARRAYS) then
                            tmp_addr            := std_logic_vector(init_ot_row) & "000";
                            ot_in_addr          <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                            ot_in_din           <= (others => '0');
                            ot_in_wr            <= '1';

                            init_ot_row         <= init_ot_row + 1;
                        elsif (iotable_filled = '1') then
                            read_txns_done_buf      <= '0';
                            count_read_descr_done   <= 0;

                            ot_in_wr            <= '0';
                            iotable_row         <= std_logic_vector(next_iotable_row);
                            input_fsm_state     <= LOAD_DESCRIPTOR;
                            next_iotable_row    <= next_iotable_row + 1;
                            iotable_filled_ack  <= '1';
                        end if;

                    when LOAD_DESCRIPTOR =>
                        iotable_filled_ack  <= '0';
                        input_fsm_state     <= CHECK_DESCRIPTOR;

                    when CHECK_DESCRIPTOR =>
                        if (CONFIG(0) = '0') then
                            input_fsm_state         <= IDLE;
                            read_txns_done_buf      <= '1';
                            next_iotable_row        <= (others => '0');
                        else
                            input_fsm_state         <= CHECK_OT;
                            tmp_addr                := CONFIG(31 downto 16) & "000";
                            ot_in_addr              <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                            ot_in_wr                <= '0';
                            input_image_width_bytes <= R_IMAGE_WIDTH(29 downto 0) & "00";
                        end if;

                    when CHECK_OT =>
                        if (ot_in_dout(0) = '0') then
                            input_fsm_state <= CHECK_PEA_MODE;
                        end if;

                    when CHECK_PEA_MODE =>
                        pea_id := conv_integer(CONFIG(31 downto 16));

                        if (pea_configured(pea_id) = '0' or cur_pea_modes(pea_id) /= CONFIG(1)) then
                            pea_configured(pea_id)  <= '1';
                            cur_pea_modes(pea_id)   <= CONFIG(1);

                            if (CONFIG(1) = '0') then
                                input_fsm_state <= ACTIVATE_EXEC_MODE;
                            else
                                input_fsm_state <= ACTIVATE_PROG_MODE;
                            end if;
                        else
                            if (CONFIG(1) = '0') then
                                input_fsm_state <= WRITE_PART_START_ADDR_TO_OT;
                            else
                                input_fsm_state <= INIT_FIRST_TRANSFER;
                            end if;
                        end if;

                    when ACTIVATE_PROG_MODE =>
                        if (switch_pea_mode_state = MODE_SWITCHED) then
                            input_fsm_state <= INIT_FIRST_TRANSFER;
                        end if;

                    when ACTIVATE_EXEC_MODE =>
                        if (switch_pea_mode_state = MODE_SWITCHED) then
                            input_fsm_state <= WRITE_PART_START_ADDR_TO_OT;
                        end if;

                    when WRITE_PART_START_ADDR_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "001";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= W_PARTITION_START_ADDR;
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_IMAGE_WIDTH_TO_OT;

                    when WRITE_IMAGE_WIDTH_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "010";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= W_IMAGE_WIDTH;
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_PART_DIMENSIONS_TO_OT;

                    when WRITE_PART_DIMENSIONS_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "011";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= W_PARTITION_DIMENSIONS;
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_X_TO_OT;

                    when WRITE_X_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "100";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= (others => '0');
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_Y_TO_OT;

                    when WRITE_Y_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "101";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= (others => '0');
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_WADDR_TO_OT;

                    when WRITE_WADDR_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "110";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= (others => '0');
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_NLSA_TO_OT;

                    when WRITE_NLSA_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "111";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= W_IMAGE_WIDTH(29 downto 0) & "00";
                        ot_in_wr    <= '1';

                        input_fsm_state <= WRITE_CONFIG_TO_OT;

                    when WRITE_CONFIG_TO_OT =>
                        tmp_addr    := CONFIG(31 downto 16) & "000";
                        ot_in_addr  <= tmp_addr(OT_ADDR_WIDTH-1 downto 0);
                        ot_in_din   <= std_logic_vector(to_unsigned(0, C_M_AXI_DATA_WIDTH-2)) & CONFIG(3) & "1";
                        ot_in_wr    <= '1';

                        input_fsm_state <= INIT_FIRST_TRANSFER;

                    when INIT_FIRST_TRANSFER =>
                        ot_in_wr    <= '0';
                        input_x     <= 0;
                        input_y     <= 0;
                        input_addr  <= R_PARTITION_START_ADDR;
                        input_nlsa  <= unsigned(R_PARTITION_START_ADDR) + unsigned(input_image_width_bytes);

                        if (CONFIG(2) = '0' and CONFIG(1) = '0') then
                            input_part_height <= R_PARTITION_DIMENSIONS(31 downto 16);
                            input_part_width  <= x"0000" & R_PARTITION_DIMENSIONS(15 downto  0);

                            if (unsigned(R_PARTITION_DIMENSIONS(15 downto 0)) < to_unsigned(C_M_AXI_BURST_LEN, 16)) then
                                M_AXI_ARLEN <= std_logic_vector(unsigned(R_PARTITION_DIMENSIONS(7 downto 0)) - 1);
                            else
                                M_AXI_ARLEN <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
                            end if;
                        else
                            input_part_height <= x"0001";
                            input_part_width  <= R_PARTITION_DIMENSIONS;

                            if (unsigned(R_PARTITION_DIMENSIONS(31 downto 0)) < to_unsigned(C_M_AXI_BURST_LEN, 32)) then
                                M_AXI_ARLEN <= std_logic_vector(unsigned(R_PARTITION_DIMENSIONS(7 downto 0)) - 1);
                            else
                                M_AXI_ARLEN <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
                            end if;
                        end if;

                        input_fsm_state <= START_SINGLE_TRANSFER;

                    when START_SINGLE_TRANSFER =>
                        if (input_transfer_state = START_TRANSFER) then
                            input_fsm_state <= WAIT_FOR_TRANSFER_COMPLETION;
                        end if;

                    when WAIT_FOR_TRANSFER_COMPLETION =>
                        if (input_transfer_state = TXN_DONE) then
                            input_fsm_state <= INIT_NEXT_TRANSFER;
                        end if;

                    when INIT_NEXT_TRANSFER =>
                        if (input_x + C_M_AXI_BURST_LEN >= unsigned(input_part_width)) then
                            if (input_y + 1 < input_part_height) then
                                input_x         <= 0;
                                input_y         <= input_y + 1;
                                input_addr      <= std_logic_vector(input_nlsa);
                                if (unsigned(input_part_width) < to_unsigned(C_M_AXI_BURST_LEN, 32)) then
                                    M_AXI_ARLEN <= std_logic_vector(unsigned(input_part_width(7 downto 0)) - 1);
                                else
                                    M_AXI_ARLEN <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
                                end if;
                                input_nlsa      <= input_nlsa + unsigned(input_image_width_bytes);

                                input_fsm_state <= START_SINGLE_TRANSFER;
                            else
                                if (CONFIG(1) = '0') then
                                    count_read_descr_done <= count_read_descr_done + 1;
                                end if;

                                if (next_iotable_row < C_NUM_IOTABLE_ROWS - 1) then
                                    iotable_row         <= std_logic_vector(next_iotable_row);
                                    next_iotable_row    <= next_iotable_row + 1;
                                    input_fsm_state     <= LOAD_DESCRIPTOR;
                                else
                                    iotable_row         <= (others => '0');
                                    next_iotable_row    <= (others => '0');
                                    input_fsm_state     <= IDLE;
                                    read_txns_done_buf  <= '1';
                                end if;
                            end if;
                        else
                            input_x         <= input_x + C_M_AXI_BURST_LEN;
                            input_addr      <= std_logic_vector(unsigned(input_addr) + burst_size_bytes);
                            if (unsigned(input_part_width) < input_x + (2 * C_M_AXI_BURST_LEN)) then
                                tmp_arlen   := std_logic_vector(unsigned(input_part_width) - input_x - C_M_AXI_BURST_LEN - 1);
                                M_AXI_ARLEN <= tmp_arlen(7 downto 0);
                            else
                                M_AXI_ARLEN <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
                            end if;
                            input_fsm_state <= START_SINGLE_TRANSFER;
                        end if;

                    when others =>
                        input_fsm_state <= IDLE;
                end case;
            end if;
        end if;
    end process;

    SWITCH_PEA_MODE: process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                switch_pea_mode_state       <= IDLE;
                mode_reg_ctr                <= 0;
                pea_config_write_init_txn   <= '0';
                pea_config_write_addr       <= (others => '0');
                pea_config_write_data       <= (others => '0');
                pea_config_read_init_txn    <= '0';
                pea_config_read_addr        <= (others => '0');
                pea_status_pre              <= (others => '0');
                pea_status_post             <= (others => '0');
            else
                case (switch_pea_mode_state) is
                    when IDLE =>
                        mode_reg_ctr <= 0;

                        if (input_fsm_state = ACTIVATE_PROG_MODE
                                or input_fsm_state = ACTIVATE_EXEC_MODE) then
                            pea_config_read_addr        <= CONFIG(31 downto 16) & x"0024";
                            pea_config_read_init_txn    <= '1';

                            switch_pea_mode_state       <= GET_PEA_STATUS_PRE;
                        end if;

                    when GET_PEA_STATUS_PRE =>
                        pea_config_read_init_txn    <= '0';

                        if (pea_config_read_txn_done = '1') then
                            pea_status_pre <= pea_config_read_data;

                            if (input_fsm_state = ACTIVATE_PROG_MODE) then
                                switch_pea_mode_state <= WRITE_PROG_MODE_REGISTER;
                            elsif (input_fsm_state = ACTIVATE_EXEC_MODE) then
                                switch_pea_mode_state <= WRITE_EXEC_MODE_REGISTER;
                            end if;
                        end if;

                    when WRITE_PROG_MODE_REGISTER =>
                        pea_config_write_addr       <= CONFIG(31 downto 16) & prog_mode_reg_offset(mode_reg_ctr);
                        pea_config_write_data       <= prog_mode_reg_data(mode_reg_ctr);
                        pea_config_write_init_txn   <= '1';

                        switch_pea_mode_state       <= PROG_MODE_TXN_DONE;

                    when PROG_MODE_TXN_DONE =>
                        pea_config_write_init_txn   <= '0';

                        if (pea_config_write_txn_done = '1') then
                            if (mode_reg_ctr < C_NUM_MODE_REGS - 1) then
                                mode_reg_ctr                <= mode_reg_ctr + 1;
                                switch_pea_mode_state       <= WRITE_PROG_MODE_REGISTER;
                            else
                                pea_config_read_addr        <= CONFIG(31 downto 16) & x"0024";
                                pea_config_read_init_txn    <= '1';

                                switch_pea_mode_state       <= GET_PEA_STATUS_POST; --MODE_SWITCHED;
                            end if;
                        end if;

                    when WRITE_EXEC_MODE_REGISTER =>
                        pea_config_write_addr       <= CONFIG(31 downto 16) & exec_mode_reg_offset(mode_reg_ctr);
                        pea_config_write_data       <= exec_mode_reg_data(mode_reg_ctr);
                        pea_config_write_init_txn   <= '1';

                        switch_pea_mode_state       <= EXEC_MODE_TXN_DONE;

                    when EXEC_MODE_TXN_DONE =>
                        pea_config_write_init_txn   <= '0';

                        if (pea_config_write_txn_done = '1') then
                            if (mode_reg_ctr < C_NUM_MODE_REGS - 1) then
                                mode_reg_ctr                <= mode_reg_ctr + 1;
                                switch_pea_mode_state       <= WRITE_EXEC_MODE_REGISTER;
                            else
                                pea_config_read_addr        <= CONFIG(31 downto 16) & x"0024";
                                pea_config_read_init_txn    <= '1';

                                switch_pea_mode_state       <= GET_PEA_STATUS_POST; --MODE_SWITCHED;
                            end if;
                        end if;

                    when GET_PEA_STATUS_POST =>
                        pea_config_read_init_txn    <= '0';

                        if (pea_config_read_txn_done = '1') then
                            pea_status_post <= pea_config_read_data;

                            switch_pea_mode_state   <= MODE_SWITCHED;
                        end if;

                    when MODE_SWITCHED =>
                        if (input_fsm_state = INIT_FIRST_TRANSFER) then
                            switch_pea_mode_state <= IDLE;
                        end if;

                    when others =>
                        switch_pea_mode_state <= IDLE;
                end case;
            end if;
        end if;
    end process;


    read_buffer_ready <= read_buffer_ready_ff_prev and (not read_buffer_ready_ff);

    -- buffering the pulse that indicates that one of the read data buffers has data available
    READ_BUF_HANDLER: process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_buffer_ready_ff        <= '0';
                read_buffer_ready_ff_prev   <= '0';
                num_read_buffers_free       <= C_NUM_BUFFERS;
            else
                read_buffer_ready_ff_prev   <= read_buffer_ready_ff;

                if (reads_done = '1') then
                    read_buffer_ready_ff <= '1';

                    if (read_buffer_free = '0') then
                        if (num_read_buffers_free > 0) then
                            num_read_buffers_free <= num_read_buffers_free - 1;
                        end if;
                    end if;
                elsif (read_buffer_free = '1' and reads_done = '0') then
                    read_buffer_ready_ff <= '0';

                    if (num_read_buffers_free < C_NUM_BUFFERS) then
                        num_read_buffers_free <= num_read_buffers_free + 1;
                    end if;
                else
                    read_buffer_ready_ff <= '0';
                end if;
            end if;
        end if;
    end process;


    read_buffer_select <= std_logic_vector(read_buffer_select_buf);

    INPUT_TXN_PROC: process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                input_transfer_state    <= IDLE;
                reads_done              <= '0';
                read_buffer_select_buf  <= (others => '0');
            else
                case (input_transfer_state) is
                    when IDLE =>
                        if (input_fsm_state = START_SINGLE_TRANSFER
                                and num_read_buffers_free > 0) then
                            input_transfer_state <= START_TRANSFER;
                        end if;

                    when START_TRANSFER =>
                        if (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                            input_transfer_state <= READ_DATA;
                        end if;

                    when READ_DATA =>
                        if (M_AXI_RVALID = '1' and M_AXI_RLAST = '1' and axi_rready = '1') then
                            reads_done           <= '1';
                            input_transfer_state <= TXN_DONE;
                        end if;

                    when TXN_DONE =>
                        reads_done <= '0';

                        if (input_fsm_state = INIT_NEXT_TRANSFER) then
                            input_transfer_state <= IDLE;

                            if (read_buffer_select_buf < C_NUM_BUFFERS - 1) then
                                read_buffer_select_buf <= read_buffer_select_buf + 1;
                            else
                                read_buffer_select_buf <= (others => '0');
                            end if;
                        end if;

                    when others =>
                        input_transfer_state <= IDLE;
                end case;
            end if;
        end if;
    end process;

    ------------------
    -- Read Address --
    ------------------

    M_AXI_ARADDR    <= input_addr;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_arvalid <= '0';
            else
                if (axi_arvalid = '0' and input_transfer_state = START_TRANSFER) then
                    axi_arvalid <= '1';
                elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    axi_arvalid <= '0';
                else
                    axi_arvalid <= axi_arvalid;
                end if;
            end if;
        end if;
    end process;

    --------------------------------------
    -- Read Data (and Response) Channel --
    --------------------------------------

    -- Forward movement occurs when the channel is valid and ready
    rnext <= M_AXI_RVALID and axi_rready;

    -- Burst length counter. Uses extra counter register bit to indicate
    -- terminal count to reduce decode logic
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or input_transfer_state = START_TRANSFER) then
                read_index <= (others => '0');
            else
                if (rnext = '1' and (read_index <= to_unsigned(C_M_AXI_BURST_LEN-1, C_TRANSACTIONS_NUM+1))) then
                    read_index <= read_index + 1;
                end if;
            end if;
        end if;
    end process;

    -- The Read Data channel returns the results of the read request
    --
    -- In this example the data checker is always able to accept
    -- more data, so no need to throttle the RREADY signal
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_rready <= '0';
            -- accept/acknowledge rdata/rresp with axi_rready by the master
            -- when M_AXI_RVALID is asserted by slave
            else
                if (M_AXI_RVALID = '1') then
                    if (M_AXI_RLAST = '1' and axi_rready = '1') then
                        axi_rready <= '0';
                    else
                        axi_rready <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;

    read_buffer_addr    <= std_logic_vector(read_buffer_addr_buf);

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or input_transfer_state = START_TRANSFER) then
                read_buffer_addr_buf <= (others => '0');
            else
                if (rnext = '1') then
                    read_buffer_addr_buf <= read_buffer_addr_buf + 1;
                end if;
            end if;
        end if;
    end process;

    read_buffer_data    <= M_AXI_RDATA;
    read_buffer_wren    <= rnext when read_index < line_data_to_read else '0';
    read_buffer_pe_id   <= CONFIG(31 downto 16);
    line_data_to_read   <= conv_integer(input_part_width) - input_x;
    read_buffer_length  <= std_logic_vector(to_unsigned(line_data_to_read, 32))
                           when line_data_to_read < C_M_AXI_BURST_LEN else std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN, 32));
    read_buffer_last    <= '1' when line_data_to_read <= C_M_AXI_BURST_LEN and input_y + 1 >= unsigned(input_part_height) else '0';
    read_buffer_prog    <= CONFIG(1);


    ------------------------------
    -- WRITE BACK TO DDR MEMORY --
    ------------------------------

    write_buffer_select <= std_logic_vector(write_buffer_select_buf);

    OUTPUT_FSM_PROC: process(M_AXI_ACLK)
        variable ot_col_num     : integer;
        variable tmp_u          : unsigned(31 downto 0);
        variable tmp_u_bytes    : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                output_fsm_state        <= IDLE;
                write_buffer_select_buf <= (others => '0');
                ot_out_wr               <= '0';
                ot_out_addr             <= (others => '0');
                ot_out_din              <= (others => '0');
                ot_out_col              <= (others => '0');
                M_AXI_AWADDR            <= (others => '0');
                output_pe_id            <= (others => '1');
                axi_awlen               <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
                buffer_start_idx        <= (others => '0');
                output_length           <= (others => '0');
                output_config           <= (others => '0');
                output_x                <= (others => '0');
                output_y                <= (others => '0');
                output_addr             <= (others => '0');
                output_nlsa             <= (others => '0');
                output_part_start_addr  <= (others => '0');
                output_image_width      <= (others => '0');
                output_part_width       <= (others => '0');
                output_part_height      <= (others => '0');
                output_invalid          <= '1';
                count_write_descr_done  <= 0;
            else
                prev_output_fsm_state <= output_fsm_state;

                if (read_txns_done_ff = '1' and read_txns_done_ff2 = '0') then
                    count_write_descr_done <= 0;
                end if;

                case (output_fsm_state) is
                    when IDLE =>
                        if (num_write_buffers_ready > 0) then
                            if (output_pe_id /= write_buffer_pe_id(OT_ADDR_WIDTH-4 downto 0) or output_invalid = '1') then
                                output_pe_id     <= write_buffer_pe_id(OT_ADDR_WIDTH-4 downto 0);
                                output_fsm_state <= GET_OT_ROW;
                                output_invalid   <= '0';
                            else
                                M_AXI_AWADDR     <= std_logic_vector(unsigned(output_row(1)) + unsigned(output_row(6)));

                                output_fsm_state <= INIT_TRANSFER; --START_SINGLE_TRANSFER;
                            end if;

                            output_length    <= write_buffer_length; -- TODO: use this...
                            ot_out_wr        <= '0';
                            ot_out_addr      <= write_buffer_pe_id(OT_ADDR_WIDTH-4 downto 0) & "000";
                            ot_out_col       <= "000";
                            count_cycles     <= 0;
                            col_count        <= 0;
                        end if;

                    when GET_OT_ROW =>
                        ot_out_addr <= output_pe_id & ot_out_col;

                        if (count_cycles >= 2) then
                            output_row(col_count)   <= ot_out_dout;
                            col_count               <= col_count + 1;
                        end if;

                        count_cycles <= count_cycles + 1;

                        if (ot_out_col = "111") then
                            if (count_cycles >= 9) then
                                output_fsm_state <= INIT_TRANSFER;
                            end if;
                        else
                            ot_out_col <= std_logic_vector(unsigned(ot_out_col) + 1);
                        end if;

                    when INIT_TRANSFER =>
                        output_config               <= output_row(0);
                        output_part_start_addr      <= output_row(1);
                        output_image_width          <= output_row(2);
                        output_x                    <= unsigned(output_row(4));
                        output_y                    <= unsigned(output_row(5));
                        output_addr                 <= output_row(6);
                        output_nlsa                 <= unsigned(output_row(7));

                        output_image_width_bytes    <= output_row(2)(29 downto 0) & "00";

                        --M_AXI_AWADDR                <= std_logic_vector(unsigned(output_row(1)) + unsigned(output_row(6)));

                        -- if linear data
                        if (output_row(0)(1) = '1') then
                            output_part_width   <= output_row(3);
                            output_part_height  <= x"0001";
                        -- if 2d data
                        else
                            output_part_width   <= x"0000" & output_row(3)(15 downto 0);
                            output_part_height  <= output_row(3)(31 downto 16);
                        end if;

                        buffer_start_idx    <= (others => '0');

                        output_fsm_state    <= START_SINGLE_TRANSFER;

                    when START_SINGLE_TRANSFER =>
                        M_AXI_AWADDR    <= std_logic_vector(unsigned(output_row(1)) + unsigned(output_row(6)));

                        if (unsigned(output_length) - buffer_start_idx > unsigned(output_part_width) - output_x) then
                            tmp_u       := unsigned(output_part_width) - output_x - 1;
                            axi_awlen   <= std_logic_vector(tmp_u(7 downto 0));
                        else
                            tmp_u       := unsigned(output_length) - buffer_start_idx - 1;
                            axi_awlen   <= std_logic_vector(tmp_u(7 downto 0));
                        end if;

                        if (output_transfer_state = START_TRANSFER) then
                            output_fsm_state <= WAIT_FOR_TRANSFER_COMPLETION;
                        end if;

                    when WAIT_FOR_TRANSFER_COMPLETION =>
                        if (output_transfer_state = TXN_DONE) then
                            if (write_index < unsigned(output_length)) then
                                buffer_start_idx    <= write_index;

                                output_x            <= (others => '0');
                                output_y            <= output_y + 1;
                                output_addr         <= std_logic_vector(output_nlsa);
                                output_nlsa         <= output_nlsa + unsigned(output_image_width_bytes);
                                output_row(4)       <= (others => '0');
                                output_row(5)       <= std_logic_vector(output_y + 1);
                                output_row(6)       <= std_logic_vector(output_nlsa);
                                output_row(7)       <= std_logic_vector(output_nlsa + unsigned(output_image_width_bytes));

                                --M_AXI_AWADDR        <= std_logic_vector(unsigned(output_row(1)) + unsigned(output_row(6)));
                                output_fsm_state    <= START_SINGLE_TRANSFER;
                            else
                                tmp_u       := unsigned(output_length) - buffer_start_idx;
                                tmp_u_bytes := std_logic_vector(tmp_u(29 downto 0)) & "00";

                                if ((output_x + tmp_u >= unsigned(output_part_width)
                                        and output_y + 1 >= unsigned(output_part_height)) or output_config(0) = '0') then
                                    count_write_descr_done  <= count_write_descr_done + 1;

                                    ot_out_wr               <= '1';
                                    ot_out_addr             <= output_pe_id(OT_ADDR_WIDTH-4 downto 0) & "000";
                                    ot_out_din              <= (others => '0');
                                    output_invalid          <= '1';

                                    output_fsm_state        <= SELECT_NEXT_BUFFER;
                                else
                                    if (output_x + tmp_u >= unsigned(output_part_width)) then
                                        output_x        <= (others => '0');
                                        output_y        <= output_y + 1;
                                        output_addr     <= std_logic_vector(output_nlsa);
                                        output_nlsa     <= output_nlsa + unsigned(output_image_width_bytes);
                                        output_row(4)   <= (others => '0');
                                        output_row(5)   <= std_logic_vector(output_y + 1);
                                        output_row(6)   <= std_logic_vector(output_nlsa);
                                        output_row(7)   <= std_logic_vector(output_nlsa + unsigned(output_image_width_bytes));
                                    else
                                        output_x        <= output_x + tmp_u;
                                        output_addr     <= std_logic_vector(unsigned(output_addr) + unsigned(tmp_u_bytes));
                                        output_row(4)   <= std_logic_vector(output_x + tmp_u);
                                        output_row(6)   <= std_logic_vector(unsigned(output_addr) + unsigned(tmp_u_bytes));
                                    end if;

                                    ot_out_col          <= "000";
                                    output_fsm_state    <= UPDATE_OT;
                                end if;
                            end if;
                        end if;

                    when UPDATE_OT =>
                        ot_col_num  := conv_integer(ot_out_col);
                        ot_out_din  <= output_row(ot_col_num);
                        ot_out_addr <= output_pe_id & ot_out_col;
                        ot_out_wr   <= '1';

                        if (ot_out_col = "111") then
                            output_fsm_state <= SELECT_NEXT_BUFFER;
                        else
                            ot_out_col <= std_logic_vector(unsigned(ot_out_col) + 1);
                        end if;

                    when SELECT_NEXT_BUFFER =>
                        ot_out_wr <= '0';

                        if (write_buffer_select_buf < C_NUM_BUFFERS - 1) then
                            write_buffer_select_buf <= write_buffer_select_buf + 1;
                        else
                            write_buffer_select_buf <= (others => '0');
                        end if;

                        output_fsm_state <= WAIT_FOR_BUFFER_DATA;

                    when WAIT_FOR_BUFFER_DATA =>
                        output_fsm_state <= IDLE;

                    when others =>
                        output_fsm_state <= IDLE;
                end case;
            end if;
        end if;
    end process;


    write_buffer_free <= write_buffer_free_ff_prev and (not write_buffer_free_ff);

    -- buffering the pulse that indicates that one of the write data buffers is free
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                write_buffer_free_ff        <= '0';
                write_buffer_free_ff_prev   <= '0';
                num_write_buffers_ready     <= 0;
            else
                write_buffer_free_ff_prev   <= write_buffer_free_ff;

                if (writes_done = '1' and write_index = unsigned(output_length)) then
                    write_buffer_free_ff <= '1';

                    if (write_buffer_ready = '0') then
                        if (num_write_buffers_ready > 0) then
                            num_write_buffers_ready <= num_write_buffers_ready - 1;
                        end if;
                    end if;
                elsif (write_buffer_ready = '1') then
                    write_buffer_free_ff <= '0';

                    if (num_write_buffers_ready < C_NUM_BUFFERS) then
                        num_write_buffers_ready <= num_write_buffers_ready + 1;
                    end if;
                else
                    write_buffer_free_ff <= '0';
                end if;
            end if;
        end if;
    end process;

    OUTPUT_TXN_PROC: process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                output_transfer_state   <= IDLE;
                writes_done             <= '0';
            else
                case (output_transfer_state) is
                    when IDLE =>
                        if (output_fsm_state = START_SINGLE_TRANSFER) then
                            output_transfer_state   <= START_TRANSFER;
                        end if;

                    when START_TRANSFER =>
                        if (output_config(0) = '0') then
                            writes_done             <= '1';
                            output_transfer_state   <= TXN_DONE;
                        else
                            output_transfer_state   <= WRITE_ADDR;
                        end if;

                    when WRITE_ADDR =>
                        if (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                            output_transfer_state <= WRITE_DATA;
                        end if;

                    when WRITE_DATA =>
                        if (axi_wlast = '1' and M_AXI_WREADY = '1') then
                            writes_done           <= '1';
                            output_transfer_state <= GET_WRITE_RESPONSE;
                        end if;

                    when GET_WRITE_RESPONSE =>
                        writes_done <= '0';

                        if (response = '1') then --M_AXI_BVALID = '1' and axi_bready = '1') then
                            output_transfer_state <= TXN_DONE;
                        end if;

                    when TXN_DONE =>
                        writes_done <= '0';

                        if (output_fsm_state /= WAIT_FOR_TRANSFER_COMPLETION) then
                            output_transfer_state <= IDLE;
                        end if;

                    when others =>
                        output_transfer_state <= IDLE;
                end case;
            end if;
        end if;
    end process;


    ---------------------------
    -- Write Address Channel --
    ---------------------------

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or output_fsm_state = INIT_TRANSFER) then
                axi_awvalid <= '0';
            else
                if (axi_awvalid = '0' and output_transfer_state = WRITE_ADDR) then
                    axi_awvalid <= '1';
                elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    axi_awvalid <= '0';
                else
                    axi_awvalid <= axi_awvalid;
                end if;
            end if;
        end if;
    end process;


    ------------------------
    -- Write Data Channel --
    ------------------------

    wnext <= M_AXI_WREADY and axi_wvalid;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or output_fsm_state = INIT_TRANSFER) then
                axi_wvalid <= '0';
            else
                if (wnext = '1') then
                    axi_wvalid <= '0';
                elsif (output_transfer_state = WRITE_DATA) then
                    axi_wvalid <= '1';
                else
                    axi_wvalid <= '0';
                end if;
            end if;
        end if;
    end process;

    write_buffer_addr   <= std_logic_vector(write_buffer_addr_buf);

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or output_fsm_state = INIT_TRANSFER) then
                write_index             <= (others => '0');
                write_buffer_addr_buf   <= (others => '0');
                row_write_index         <= (others => '0');
            else
                if (wnext = '1') then
                    write_index             <= write_index + 1;
                    write_buffer_addr_buf   <= write_buffer_addr_buf + 1;

                    if (axi_wlast = '1' and (row_write_index + output_x = unsigned(output_part_width) - 1)) then
                        row_write_index <= (others => '0');
                    else
                        row_write_index <= row_write_index + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or output_fsm_state = INIT_TRANSFER) then
                axi_wlast <= '0';
            -- axi_wlast is asserted when the write index
            -- count reaches the penultimate count to synchronize
            -- with the last write data when write_index is b1111
            -- elsif (&(write_index[C_TRANSACTIONS_NUM-1:1])&& ~write_index[0] && wnext)
            else
                if (prev_output_fsm_state = WAIT_FOR_TRANSFER_COMPLETION and output_fsm_state /= WAIT_FOR_TRANSFER_COMPLETION) then
                    axi_wlast <= '0';
                elsif (((((write_index = to_unsigned(C_M_AXI_BURST_LEN-1, 32)) or (row_write_index + output_x = unsigned(output_part_width) - 1)) and C_M_AXI_BURST_LEN >= 2) and wnext = '0') or (C_M_AXI_BURST_LEN = 1)) then
                    axi_wlast <= '1';
                -- Deassrt axi_wlast when the last write data has been
                -- accepted by the slave with a valid response
                elsif (wnext = '1') then
                    axi_wlast <= '0';
                elsif (axi_wlast = '1' and C_M_AXI_BURST_LEN = 1) then
                    axi_wlast <= '0';
                end if;
            end if;
        end if;
    end process;


    ----------------------------
    -- Write Response Channel --
    ----------------------------

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or output_transfer_state = TXN_DONE) then
                axi_bready  <= '0';
                response    <= '0';
            -- accept/acknowledge bresp with axi_bready by the master
            -- when M_AXI_BVALID is asserted by slave
            else
                if (M_AXI_BVALID = '1' and axi_bready = '0') then
                    axi_bready  <= '1';
                    response    <= '1';
                -- deassert after one clock cycle
                elsif (axi_bready = '1') then
                    axi_bready  <= '0';
                end if;
            end if;
        end if;
    end process;

end implementation;
