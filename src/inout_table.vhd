library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity inout_table is
    generic(
        C_ROWS          : integer   := 16
    );
    port(
        -- the clock
        clk             : in  std_logic;

        -- reset (active low)
        resetn          : in  std_logic;

        -- if we = '1', then the table will be filled
        we              : in  std_logic;
        ADDR_IN         : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
        DATA_IN         : in  std_logic_vector(31 downto 0);

        -- if re = '1', data will be read from the table
        re              : in  std_logic;
        ADDR_OUT        : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
        DATA_OUT        : out std_logic_vector(31 downto 0);

        -- the row of the table entries, that should be read
        ROW             : in  std_logic_vector(28 downto 0);

        -- output of the table row that is refered to with ROW
        CONFIG                      : out std_logic_vector(31 downto 0);
        R_PARTITION_START_ADDR      : out std_logic_vector(31 downto 0);
        R_IMAGE_WIDTH               : out std_logic_vector(31 downto 0);
        R_PARTITION_DIMENSIONS      : out std_logic_vector(31 downto 0);
        W_PARTITION_START_ADDR      : out std_logic_vector(31 downto 0);
        W_IMAGE_WIDTH               : out std_logic_vector(31 downto 0);
        W_PARTITION_DIMENSIONS      : out std_logic_vector(31 downto 0)
    );
end inout_table;

architecture bhv of inout_table is

    type TABLE_TYPE is array((C_ROWS*8)-1 downto 0) of std_logic_vector(31 downto 0);
    shared variable table : TABLE_TYPE;

    constant ADDR_BITS : integer := log2(C_ROWS*8*4);

begin

    process(clk)
        variable idx : integer;
    begin
        if rising_edge(clk) then
            if (resetn = '0') then
                for i in 0 to C_ROWS-1 loop
                    table(8 * i) := (others => '0');
                end loop;
            else
                if (we = '1') then
                    if (unsigned(ADDR_IN(ADDR_BITS-1 downto 2)) <= to_unsigned((C_ROWS * 8) - 1, ADDR_BITS - 2)) then
                        idx := to_integer(unsigned(ADDR_IN(ADDR_BITS-1 downto 2)));

                        table(idx) := std_logic_vector(unsigned(DATA_IN));
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(clk)
        variable idx : integer;
    begin
        if rising_edge(clk) then
            if (resetn = '0') then
                DATA_OUT <= (others => '0');
            else
                if (re = '1') then
                    if (unsigned(ADDR_OUT(ADDR_BITS-1 downto 2)) <= to_unsigned((C_ROWS * 8) - 1, ADDR_BITS - 2)) then
                        idx := to_integer(unsigned(ADDR_OUT(ADDR_BITS-1 downto 2)));

                        DATA_OUT <= table(idx);
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(clk)
        variable row_addr   : std_logic_vector(31 downto 0);
        variable i_row_addr : integer;
    begin
        if (rising_edge(clk)) then
            if (resetn = '0') then
                CONFIG                  <= x"0000000E";
                R_PARTITION_START_ADDR  <= (others => '0');
                R_IMAGE_WIDTH           <= (others => '0');
                R_PARTITION_DIMENSIONS  <= (others => '0');
                W_PARTITION_START_ADDR  <= (others => '0');
                W_IMAGE_WIDTH           <= (others => '0');
                W_PARTITION_DIMENSIONS  <= (others => '0');
            else
                if (unsigned(ROW) <= to_unsigned((C_ROWS * 8) - 1, 29)) then
                    row_addr    := ROW & "000";
                    i_row_addr  := to_integer(unsigned(row_addr));

                    CONFIG                  <= table(i_row_addr);
                    R_PARTITION_START_ADDR  <= table(i_row_addr + 1);
                    R_IMAGE_WIDTH           <= table(i_row_addr + 2);
                    R_PARTITION_DIMENSIONS  <= table(i_row_addr + 3);
                    W_PARTITION_START_ADDR  <= table(i_row_addr + 4);
                    W_IMAGE_WIDTH           <= table(i_row_addr + 5);
                    W_PARTITION_DIMENSIONS  <= table(i_row_addr + 6);
                else
                    CONFIG                  <= x"000000EE";
                    R_PARTITION_START_ADDR  <= (others => '0');
                    R_IMAGE_WIDTH           <= (others => '0');
                    R_PARTITION_DIMENSIONS  <= (others => '0');
                    W_PARTITION_START_ADDR  <= (others => '0');
                    W_IMAGE_WIDTH           <= (others => '0');
                    W_PARTITION_DIMENSIONS  <= (others => '0');
                end if;
            end if;
        end if;
    end process;

end bhv;
