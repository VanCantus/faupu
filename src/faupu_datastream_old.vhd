library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_datastream is
    generic (
        -- Users to add parameters here

        C_BUFFER_SELECT_LENGTH      : integer   := 1;
        C_NUM_BUFFERS               : integer   := 2;
        C_BUFFER_ADDR_WIDTH         : integer   := 16;
        C_NUM_PE_ARRAYS             : integer   := 2;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
        C_M_AXI_BURST_LEN           : integer   := 16;
        -- Thread ID Width
        C_M_AXI_ID_WIDTH            : integer   := 1;
        -- Width of Address Bus
        C_M_AXI_ADDR_WIDTH          : integer   := 32;
        -- Width of Data Bus
        C_M_AXI_DATA_WIDTH          : integer   := 32;
        -- Width of User Write Address Bus
        C_M_AXI_AWUSER_WIDTH        : integer   := 0;
        -- Width of User Read Address Bus
        C_M_AXI_ARUSER_WIDTH        : integer   := 0;
        -- Width of User Write Data Bus
        C_M_AXI_WUSER_WIDTH         : integer   := 0;
        -- Width of User Read Data Bus
        C_M_AXI_RUSER_WIDTH         : integer   := 0;
        -- Width of User Response Bus
        C_M_AXI_BUSER_WIDTH         : integer   := 0
    );
    port (
        -- Users to add ports here

        -- debug
        debug_out0          : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        debug_out1          : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

        iotable_filled      : in  std_logic;
        iotable_filled_ack  : out std_logic;
        read_txns_done      : out std_logic;

        -- indicates if there is data ready in one of the write data buffers for writing back to the RAM
        write_buffer_ready  : in  std_logic;
        -- indicates if data is done writing to one of the read data buffers and ready for read by the CU
        read_buffer_ready   : out std_logic;

        -- indicates if one of the write data buffers is able to accept a single burst from the CU
        write_buffer_free   : out std_logic;
        -- indicates if one of the read data buffers is able to accept a single burst from the RAM
        read_buffer_free    : in  std_logic;

        -- select either buffer 0 or 1
        write_buffer_select : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        -- Address for getting data from the write data buffer
        write_buffer_addr   : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        -- Data from the write data buffer
        write_buffer_data   : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- the ID of the PE array from where the data is coming
        write_buffer_pe_id  : in  std_logic_vector(15 downto 0); -- TODO: length of the signal
        write_buffer_length : in  std_logic_vector(31 downto 0);
        write_buffer_last   : in  std_logic;

        -- select either buffer 0 or 1
        read_buffer_select  : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        -- Address for writing data to the read data buffer
        read_buffer_addr    : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        -- Data to the read data buffer
        read_buffer_data    : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- the ID of the PE array to where the data has to be written
        read_buffer_pe_id   : out std_logic_vector(15 downto 0); -- TODO: length of the signal
        read_buffer_length  : out std_logic_vector(31 downto 0);
        read_buffer_prog    : out std_logic;
        read_buffer_last    : out std_logic;
        -- write enable for the read data buffer
        read_buffer_wren    : out std_logic;

        -- row from where to read from the IO table
        iotable_row         : out std_logic_vector(28 downto 0);

        -- output of the table row that is refered to with iotable_row
        CONFIG                      : in  std_logic_vector(31 downto 0);
        R_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
        R_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
        R_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);
        W_PARTITION_START_ADDR      : in  std_logic_vector(31 downto 0);
        W_IMAGE_WIDTH               : in  std_logic_vector(31 downto 0);
        W_PARTITION_DIMENSIONS      : in  std_logic_vector(31 downto 0);

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global Clock Signal.
        M_AXI_ACLK      : in  std_logic;
        -- Global Reset Singal. This Signal is Active Low
        M_AXI_ARESETN   : in  std_logic;
        -- Master Interface Write Address ID
        M_AXI_AWID      : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Address
        M_AXI_AWADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_AWLEN     : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_AWSIZE    : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_AWBURST   : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_AWLOCK    : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_AWCACHE   : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_AWPROT    : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each write transaction.
        M_AXI_AWQOS     : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the write address channel.
        M_AXI_AWUSER    : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid write address and control information.
        M_AXI_AWVALID   : out std_logic;
        -- Write address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_AWREADY   : in  std_logic;
        -- Master Interface Write ID
        M_AXI_WID       : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Data.
        M_AXI_WDATA     : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte
        -- lanes hold valid data. There is one write strobe
        -- bit for each eight bits of the write data bus.
        M_AXI_WSTRB     : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        -- Write last. This signal indicates the last transfer in a write burst.
        M_AXI_WLAST     : out std_logic;
        -- Optional User-defined signal in the write data channel.
        M_AXI_WUSER     : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available
        M_AXI_WVALID    : out std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        M_AXI_WREADY    : in  std_logic;
        -- Master Interface Write Response.
        M_AXI_BID       : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Write response. This signal indicates the status of the write transaction.
        M_AXI_BRESP     : in  std_logic_vector(1 downto 0);
        -- Optional User-defined signal in the write response channel
        M_AXI_BUSER     : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        -- Write response valid. This signal indicates that the
        -- channel is signaling a valid write response.
        M_AXI_BVALID    : in  std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        M_AXI_BREADY    : out std_logic;
        -- Master Interface Read Address.
        M_AXI_ARID      : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Read address. This signal indicates the initial
        -- address of a read burst transaction.
        M_AXI_ARADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_ARLEN     : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_ARSIZE    : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_ARBURST   : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_ARLOCK    : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_ARCACHE   : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_ARPROT    : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each read transaction
        M_AXI_ARQOS     : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the read address channel.
        M_AXI_ARUSER    : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid read address and control information
        M_AXI_ARVALID   : out std_logic;
        -- Read address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_ARREADY   : in  std_logic;
        -- Read ID tag. This signal is the identification tag
        -- for the read data group of signals generated by the slave.
        M_AXI_RID       : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Read Data
        M_AXI_RDATA     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the read transfer
        M_AXI_RRESP     : in  std_logic_vector(1 downto 0);
        -- Read last. This signal indicates the last transfer in a read burst
        M_AXI_RLAST     : in  std_logic;
        -- Optional User-defined signal in the read address channel.
        M_AXI_RUSER     : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        -- Read valid. This signal indicates that the channel
        -- is signaling the required read data.
        M_AXI_RVALID    : in  std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        M_AXI_RREADY    : out std_logic
    );
end faupu_datastream;

architecture implementation of faupu_datastream is

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
     constant  C_TRANSACTIONS_NUM   : integer := clogb2(C_M_AXI_BURST_LEN-1);
    -- Example State machine to initialize counter, initialize write transactions,
     -- initialize read transactions and comparison of read data with the
     -- written data words.
     type write_exec_state_type is ( IDLE,      -- This state initiates AXI4Lite transaction
                                                -- after the state machine changes state to INIT_WRITE
                                    FETCHING,
                                    INIT_WRITE, -- This state initializes write transaction
                                                -- once write are done, the state machine
                                                -- changes state back to IDLE
                                    WRITING, SWITCH_BUFFER);
     type read_exec_state_type is ( IDLE,       -- This state initiates AXI4Lite transaction
                                                -- after the state machine changes state to INIT_READ
                                    INIT_READ,  -- This state initializes read transaction
                                                -- once reads are done, the state machine
                                                -- changes state back to IDLE
                                    SWITCH_BUFFER);

     signal mst_write_exec_state : write_exec_state_type;
     signal mst_read_exec_state  : read_exec_state_type;

     --type select_buffer_state is (NONE, BUFFER0, BUFFER1);
     --signal select_write_buffer     : select_buffer_state;
     --signal select_read_buffer      : select_buffer_state;

     -- indicates how many of the write buffers have data available
     signal num_write_buffers_ready : integer;
     -- indicates how many of the read buffers are empty
     signal num_read_buffers_free   : integer;

    -- AXI4FULL signals
    --AXI4 internal temp signals
    signal axi_awaddr               : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_awvalid              : std_logic;
    signal axi_wdata                : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_wlast                : std_logic;
    signal axi_wvalid               : std_logic;
    signal axi_bready               : std_logic;
    signal axi_araddr               : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal current_line_offset      : std_logic_vector(C_M_AXI_ADDR_WIDTH-3 downto 0);
    signal axi_arvalid              : std_logic;
    signal axi_rready               : std_logic;
    --write beat count in a burst
    signal write_index              : std_logic_vector(C_TRANSACTIONS_NUM downto 0);
    --read beat count in a burst
    signal read_index               : std_logic_vector(C_TRANSACTIONS_NUM downto 0);
    --size of C_M_AXI_BURST_LEN length burst in bytes
    signal burst_size_bytes         : unsigned(31 downto 0);
    signal start_single_burst_write : std_logic;
    signal start_single_burst_read  : std_logic;
    signal writes_done              : std_logic;
    signal reads_done               : std_logic;
    signal burst_write_active       : std_logic;
    signal burst_read_active        : std_logic;
    --Interface response error flags
    signal write_resp_error         : std_logic;
    signal read_resp_error          : std_logic;
    signal wnext                    : std_logic;
    signal rnext                    : std_logic;

    -- signals for counting the x and y coordinates in the current partition
    signal read_data_count_x        : unsigned(31 downto 0);
    signal read_data_count_y        : unsigned(31 downto 0);
    signal write_data_count_x       : unsigned(31 downto 0);
    signal write_data_count_y       : unsigned(31 downto 0);

    -- signals for splitting the R_PARTITION_DIMENSIONS signal
    signal current_partition_width  : std_logic_vector(31 downto 0);
    signal current_partition_height : std_logic_vector(31 downto 0);
    signal partition_width_write    : std_logic_vector(31 downto 0);
    signal partition_height_write   : std_logic_vector(31 downto 0);

    signal current_line_write_start_addr    : std_logic_vector(31 downto 0);
    signal current_line_read_start_addr     : std_logic_vector(31 downto 0);
    signal next_line_write_start_addr       : std_logic_vector(31 downto 0);
    signal next_line_read_start_addr        : std_logic_vector(31 downto 0);

    signal image_width_write_bytes          : std_logic_vector(31 downto 0);
    signal image_width_read_bytes           : std_logic_vector(31 downto 0);
    signal partition_width_write_bytes      : std_logic_vector(31 downto 0);
    signal partition_width_read_bytes       : std_logic_vector(31 downto 0);

    -- memory for storing the current write transaction for each PE array
    type write_txns_mem_type is array ((C_NUM_PE_ARRAYS*8)-1 downto 0) of std_logic_vector(31 downto 0);
    signal write_txns_mem : write_txns_mem_type;

    signal iotab_pe_array_id    : std_logic_vector(15 downto 0);

    signal reg_config                       : std_logic_vector(31 downto 0);
    signal reg_w_partition_start_addr       : std_logic_vector(31 downto 0);
    signal reg_w_image_width                : std_logic_vector(31 downto 0);
    signal reg_w_partition_dimensions       : std_logic_vector(31 downto 0);

    -- offset in the current write line
    signal write_txn_addr_offset_in_line    : std_logic_vector(31 downto 0);

    signal line_data_to_read        : integer;
    signal read_tlast               : std_logic;

    signal write_tlast              : std_logic;

    signal iotab_data_fetched       : std_logic;
    signal iotab_fetching_data      : std_logic;
    signal iotab_fetching_data_prev : std_logic;

    signal write_mem_data_fetched       : std_logic;
    signal write_mem_fetching_data      : std_logic;
    signal write_mem_fetching_data_prev : std_logic;

    type read_fetching_state_type is (WAIT_FOR_IOTAB, FETCHING, READING_DATA, FETCH_NEXT);
    signal iotab_fetching_state     : read_fetching_state_type;

    type write_fetching_state_type is (FETCHING, WRITING_DATA);
    signal write_mem_fetching_state : write_fetching_state_type;

    signal write_txns_done          : std_logic;

    signal read_buffer_addr_next    : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);

    signal wait_for_next_read_burst     : std_logic;
    signal wait_for_next_write_burst    : std_logic;

    signal write_count_in_line      : integer;
    signal write_buffer_count       : integer;

    type write_fetch_state is (IDLE, SEND_ADDR, FETCHING, WAIT_FOR_RESPONSE);
    signal write_data_fetch_state   : write_fetch_state;

    signal goto_next_line_read      : std_logic;

    signal iotable_row_buf          : std_logic_vector(28 downto 0);
    signal write_buffer_addr_buf    : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal read_buffer_wren_buf     : std_logic;
    signal write_buffer_select_buf  : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal read_buffer_select_buf   : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);

    signal read_buffer_ready_ff         : std_logic;
    signal read_buffer_ready_ff_prev    : std_logic;
    signal write_buffer_free_ff         : std_logic;
    signal write_buffer_free_ff_prev    : std_logic;

    signal cur_txn_done                 : std_logic;

    signal read_txns_done_buf       : std_logic;

    signal debug_0_out0_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_1_out0_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_2_out0_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_3_out0_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_0_out1_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_1_out1_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_2_out1_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);
    signal debug_3_out1_buf         : std_logic_vector((C_M_AXI_DATA_WIDTH/4)-1 downto 0);

begin
    -- I/O Connections assignments

    debug_out0          <= debug_3_out0_buf & debug_2_out0_buf & debug_1_out0_buf & debug_0_out0_buf;
    debug_out1          <= debug_3_out1_buf & debug_2_out1_buf & debug_1_out1_buf & debug_0_out1_buf;

    read_txns_done      <= read_txns_done_buf;

    --I/O Connections. Write Address (AW)
    M_AXI_AWID          <= (others => '0');
    --The AXI address is a concatenation of the target base address + active offset range
    --M_AXI_AWADDR        <= std_logic_vector(unsigned(current_line_write_start_addr) + unsigned(axi_awaddr));
    --Burst LENgth is number of transaction beats, minus 1
    M_AXI_AWLEN         <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    M_AXI_AWSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8)-1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_AWBURST       <= "01";
    M_AXI_AWLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_AWCACHE       <= "0010";
    M_AXI_AWPROT        <= "000";
    M_AXI_AWQOS         <= x"0";
    M_AXI_AWUSER        <= (others => '1');
    M_AXI_AWVALID       <= axi_awvalid;
    --Write Data(W)
    M_AXI_WID           <= (others => '0');
    M_AXI_WDATA         <= axi_wdata;
    --All bursts are complete and aligned in this example
    M_AXI_WSTRB         <= (others => '1');
    M_AXI_WLAST         <= axi_wlast;
    M_AXI_WUSER         <= (others => '0');
    M_AXI_WVALID        <= axi_wvalid;
    --Write Response (B)
    M_AXI_BREADY        <= axi_bready;
    --Read Address (AR)
    M_AXI_ARID          <= (others => '0');
    M_AXI_ARADDR        <= std_logic_vector(unsigned(current_line_read_start_addr) + unsigned(axi_araddr));
    --Burst LENgth is number of transaction beats, minus 1
    M_AXI_ARLEN         <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    M_AXI_ARSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8)-1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_ARBURST       <= "01";
    M_AXI_ARLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_ARCACHE       <= "0010";
    M_AXI_ARPROT        <= "000";
    M_AXI_ARQOS         <= x"0";
    M_AXI_ARUSER        <= (others => '1');
    M_AXI_ARVALID       <= axi_arvalid;
    --Read and Read Response (R)
    M_AXI_RREADY        <= axi_rready;
    --Burst size in bytes
    burst_size_bytes    <= to_unsigned((C_M_AXI_BURST_LEN * (C_M_AXI_DATA_WIDTH/8)), 32);

    iotable_row         <= iotable_row_buf;
    write_buffer_addr   <= write_buffer_addr_buf;
    read_buffer_wren    <= read_buffer_wren_buf;
    write_buffer_select <= write_buffer_select_buf;
    read_buffer_select  <= read_buffer_select_buf;

    current_partition_width(15 downto  0)   <= R_PARTITION_DIMENSIONS(15 downto  0) when CONFIG(5) = '0' else R_PARTITION_DIMENSIONS(15 downto 0);
    current_partition_width(31 downto 16)   <= (others => '0') when CONFIG(5) = '0' else R_PARTITION_DIMENSIONS(31 downto 16);
    current_partition_height(15 downto  0)  <= R_PARTITION_DIMENSIONS(31 downto 16) when CONFIG(5) = '0' else (others => '0');
    current_partition_height(31 downto 16)  <= (others => '0');

    partition_width_write(15 downto  0)     <= reg_w_partition_dimensions(15 downto  0) when reg_config(5) = '0' else reg_w_partition_dimensions(15 downto 0);
    partition_width_write(31 downto 16)     <= (others => '0') when reg_config(5) = '0' else reg_w_partition_dimensions(31 downto 16);
    partition_height_write(15 downto  0)    <= reg_w_partition_dimensions(31 downto 16) when reg_config(5) = '0' else (others => '0');
    partition_height_write(31 downto 16)    <= (others => '0');

    image_width_write_bytes     <= reg_w_image_width(29 downto 0) & "00";
    image_width_read_bytes      <= R_IMAGE_WIDTH(29 downto 0) & "00";
    partition_width_read_bytes  <= current_partition_width(29 downto 0) & "00";
    partition_width_write_bytes <= partition_width_write(29 downto 0) & "00";

    next_line_read_start_addr   <= std_logic_vector(unsigned(current_line_read_start_addr) + unsigned(image_width_read_bytes));

    iotab_pe_array_id           <= CONFIG(31 downto 16);


    DEBUG_PROC: process(M_AXI_ACLK)
        variable mem_index : integer;
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                debug_0_out0_buf <= (others => '0');
                debug_1_out0_buf <= (others => '0');
                debug_2_out0_buf <= (others => '0');
                debug_3_out0_buf <= (others => '0');

                debug_0_out1_buf <= (others => '0');
                debug_1_out1_buf <= (others => '0');
                debug_2_out1_buf <= (others => '0');
                debug_3_out1_buf <= (others => '0');
            else
                debug_0_out0_buf <= std_logic_vector(to_unsigned(num_write_buffers_ready, C_M_AXI_DATA_WIDTH/4));
                debug_1_out0_buf <= std_logic_vector(to_unsigned(num_read_buffers_free, C_M_AXI_DATA_WIDTH/4));

                if (writes_done = '1') then --reads_done = '1') then
                    debug_2_out0_buf <= std_logic_vector(unsigned(debug_2_out0_buf) + 1);
                end if;

                if (start_single_burst_write = '1') then --writes_done = '1') then
                    debug_3_out0_buf <= std_logic_vector(unsigned(debug_3_out0_buf) + 1);
                end if;

                --if (mst_read_exec_state = IDLE) then
                --    debug_0_out1_buf <= std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH/4));
                --elsif (mst_read_exec_state = INIT_READ) then
                --    debug_0_out1_buf <= std_logic_vector(to_unsigned(2, C_M_AXI_DATA_WIDTH/4));
                --elsif (mst_read_exec_state = SWITCH_BUFFER) then
                --    debug_0_out1_buf <= std_logic_vector(to_unsigned(3, C_M_AXI_DATA_WIDTH/4));
                --else
                --    debug_0_out1_buf <= std_logic_vector(to_unsigned(8, C_M_AXI_DATA_WIDTH/4));
                --end if;
                if (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    debug_0_out1_buf <= std_logic_vector(unsigned(debug_0_out1_buf) + 1);
                end if;

                if (mst_write_exec_state = IDLE) then
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH/4));
                elsif (mst_write_exec_state = FETCHING) then
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(2, C_M_AXI_DATA_WIDTH/4));
                    --debug_2_out1_buf <= (others => '0');
                elsif (mst_write_exec_state = INIT_WRITE) then
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(3, C_M_AXI_DATA_WIDTH/4));
                elsif (mst_write_exec_state = WRITING) then
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(4, C_M_AXI_DATA_WIDTH/4));
                elsif (mst_write_exec_state = SWITCH_BUFFER) then
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(5, C_M_AXI_DATA_WIDTH/4));
                else
                    debug_1_out1_buf <= std_logic_vector(to_unsigned(8, C_M_AXI_DATA_WIDTH/4));
                end if;
                --if (mst_write_exec_state = FETCHING) then
                --    debug_1_out1_buf <= (others => '0');
                --elsif (start_single_burst_write = '1' and axi_awvalid = '0') then
                --    debug_1_out1_buf <= std_logic_vector(to_unsigned(1, C_M_AXI_DATA_WIDTH/4));
                --elsif (unsigned(debug_1_out1_buf) = 1 and axi_awvalid = '0') then
                --    debug_1_out1_buf <= std_logic_vector(to_unsigned(2, C_M_AXI_DATA_WIDTH/4));
                --elsif (unsigned(debug_1_out1_buf) = 0) then
                --    debug_1_out1_buf <= std_logic_vector(to_unsigned(3, C_M_AXI_DATA_WIDTH/4));
                --end if;

                --if (mst_write_exec_state = FETCHING) then
                --    debug_2_out1_buf <= (others => '0');
                --elsif (start_single_burst_write = '1') then
                --    debug_2_out1_buf <= std_logic_vector(unsigned(debug_2_out1_buf) + 1);
                --end if;
                debug_2_out1_buf(0) <= axi_awvalid;
                debug_2_out1_buf(1) <= start_single_burst_write;
                debug_2_out1_buf(2) <= burst_write_active;
                debug_2_out1_buf(3) <= write_mem_fetching_data;

                if (M_AXI_BVALID = '1' and axi_bready = '1') then
                    debug_3_out1_buf(1 downto 0) <= M_AXI_BRESP;
                    debug_3_out1_buf((C_M_AXI_DATA_WIDTH/4)-1 downto 2) <= (others => '0');
                end if;
                --if (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                --    debug_3_out1_buf <= std_logic_vector(unsigned(debug_3_out1_buf) + 1);
                --end if;
            end if;
        end if;
    end process;

    -- process for fetching data from the IO table
    process(M_AXI_ACLK)
        variable mem_index          : integer;
        variable write_txn_config   : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                for i in 0 to (C_NUM_PE_ARRAYS*8)-1 loop
                    write_txns_mem(i)   <= (others => '0');
                end loop;
            else
                if (write_mem_fetching_state = WRITING_DATA and write_tlast = '1') then
                    if (unsigned(write_buffer_pe_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                        mem_index := 0;
                    else
                        mem_index := conv_integer(write_buffer_pe_id & "000");
                    end if;

                    write_txns_mem(mem_index)       <= (others => '0');
                    write_txns_mem(mem_index + 1)   <= (others => '0');
                    write_txns_mem(mem_index + 2)   <= (others => '0');
                    write_txns_mem(mem_index + 3)   <= (others => '0');
                    write_txns_mem(mem_index + 4)   <= (others => '0');
                    write_txns_mem(mem_index + 5)   <= (others => '0');
                    write_txns_mem(mem_index + 6)   <= (others => '0');
                elsif (mst_write_exec_state = SWITCH_BUFFER and cur_txn_done = '0') then
                    if (unsigned(write_buffer_pe_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                        mem_index := 0;
                    else
                        mem_index := conv_integer(write_buffer_pe_id & "000");
                    end if;

                    write_txns_mem(mem_index + 1) <= current_line_write_start_addr;
                    write_txns_mem(mem_index + 4) <= axi_awaddr;
                    write_txns_mem(mem_index + 5) <= std_logic_vector(write_data_count_x);
                    write_txns_mem(mem_index + 6) <= std_logic_vector(write_data_count_y);
                elsif (iotab_fetching_state = FETCHING and CONFIG(0) = '1') then
                    if (unsigned(iotab_pe_array_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                        mem_index := 0;
                    else
                        mem_index := conv_integer(iotab_pe_array_id & "000");
                    end if;
                    write_txn_config := write_txns_mem(mem_index);

                    ---- if there is no process running on the PE array,
                    ---- the next process can be started and the stuff needed
                    ---- for the write transactions have to be stored
                    if (write_txn_config(0) = '0') then
                        -- if not programming data (and output length > 0)
                        if (CONFIG(1) = '0' and unsigned(W_PARTITION_DIMENSIONS) > 0) then
                            -- hold write transaction for this PE array in the memory
                            write_txns_mem(mem_index)(31 downto 1)  <= CONFIG(31 downto 1);
                            write_txns_mem(mem_index)(0)            <= '1';
                            write_txns_mem(mem_index + 1)           <= W_PARTITION_START_ADDR;
                            write_txns_mem(mem_index + 2)           <= W_IMAGE_WIDTH;
                            write_txns_mem(mem_index + 3)           <= W_PARTITION_DIMENSIONS;
                            write_txns_mem(mem_index + 4)           <= (others => '0');
                            write_txns_mem(mem_index + 5)           <= (others => '0');
                            write_txns_mem(mem_index + 6)           <= (others => '0');
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;


    write_mem_data_fetched <= (not write_mem_fetching_data) and write_mem_fetching_data_prev;

    ---- process for fetching data from the IO table
    --process(M_AXI_ACLK)
    --    variable mem_index          : integer;
    --    variable write_txn_config   : std_logic_vector(31 downto 0);
    --begin
    --    if (rising_edge(M_AXI_ACLK)) then
    --        if (M_AXI_ARESETN = '0') then
    --            write_mem_fetching_data         <= '1';
    --            write_mem_fetching_data_prev    <= '1';
    --            write_mem_fetching_state        <= FETCHING;
    --        else
    --            write_mem_fetching_data_prev    <= write_mem_fetching_data;

    --            case (write_mem_fetching_state) is
    --                when FETCHING =>
    --                    if (mst_write_exec_state = FETCHING) then
    --                        write_mem_fetching_data     <= '1';
    --                    elsif (mst_write_exec_state = INIT_WRITE) then
    --                        write_mem_fetching_data     <= '0';
    --                        write_mem_fetching_state    <= WRITING_DATA;
    --                    else
    --                        write_mem_fetching_state    <= FETCHING;
    --                    end if;

    --                when WRITING_DATA =>
    --                    if (writes_done = '1') then --write_tlast = '1') then
    --                        write_mem_fetching_state <= FETCHING;
    --                    else
    --                        write_mem_fetching_state <= WRITING_DATA;
    --                    end if;

    --                when others =>
    --                    write_mem_fetching_state <= FETCHING;

    --            end case;
    --        end if;
    --    end if;
    --end process;


    iotab_data_fetched  <= (not iotab_fetching_data) and iotab_fetching_data_prev;

    -- process for fetching data from the IO table
    process(M_AXI_ACLK)
        variable mem_index          : integer;
        variable write_txn_config   : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                iotab_fetching_data         <= '1';
                iotab_fetching_data_prev    <= '1';
                iotab_fetching_state        <= WAIT_FOR_IOTAB;
                iotable_row_buf             <= (others => '0');
                read_txns_done_buf          <= '0';
                table_filled_ack            <= '0';
            else
                table_filled_ack <= '0';

                case (iotab_fetching_state) is
                    when WAIT_FOR_IOTAB =>
                        iotab_fetching_data         <= '1';
                        iotab_fetching_data_prev    <= '1';
                        read_txns_done_buf          <= '0';

                        if (table_filled = '1') then
                            table_filled_ack        <= '1';
                            iotab_fetching_state    <= FETCHING;
                            iotable_row_buf         <= (others => '0');
                        else
                            iotab_fetching_state    <= WAIT_FOR_IOTAB;
                        end if;

                    when FETCHING =>
                        -- if table row is valid
                        if (write_mem_fetching_state /= WRITING_DATA or write_tlast = '0' or mst_write_exec_state /= SWITCH_BUFFER or cur_txn_done = '1') then
                            if (CONFIG(0) = '1') then
                                if (unsigned(iotab_pe_array_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                                    mem_index := 0;
                                else
                                    mem_index := conv_integer(iotab_pe_array_id & "000");
                                end if;
                                write_txn_config := write_txns_mem(mem_index);

                                ---- if there is no process running on the PE array,
                                ---- the next process can be started and the stuff needed
                                ---- for the write transactions have to be stored
                                if (write_txn_config(0) = '0') then
                                    iotab_fetching_state  <= READING_DATA;
                                end if;
                            -- else, assert that all transactions are done and wait for
                            -- new data in the IO table
                            else
                                read_txns_done_buf      <= '1';
                                iotab_fetching_state    <= WAIT_FOR_IOTAB;
                            end if;
                        end if;

                    when READING_DATA =>
                        iotab_fetching_data         <= '0';
                        iotab_fetching_data_prev    <= iotab_fetching_data;

                        if (read_tlast = '1') then
                            iotab_fetching_state <= FETCH_NEXT;
                        else
                            iotab_fetching_state <= READING_DATA;
                        end if;

                    when FETCH_NEXT =>
                        iotab_fetching_data         <= '1';
                        iotab_fetching_data_prev    <= '1';
                        iotable_row_buf             <= std_logic_vector(unsigned(iotable_row_buf) + 1);
                        iotab_fetching_state        <= FETCHING;

                    when others =>
                        iotab_fetching_state <= WAIT_FOR_IOTAB;

                end case;
            end if;
        end if;
    end process;


    write_buffer_free <= write_buffer_free_ff_prev and (not write_buffer_free_ff);

    -- buffering the pulse that indicates that one of the write data buffers is free
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                write_buffer_free_ff        <= '0';
                write_buffer_free_ff_prev   <= '0';
                num_write_buffers_ready     <= 0;
            else
                write_buffer_free_ff_prev   <= write_buffer_free_ff;

                if (writes_done = '1') then
                    write_buffer_free_ff <= '1';

                    if (write_buffer_ready = '0') then
                        if (num_write_buffers_ready > 0) then
                            num_write_buffers_ready <= num_write_buffers_ready - 1;
                        end if;
                    end if;
                elsif (write_buffer_ready = '1' and writes_done = '0') then
                    write_buffer_free_ff <= '0';

                    if (num_write_buffers_ready < C_NUM_BUFFERS) then
                        num_write_buffers_ready <= num_write_buffers_ready + 1;
                    end if;
                else
                    write_buffer_free_ff <= '0';
                end if;
            end if;
        end if;
    end process;


    read_buffer_ready <= read_buffer_ready_ff_prev and (not read_buffer_ready_ff);

    -- buffering the pulse that indicates that one of the read data buffers has data available
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_buffer_ready_ff        <= '0';
                read_buffer_ready_ff_prev   <= '0';
                num_read_buffers_free       <= C_NUM_BUFFERS;
            else
                read_buffer_ready_ff_prev   <= read_buffer_ready_ff;

                if (reads_done = '1') then
                    read_buffer_ready_ff <= '1';

                    if (read_buffer_free = '0') then
                        if (num_read_buffers_free > 0) then
                            num_read_buffers_free <= num_read_buffers_free - 1;
                        end if;
                    end if;
                elsif (read_buffer_free = '1' and reads_done = '0') then
                    read_buffer_ready_ff <= '0';

                    if (num_read_buffers_free < C_NUM_BUFFERS) then
                        num_read_buffers_free <= num_read_buffers_free + 1;
                    end if;
                else
                    read_buffer_ready_ff <= '0';
                end if;
            end if;
        end if;
    end process;


    ----------------------
    --Write Address Channel
    ----------------------

    -- The purpose of the write address channel is to request the address and
    -- command information for the entire transaction.  It is a single beat
    -- of information.

    -- The AXI4 Write address channel in this example will continue to initiate
    -- write commands as fast as it is allowed by the slave/interconnect.
    -- The address will be incremented on each accepted address transaction,
    -- by burst_size_byte to point to the next address.

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_awvalid <= '0';
            else
                -- If previously not valid, start next transaction
                if (axi_awvalid = '0' and start_single_burst_write = '1') then
                    axi_awvalid <= '1';
                -- Once asserted, VALIDs cannot be deasserted, so axi_awvalid
                -- must wait until transaction is accepted
                elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    axi_awvalid <= '0';
                else
                    axi_awvalid <= axi_awvalid;
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
        variable mem_index : integer;
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                current_line_write_start_addr   <= (others => '0');
                next_line_write_start_addr      <= (others => '0');
            else
                if (write_mem_fetching_state = WRITING_DATA and write_tlast = '1') then
                    if (unsigned(write_buffer_pe_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                        mem_index := 0;
                    else
                        mem_index := conv_integer(write_buffer_pe_id & "000");
                    end if;

                    current_line_write_start_addr   <= write_txns_mem(mem_index + 1);
                elsif (write_mem_data_fetched = '1') then
                    current_line_write_start_addr   <= std_logic_vector(unsigned(reg_w_partition_start_addr));
                    next_line_write_start_addr      <= std_logic_vector(unsigned(reg_w_partition_start_addr) + unsigned(image_width_write_bytes));
                elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    if (unsigned(axi_awaddr) + burst_size_bytes >= unsigned(partition_width_write_bytes)) then
                        current_line_write_start_addr   <= next_line_write_start_addr;
                        next_line_write_start_addr      <= std_logic_vector(unsigned(next_line_write_start_addr) + unsigned(image_width_write_bytes));
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- Next address after AWREADY indicates previous address acceptance
    process(M_AXI_ACLK)
        variable next_addr : unsigned(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_awaddr      <= (others => '0');
                M_AXI_AWADDR    <= (others => '0');
            else
                if (write_mem_data_fetched = '1') then
                    axi_awaddr      <= write_txn_addr_offset_in_line;
                    M_AXI_AWADDR    <= std_logic_vector(unsigned(reg_w_partition_start_addr) + unsigned(write_txn_addr_offset_in_line));
                elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then
                    next_addr := unsigned(axi_awaddr) + burst_size_bytes;

                    if (next_addr >= unsigned(partition_width_write_bytes)) then
                        axi_awaddr      <= (others => '0');
                        M_AXI_AWADDR    <= next_line_write_start_addr;
                    else
                        axi_awaddr      <= std_logic_vector(next_addr);
                        M_AXI_AWADDR    <= std_logic_vector(unsigned(current_line_write_start_addr) + next_addr);
                    end if;
                end if;
            end if;
        end if;
    end process;


    ----------------------
    --Write Data Channel
    ----------------------

    --The write data will continually try to push write data across the interface.

    --The amount of data accepted will depend on the AXI slave and the AXI
    --Interconnect settings, such as if there are FIFOs enabled in interconnect.

    --Note that there is no explicit timing relationship to the write address channel.
    --The write channel has its own throttling flag, separate from the AW channel.

    --Synchronization between the channels must be determined by the user.

    --The simpliest but lowest performance would be to only issue one address write
    --and write data burst at a time.

    --In this example they are kept in sync by using the same address increment
    --and burst sizes. Then the AW and W channels have their transactions measured
    --with threshold counters as part of the user logic, to make sure neither
    --channel gets too far ahead of each other.

    --Forward movement occurs when the write channel is valid and ready

    wnext <= M_AXI_WREADY and axi_wvalid;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                write_data_fetch_state  <= IDLE;
                write_buffer_addr_buf   <= (others => '0');
                write_count_in_line     <= 0;
                write_buffer_count      <= 0;
            else
                case (write_data_fetch_state) is
                    when IDLE =>
                        if (start_single_burst_write = '1') then
                            write_buffer_addr_buf   <= (others => '0');
                            write_data_fetch_state  <= SEND_ADDR;
                            write_buffer_count      <= 0;
                        else
                            write_data_fetch_state <= IDLE;
                        end if;

                    when SEND_ADDR =>
                        if (axi_awvalid = '1' and M_AXI_AWREADY = '1') then
                            write_data_fetch_state <= FETCHING;
                        else
                            write_data_fetch_state <= SEND_ADDR;
                        end if;

                    when FETCHING =>
                        if (wnext = '1') then
                            write_buffer_addr_buf   <= std_logic_vector(unsigned(write_buffer_addr_buf) + 1);
                            write_count_in_line     <= write_count_in_line + 1;
                            write_buffer_count      <= write_buffer_count + 1;

                            if (write_count_in_line >= unsigned(partition_width_write) - 1) then
                                write_data_fetch_state  <= WAIT_FOR_RESPONSE;
                                write_count_in_line     <= 0;
                            elsif (write_buffer_count >= C_M_AXI_BURST_LEN - 1) then
                                write_data_fetch_state  <= WAIT_FOR_RESPONSE;
                            else
                                write_data_fetch_state  <= FETCHING;
                            end if;
                        end if;

                    when WAIT_FOR_RESPONSE =>
                        if (writes_done = '1') then
                            write_data_fetch_state <= IDLE;
                        else
                            write_data_fetch_state <= WAIT_FOR_RESPONSE;
                        end if;

                    when others =>
                        write_data_fetch_state <= IDLE;

                end case;
            end if;
        end if;
    end process;

    -- WVALID logic, similar to the axi_awvalid always block above
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_wvalid <= '0';
            else
                if (wnext = '1') then
                    axi_wvalid <= '0';
                elsif (write_data_fetch_state = FETCHING) then
                    axi_wvalid <= '1';
                else
                    axi_wvalid <= '0';
                end if;
            end if;
        end if;
    end process;

    --WLAST generation on the MSB of a counter underflow
    -- WVALID logic, similar to the axi_awvalid always block above
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_wlast <= '0';
            -- axi_wlast is asserted when the write index
            -- count reaches the penultimate count to synchronize
            -- with the last write data when write_index is b1111
            -- elsif (&(write_index[C_TRANSACTIONS_NUM-1:1])&& ~write_index[0] && wnext)
            else
                if (((((write_index = std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, C_TRANSACTIONS_NUM+1)))
                        or (write_count_in_line >= unsigned(partition_width_write) - 1)) and C_M_AXI_BURST_LEN >= 2)
                        and M_AXI_WREADY = '1' and axi_wvalid = '0') or (C_M_AXI_BURST_LEN = 1)) then
                    axi_wlast <= '1';
                -- Deassrt axi_wlast when the last write data has been
                -- accepted by the slave with a valid response
                elsif (wnext = '1') then
                    axi_wlast <= '0';
                elsif (axi_wlast = '1' and C_M_AXI_BURST_LEN = 1) then
                    axi_wlast <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Burst length counter. Uses extra counter register bit to indicate terminal
    -- count to reduce decode logic */
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or start_single_burst_write = '1') then
                write_index <= (others => '0');
            else
                if (wnext = '1' and (write_index /= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, C_TRANSACTIONS_NUM+1)))) then
                    write_index <= std_logic_vector(unsigned(write_index) + 1);
                end if;
            end if;
        end if;
    end process;

    axi_wdata <= write_buffer_data; --std_logic_vector(to_unsigned(5, C_M_AXI_DATA_WIDTH)); --

    ---- Write Data Generator
    ---- Data pattern is only a simple incrementing count from 0 for each burst  */
    --process(M_AXI_ACLK)
    --begin
    --    if (rising_edge(M_AXI_ACLK)) then
    --        if (M_AXI_ARESETN = '0') then
    --            axi_wdata <= (others => '0');
    --        --elsif (wnext && axi_wlast) then
    --        --    axi_wdata <= 'b0;
    --        else
    --            if (wnext = '1') then
    --                axi_wdata <= std_logic_vector(unsigned(axi_wdata) + 1);
    --            end if;
    --        end if;
    --    end if;
    --end process;

    process(M_AXI_ACLK)
        variable mem_index : integer;
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                write_data_count_x          <= to_unsigned(0, 32);
                write_data_count_y          <= to_unsigned(0, 32);
                write_tlast                 <= '0';
                wait_for_next_write_burst   <= '0';
            else
                write_tlast         <= '0';

                if (write_mem_fetching_state = WRITING_DATA and write_tlast = '1') then
                    write_data_count_x <= (others => '0');
                    write_data_count_y <= (others => '0');
                elsif (write_mem_data_fetched = '1') then
                    if (unsigned(write_buffer_pe_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                        mem_index := 0;
                    else
                        mem_index := conv_integer(write_buffer_pe_id & "000");
                    end if;

                    write_data_count_x  <= unsigned(write_txns_mem(mem_index + 5));
                    write_data_count_y  <= unsigned(write_txns_mem(mem_index + 6));
                elsif (wnext = '1') then
                    if (write_data_count_x + 1 >= unsigned(partition_width_write)) then
                        if (write_data_count_y + 1 >= unsigned(partition_height_write)) then
                            write_tlast <= '1';
                        end if;

                        write_data_count_x   <= to_unsigned(0, 32);
                        write_data_count_y   <= write_data_count_y + 1;

                        wait_for_next_write_burst   <= '1';
                    elsif (wait_for_next_write_burst = '0') then
                        write_data_count_x  <= write_data_count_x + 1;
                    end if;
                elsif (burst_write_active = '0') then
                    wait_for_next_write_burst <= '0';
                end if;
            end if;
        end if;
    end process;


    ------------------------------
    --Write Response (B) Channel
    ------------------------------

    --The write response channel provides feedback that the write has committed
    --to memory. BREADY will occur when all of the data and the write address
    --has arrived and been accepted by the slave.

    --The write issuance (number of outstanding write addresses) is started by
    --the Address Write transfer, and is completed by a BREADY/BRESP.

    --While negating BREADY will eventually throttle the AWREADY signal,
    --it is best not to throttle the whole data channel this way.

    --The BRESP bit [1] is used indicate any errors from the interconnect or
    --slave for the entire write burst.

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_bready <= '0';
            -- accept/acknowledge bresp with axi_bready by the master
            -- when M_AXI_BVALID is asserted by slave
            else
                --if (M_AXI_BVALID = '1' and axi_bready = '1') then
                --    axi_bready <= '0';
                --elsif (write_data_fetch_state = FETCHING) then
                --    if (write_count_in_line >= unsigned(partition_width_write) - 1) then
                --        axi_bready <= '1';
                --    elsif (write_buffer_count >= C_M_AXI_BURST_LEN - 1) then
                --        axi_bready <= '1';
                --    end if;
                --end if;
                if (write_data_fetch_state = WAIT_FOR_RESPONSE and M_AXI_BVALID = '1' and axi_bready = '0') then
                    axi_bready <= '1';
                -- deassert after one clock cycle
                elsif (axi_bready = '1') then
                    axi_bready <= '0';
                end if;
            end if;
        end if;
    end process;


    --Flag any write response errors
    write_resp_error <= axi_bready and M_AXI_BVALID and M_AXI_BRESP(1);


    ------------------------------
    --Read Address Channel
    ------------------------------

    --The Read Address Channel (AW) provides a similar function to the
    --Write Address channel- to provide the tranfer qualifiers for the burst.

    --In this example, the read address increments in the same
    --manner as the write address channel.

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_arvalid <= '0';
            else
                -- If previously not valid, start next transaction
                if (axi_arvalid = '0' and start_single_burst_read = '1') then
                    axi_arvalid <= '1';
                -- Once asserted, VALIDs cannot be deasserted, so axi_arvalid
                -- must wait until transaction is accepted
                elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    axi_arvalid <= '0';
                else
                    axi_arvalid <= axi_arvalid;
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                current_line_read_start_addr <= (others => '0');
            else
                if (iotab_data_fetched = '1') then
                    current_line_read_start_addr <= R_PARTITION_START_ADDR;
                elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    if (unsigned(axi_araddr) + burst_size_bytes >= unsigned(partition_width_read_bytes)) then
                        current_line_read_start_addr <= next_line_read_start_addr;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- Next address after AWREADY indicates previous address acceptance
    process(M_AXI_ACLK)
        variable next_addr : unsigned(31 downto 0);
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_araddr          <= (others => '0');
                current_line_offset <= (others => '0');
            else
                if (iotab_data_fetched = '1') then
                    axi_araddr          <= (others => '0');
                    current_line_offset <= (others => '0');
                elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    next_addr := unsigned(axi_araddr) + burst_size_bytes;

                    current_line_offset <= axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto 2);

                    if (next_addr >= unsigned(partition_width_read_bytes)) then
                        axi_araddr <= (others => '0');
                    else
                        axi_araddr <= std_logic_vector(next_addr);
                    end if;
                end if;
            end if;
        end if;
    end process;


    ----------------------------------
    --Read Data (and Response) Channel
    ----------------------------------

    -- Forward movement occurs when the channel is valid and ready
    rnext <= M_AXI_RVALID and axi_rready;


    -- Burst length counter. Uses extra counter register bit to indicate
    -- terminal count to reduce decode logic
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or start_single_burst_read = '1') then
                read_index <= (others => '0');
            else
                if (rnext = '1' and (read_index <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, C_TRANSACTIONS_NUM+1)))) then
                    read_index <= std_logic_vector(unsigned(read_index) + 1);
                end if;
            end if;
        end if;
    end process;

    -- The Read Data channel returns the results of the read request
    --
    -- In this example the data checker is always able to accept
    -- more data, so no need to throttle the RREADY signal
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                axi_rready <= '0';
            -- accept/acknowledge rdata/rresp with axi_rready by the master
            -- when M_AXI_RVALID is asserted by slave
            else
                if (M_AXI_RVALID = '1') then
                    if (M_AXI_RLAST = '1' and axi_rready = '1') then
                        axi_rready <= '0';
                    else
                        axi_rready <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;


    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_buffer_data    <= (others => '0');
            else
                if (M_AXI_RVALID = '1' and axi_rready = '1') then
                    read_buffer_data <= M_AXI_RDATA;
                end if;
            end if;
        end if;
    end process;

    read_buffer_pe_id   <= iotab_pe_array_id;
    line_data_to_read   <= conv_integer(current_partition_width) - conv_integer(current_line_offset);
    read_buffer_length  <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN - line_data_to_read, 32))
                           when line_data_to_read < C_M_AXI_BURST_LEN else std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN, 32));
    read_buffer_last    <= read_buffer_wren_buf when line_data_to_read <= C_M_AXI_BURST_LEN and read_data_count_y + 1 >= unsigned(current_partition_height) else '0';
    read_buffer_prog    <= CONFIG(1);

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_buffer_addr        <= (others => '0');
                read_buffer_addr_next   <= (others => '0');
            else
                if (rnext = '1') then
                    read_buffer_addr_next   <= std_logic_vector(unsigned(read_buffer_addr_next) + 1);
                    read_buffer_addr        <= read_buffer_addr_next;
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                read_data_count_x           <= (others => '0');
                read_data_count_y           <= (others => '0');
                read_buffer_wren_buf        <= '0';
                read_tlast                  <= '0';
                wait_for_next_read_burst    <= '0';
                goto_next_line_read         <= '0';
            else
                read_tlast              <= '0';
                read_buffer_wren_buf    <= '0';

                if (iotab_data_fetched = '1') then
                    read_data_count_x       <= (others => '0');
                    read_data_count_y       <= (others => '0');
                    goto_next_line_read     <= '0';
                elsif (rnext = '1') then
                    if (read_data_count_x + 1 >= unsigned(current_partition_width)) then
                        if (read_data_count_y + 1 >= unsigned(current_partition_height)) then
                            read_tlast <= '1';
                        end if;

                        read_data_count_x   <= to_unsigned(0, 32);
                        goto_next_line_read <= '1';

                        wait_for_next_read_burst    <= '1';
                        read_buffer_wren_buf        <= '1';
                    elsif (wait_for_next_read_burst = '0') then
                        read_data_count_x       <= read_data_count_x + 1;

                        read_buffer_wren_buf    <= '1';
                    end if;
                elsif (burst_read_active = '0') then
                    if (wait_for_next_read_burst = '1' and goto_next_line_read = '1') then
                        read_data_count_y   <= read_data_count_y + 1;
                        goto_next_line_read <= '0';
                    end if;

                    wait_for_next_read_burst <= '0';
                end if;
            end if;
        end if;
    end process;


    --Flag any read response errors
    read_resp_error <= axi_rready and M_AXI_RVALID and M_AXI_RRESP(1);


    ----------------------------------
    --Example design throttling
    ----------------------------------

    -- For maximum port throughput, this user example code will try to allow
    -- each channel to run as independently and as quickly as possible.

    -- However, there are times when the flow of data needs to be throtted by
    -- the user application. This example application requires that data is
    -- not read before it is written and that the write channels do not
    -- advance beyond an arbitrary threshold (say to prevent an
    -- overrun of the current read address by the write address).

    -- From AXI4 Specification, 13.13.1: "If a master requires ordering between
    -- read and write transactions, it must ensure that a response is received
    -- for the previous transaction before issuing the next transaction."

    -- This example accomplishes this user application throttling through:
    -- -Reads wait for writes to fully complete
    -- -Address writes wait when not read + issued transaction counts pass
    -- a parameterized threshold
    -- -Writes wait when a not read + active data burst count pass
    -- a parameterized threshold

    MASTER_WRITE_EXECUTION_PROC: process(M_AXI_ACLK)
        variable mem_index  : integer;
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                -- reset condition
                -- All the signals are ed default values under reset condition
                mst_write_exec_state            <= IDLE;
                start_single_burst_write        <= '0';
                write_buffer_select_buf         <= (others => '0');
                cur_txn_done                    <= '0';
                write_mem_fetching_data         <= '0';
                write_mem_fetching_data_prev    <= '0';
            else
                write_mem_fetching_data_prev    <= write_mem_fetching_data;

                -- state transition
                case (mst_write_exec_state) is
                    when IDLE =>
                        -- This state is responsible to initiate
                        -- AXI transaction when one of the write buffers is full
                        if (num_write_buffers_ready > 0) then
                            mst_write_exec_state    <= FETCHING;
                            write_mem_fetching_data <= '1';
                        else
                            mst_write_exec_state    <= IDLE;
                        end if;

                    when FETCHING =>
                        -- fetch the date for the current transaction from the write transactions memory
                        if (unsigned(write_buffer_pe_id) >= to_unsigned(C_NUM_PE_ARRAYS, 16)) then
                            mem_index := 0;
                        else
                            mem_index := conv_integer(write_buffer_pe_id & "000");
                        end if;
                        reg_config                      <= write_txns_mem(mem_index);
                        reg_w_partition_start_addr      <= write_txns_mem(mem_index + 1);
                        reg_w_image_width               <= write_txns_mem(mem_index + 2);
                        reg_w_partition_dimensions      <= write_txns_mem(mem_index + 3);
                        write_txn_addr_offset_in_line   <= write_txns_mem(mem_index + 4);

                        if (write_txns_mem(mem_index)(0) = '1') then
                            mst_write_exec_state    <= INIT_WRITE;
                            cur_txn_done            <= '0';
                            write_mem_fetching_data <= '0';
                        end if;

                    when INIT_WRITE =>
                        -- This state is responsible to issue start_single_burst_write pulse to
                        -- initiate a write transaction. Write transactions will be
                        -- issued until burst_write_active signal is asserted.
                        -- write controller
                        if (axi_awvalid = '0' and start_single_burst_write = '0' and burst_write_active = '0') then
                            start_single_burst_write <= '1';
                            mst_write_exec_state     <= WRITING;
                        else
                            start_single_burst_write <= '0';
                            mst_write_exec_state     <= INIT_WRITE;
                        end if;

                    when WRITING =>
                        start_single_burst_write <= '0'; --Negate to generate a pulse

                        if (write_tlast = '1') then
                            cur_txn_done <= '1';
                        end if;

                        if (writes_done = '1') then
                            mst_write_exec_state <= SWITCH_BUFFER;
                        else
                            mst_write_exec_state <= WRITING;
                        end if;

                    when SWITCH_BUFFER =>
                        if (unsigned(write_buffer_select_buf) >= C_NUM_BUFFERS - 1) then
                            write_buffer_select_buf <= (others => '0');
                        else
                            write_buffer_select_buf <= std_logic_vector(unsigned(write_buffer_select_buf) + 1);
                        end if;

                        mst_write_exec_state <= IDLE;

                    when others =>
                        mst_write_exec_state <= IDLE;
                end case;
            end if;
        end if;
    end process;

    MASTER_READ_EXECUTION_PROC: process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                -- reset condition
                -- All the signals are ed default values under reset condition
                mst_read_exec_state     <= IDLE;
                start_single_burst_read <= '0';
                read_buffer_select_buf  <= (others => '0');
            else
                -- state transition
                case (mst_read_exec_state) is
                    when IDLE =>
                        -- This state is responsible to initiate
                        -- AXI transaction when one of the read buffers is empty
                        if (num_read_buffers_free > 0) then
                            mst_read_exec_state <= INIT_READ;
                        else
                            mst_read_exec_state <= IDLE;
                        end if;

                    when INIT_READ =>
                        -- This state is responsible to issue start_single_burst_read pulse to
                        -- initiate a read transaction. Read transactions will be
                        -- issued until burst_read_active signal is asserted.
                        -- read controller
                        if (reads_done = '1') then
                            mst_read_exec_state <= SWITCH_BUFFER;
                        else
                            mst_read_exec_state <= INIT_READ;

                            -- wait if the module is fetching data from the IO table or the IO table isn't filled
                            if (iotab_fetching_state = READING_DATA and iotab_fetching_data = '0'
                                    and axi_arvalid = '0' and burst_read_active = '0' and start_single_burst_read = '0') then
                                start_single_burst_read <= '1';
                            else
                                start_single_burst_read <= '0'; --Negate to generate a pulse
                            end if;
                        end if;

                    when SWITCH_BUFFER =>
                        if (unsigned(read_buffer_select_buf) >= C_NUM_BUFFERS - 1) then
                            read_buffer_select_buf <= (others => '0');
                        else
                            read_buffer_select_buf <= std_logic_vector(unsigned(read_buffer_select_buf) + 1);
                        end if;

                        mst_read_exec_state <= IDLE;

                    when others =>
                        mst_read_exec_state <= IDLE;
                end case;
            end if;
        end if;
    end process;


    -- burst_write_active signal is asserted when there is a burst write transaction
    -- is initiated by the assertion of start_single_burst_write. burst_write_active
    -- signal remains asserted until the burst write is accepted by the slave
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                burst_write_active <= '0';
            --The burst_write_active is asserted when a write burst transaction is initiated
            else
                if (start_single_burst_write = '1') then
                    burst_write_active <= '1';
                elsif (writes_done = '1') then
                    burst_write_active <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Check for last write completion.

    writes_done <= M_AXI_BVALID and axi_bready;

    ---- This logic is to qualify the last write count with the final write
    ---- response. This demonstrates how to confirm that a write has been
    ---- committed.
    --process(M_AXI_ACLK)
    --begin
    --    if (rising_edge(M_AXI_ACLK)) then
    --        if (M_AXI_ARESETN = '0') then
    --            writes_done <= '0';
    --        else
    --            if (M_AXI_BVALID = '1' and axi_bready = '1') then
    --                writes_done <= '1';
    --            else
    --                writes_done <= '0';
    --            end if;
    --        end if;
    --    end if;
    --end process;

    -- burst_read_active signal is asserted when there is a burst write transaction
    -- is initiated by the assertion of start_single_burst_write. start_single_burst_read
    -- signal remains asserted until the burst read is accepted by the master
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                burst_read_active <= '0';
            --The burst_write_active is asserted when a write burst transaction is initiated
            else
                if (start_single_burst_read = '1')then
                    burst_read_active <= '1';
                elsif (M_AXI_RVALID = '1' and axi_rready = '1' and M_AXI_RLAST = '1') then
                    burst_read_active <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Check for last read completion.

    -- This logic is to qualify the last read count with the final read
    -- response. This demonstrates how to confirm that a read has been
    -- committed.
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                reads_done <= '0';
            --The reads_done should be associated with a rready response
            --elsif (M_AXI_RVALID && axi_rready && axi_rlast)
            else
                if (M_AXI_RVALID = '1' and axi_rready = '1' and (read_index = std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1,C_TRANSACTIONS_NUM+1)) or M_AXI_RLAST = '1')) then
                    reads_done <= '1';
                else
                    reads_done <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Add user logic here

    -- User logic ends

end implementation;
