library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_iotable_fill is
    generic (
        -- Users to add parameters here

        C_NUM_IOTABLE_ROWS      : integer   := 16;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
        C_M_AXI_BURST_LEN       : integer   := 16;
        -- Thread ID Width
        C_M_AXI_ID_WIDTH        : integer   := 1;
        -- Width of Address Bus
        C_M_AXI_ADDR_WIDTH      : integer   := 32;
        -- Width of Data Bus
        C_M_AXI_DATA_WIDTH      : integer   := 32;
        -- Width of User Write Address Bus
        C_M_AXI_AWUSER_WIDTH    : integer   := 0;
        -- Width of User Read Address Bus
        C_M_AXI_ARUSER_WIDTH    : integer   := 0;
        -- Width of User Write Data Bus
        C_M_AXI_WUSER_WIDTH     : integer   := 0;
        -- Width of User Read Data Bus
        C_M_AXI_RUSER_WIDTH     : integer   := 0;
        -- Width of User Response Bus
        C_M_AXI_BUSER_WIDTH     : integer   := 0
    );
    port (
        -- Users to add ports here

        iotable_filled      : out std_logic;
        iotable_filled_ack  : in  std_logic;
        table_txns_done     : in  std_logic;

        read_enable         : in  std_logic;
        read_ack            : out std_logic;
        read_start_addr     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        read_len            : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);

        -- fill table
        table_write_enable  : out std_logic;
        table_write_addr    : out std_logic_vector(31 downto 0);
        table_write_data    : out std_logic_vector(31 downto 0);

        -- User ports ends
        -- Do not modify the ports beyond this line

        TXNS_DONE           : out std_logic;
        TXNS_DONE_ACK       : in  std_logic;
        -- Global Clock Signal.
        M_AXI_ACLK          : in  std_logic;
        -- Global Reset Singal. This Signal is Active Low
        M_AXI_ARESETN       : in  std_logic;
        -- Master Interface Write Address ID
        M_AXI_AWID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Address
        M_AXI_AWADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_AWLEN         : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_AWSIZE        : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_AWBURST       : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_AWLOCK        : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_AWCACHE       : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_AWPROT        : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each write transaction.
        M_AXI_AWQOS         : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the write address channel.
        M_AXI_AWUSER        : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid write address and control information.
        M_AXI_AWVALID       : out std_logic;
        -- Write address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_AWREADY       : in  std_logic;
        -- Master Interface Write ID
        M_AXI_WID           : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Data.
        M_AXI_WDATA         : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte
        -- lanes hold valid data. There is one write strobe
        -- bit for each eight bits of the write data bus.
        M_AXI_WSTRB         : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        -- Write last. This signal indicates the last transfer in a write burst.
        M_AXI_WLAST         : out std_logic;
        -- Optional User-defined signal in the write data channel.
        M_AXI_WUSER         : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available
        M_AXI_WVALID        : out std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        M_AXI_WREADY        : in  std_logic;
        -- Master Interface Write Response.
        M_AXI_BID           : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Write response. This signal indicates the status of the write transaction.
        M_AXI_BRESP         : in  std_logic_vector(1 downto 0);
        -- Optional User-defined signal in the write response channel
        M_AXI_BUSER         : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        -- Write response valid. This signal indicates that the
        -- channel is signaling a valid write response.
        M_AXI_BVALID        : in  std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        M_AXI_BREADY        : out std_logic;
        -- Master Interface Read Address.
        M_AXI_ARID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Read address. This signal indicates the initial
        -- address of a read burst transaction.
        M_AXI_ARADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_ARLEN         : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_ARSIZE        : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_ARBURST       : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_ARLOCK        : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_ARCACHE       : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_ARPROT        : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each read transaction
        M_AXI_ARQOS         : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the read address channel.
        M_AXI_ARUSER        : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid read address and control information
        M_AXI_ARVALID       : out std_logic;
        -- Read address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_ARREADY       : in  std_logic;
        -- Read ID tag. This signal is the identification tag
        -- for the read data group of signals generated by the slave.
        M_AXI_RID           : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Read Data
        M_AXI_RDATA         : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the read transfer
        M_AXI_RRESP         : in  std_logic_vector(1 downto 0);
        -- Read last. This signal indicates the last transfer in a read burst
        M_AXI_RLAST         : in  std_logic;
        -- Optional User-defined signal in the read address channel.
        M_AXI_RUSER         : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        -- Read valid. This signal indicates that the channel
        -- is signaling the required read data.
        M_AXI_RVALID        : in  std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        M_AXI_RREADY        : out std_logic
    );
end faupu_iotable_fill;

architecture implementation of faupu_iotable_fill is

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
    constant C_TRANSACTIONS_NUM : integer   := clogb2(C_M_AXI_BURST_LEN-1);

    -- Burst length for transactions, in C_M_AXI_DATA_WIDTHs.
    -- Non-2^n lengths will eventually cause bursts across 4K address boundaries.
    constant C_MASTER_LENGTH    : integer   := 12;

    constant ROWS_PER_BURST     : integer   := C_M_AXI_BURST_LEN / 8;

    -- Example State machine to initialize counter, initialize write transactions,
    -- initialize read transactions and comparison of read data with the
    -- written data words.
    type state is ( IDLE,       -- This state initiates AXI4Lite transaction
                                -- after the state machine changes state to INIT_READ
                                -- when there is 0 to 1 transition on INIT_AXI_TXN
                    INIT_READ); -- This state initializes read transaction
                                -- once reads are done, the state machine
                                -- changes state to INIT_COMPARE

    signal mst_exec_state  : state ;

    -- AXI4FULL signals
    --AXI4 internal temp signals
    signal axi_araddr               : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_arvalid              : std_logic;
    signal axi_rready               : std_logic;
    --read beat count in a burst
    signal read_index               : std_logic_vector(C_M_AXI_DATA_WIDTH+2 downto 0);
    --size of C_M_AXI_BURST_LEN length burst in bytes
    signal burst_size_bytes         : std_logic_vector(C_TRANSACTIONS_NUM+2 downto 0);
    signal start_single_burst_read  : std_logic;
    signal reads_done               : std_logic;
    signal burst_read_active        : std_logic;
    --Interface response error flags
    signal rnext                    : std_logic;
    signal init_txn_ff              : std_logic;
    signal init_txn_ff2             : std_logic;
    signal init_txn_pulse           : std_logic;

    signal keep_init_txn_pulse      : std_logic;
    signal keep_init_txn_pulse_prev : std_logic;
    signal new_table_data_pulse     : std_logic;

    signal table_txn_active         : std_logic;
    signal iotable_filled_buf       : std_logic;

    signal cur_read_start_addr      : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal table_write_addr_buf     : std_logic_vector(31 downto 0);

    signal num_table_rows_to_fetch  : unsigned(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal num_words_to_fetch       : std_logic_vector(C_M_AXI_DATA_WIDTH+2 downto 0);
    signal table_write_idx          : unsigned(C_M_AXI_DATA_WIDTH+2 downto 0);

begin
    -- I/O Connections assignments

    --I/O Connections. Write Address (AW)
    M_AXI_AWID          <= (others => '0');
    --The AXI address is a concatenation of the target base address + active offset range
    M_AXI_AWADDR        <= (others => '0');
    --Burst LENgth is number of transaction beats, minus 1
    M_AXI_AWLEN         <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN - 1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    M_AXI_AWSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8) - 1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_AWBURST       <= "01";
    M_AXI_AWLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_AWCACHE       <= "0010";
    M_AXI_AWPROT        <= "000";
    M_AXI_AWQOS         <= x"0";
    M_AXI_AWUSER        <= (others => '1');
    M_AXI_AWVALID       <= '0';
    --Write Data(W)
    M_AXI_WDATA         <= (others => '0');
    --All bursts are complete and aligned in this example
    M_AXI_WSTRB         <= (others => '1');
    M_AXI_WLAST         <= '0';
    M_AXI_WUSER         <= (others => '0');
    M_AXI_WVALID        <= '0';
    M_AXI_WID           <= (others => '0');
    --Write Response (B)
    M_AXI_BREADY        <= '0';
    --Read Address (AR)
    M_AXI_ARID          <= (others => '0');
    M_AXI_ARADDR        <= std_logic_vector(unsigned(cur_read_start_addr) + unsigned(axi_araddr));
    --Burst LENgth is number of transaction beats, minus 1
    M_AXI_ARLEN         <= std_logic_vector(unsigned(num_words_to_fetch(7 downto 0)) - 1) when unsigned(num_words_to_fetch) < C_M_AXI_BURST_LEN else std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN - 1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    M_AXI_ARSIZE        <= std_logic_vector(to_unsigned(clogb2((C_M_AXI_DATA_WIDTH/8) - 1), 3));
    --INCR burst type is usually used, except for keyhole bursts
    M_AXI_ARBURST       <= "01";
    M_AXI_ARLOCK        <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_ARCACHE       <= "0010";
    M_AXI_ARPROT        <= "000";
    M_AXI_ARQOS         <= x"0";
    M_AXI_ARUSER        <= (others => '1');
    M_AXI_ARVALID       <= axi_arvalid;
    --Read and Read Response (R)
    M_AXI_RREADY        <= axi_rready;
    --Burst size in bytes
    burst_size_bytes    <= std_logic_vector(to_unsigned((C_M_AXI_BURST_LEN * (C_M_AXI_DATA_WIDTH/8)), C_TRANSACTIONS_NUM+3));
    init_txn_pulse      <= (not init_txn_ff2) and init_txn_ff;

    -- Level Based Interrupt
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or TXNS_DONE_ACK = '1') then
                TXNS_DONE <= '0';
            elsif (table_txns_done = '1') then
                TXNS_DONE <= '1';
            end if;
        end if;
    end process;

    num_words_to_fetch <= std_logic_vector(num_table_rows_to_fetch) & "000";

    table_write_enable  <= rnext when table_write_idx < unsigned(num_words_to_fetch) else '0';
    table_write_addr    <= table_write_addr_buf;
    table_write_data    <= M_AXI_RDATA;

    iotable_filled      <= iotable_filled_buf;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or iotable_filled_buf = '1') then
                table_write_idx         <= (others => '0');
                table_write_addr_buf    <= (others => '0');
            else
                if (rnext = '1') then
                    table_write_idx         <= table_write_idx + 1;
                    table_write_addr_buf    <= std_logic_vector(unsigned(table_write_addr_buf) + 4);
                end if;
            end if;
        end if;
    end process;

    --Generate a pulse to initiate AXI transaction.
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            -- Initiates AXI transaction delay
            if (M_AXI_ARESETN = '0' ) then
                init_txn_ff     <= '0';
                init_txn_ff2    <= '0';
                read_ack        <= '0';
            else
                init_txn_ff <= read_enable;
                init_txn_ff2 <= init_txn_ff;

                if (read_enable = '1') then
                    read_ack <= '1';
                else
                    read_ack <= '0';
                end if;
            end if;
        end if;
    end process;


    new_table_data_pulse <= (not keep_init_txn_pulse) and keep_init_txn_pulse_prev;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                num_table_rows_to_fetch <= (others => '0');
                iotable_filled_buf      <= '0';
                cur_read_start_addr     <= (others => '0');
            else
                if (iotable_filled_ack = '1') then
                    iotable_filled_buf <= '0';
                end if;

                if (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1') then
                    num_table_rows_to_fetch <= unsigned(read_len);
                    cur_read_start_addr     <= read_start_addr;
                elsif (reads_done = '1') then
                    if (num_table_rows_to_fetch > ROWS_PER_BURST) then
                        num_table_rows_to_fetch <= num_table_rows_to_fetch - ROWS_PER_BURST;
                    else
                        num_table_rows_to_fetch <= (others => '0');
                        iotable_filled_buf <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                table_txn_active <= '0';
            else
                if (iotable_filled_buf = '1') then
                    table_txn_active <= '1';
                elsif (table_txns_done = '1') then
                    table_txn_active <= '0';
                end if;
            end if;
        end if;
    end process;

    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                keep_init_txn_pulse         <= '0';
                keep_init_txn_pulse_prev    <= '0';
            else
                keep_init_txn_pulse_prev    <= keep_init_txn_pulse;

                if (init_txn_pulse = '1') then
                    keep_init_txn_pulse     <= '1';
                elsif (num_table_rows_to_fetch = 0) then
                    keep_init_txn_pulse     <= '0';
                end if;
            end if;
        end if;
    end process;

    ------------------------------
    --Read Address Channel
    ------------------------------

    --The Read Address Channel (AW) provides a similar function to the
    --Write Address channel- to provide the tranfer qualifiers for the burst.

    --In this example, the read address increments in the same
    --manner as the write address channel.
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1')) then
                axi_arvalid <= '0';
            -- If previously not valid, start next transaction
            else
                if (axi_arvalid = '0' and start_single_burst_read = '1') then
                    axi_arvalid <= '1';
                elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    axi_arvalid <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Next address after ARREADY indicates previous address acceptance
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1')) then
                axi_araddr <= (others => '0');
            else
                if (M_AXI_ARREADY = '1' and axi_arvalid = '1') then
                    axi_araddr <= std_logic_vector(unsigned(axi_araddr) + unsigned(burst_size_bytes));
                end if;
            end if;
        end if;
    end process;


    ----------------------------------
    --Read Data (and Response) Channel
    ----------------------------------

    -- Forward movement occurs when the channel is valid and ready
    rnext <= M_AXI_RVALID and axi_rready;


    -- Burst length counter. Uses extra counter register bit to indicate
    -- terminal count to reduce decode logic
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or start_single_burst_read = '1' or (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1')) then
                read_index <= (others => '0');
            else
                if (rnext = '1' and (read_index <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, C_M_AXI_DATA_WIDTH+3)))) then
                    read_index <= std_logic_vector(unsigned(read_index) + 1);
                end if;
            end if;
        end if;
    end process;

    --/*
    -- The Read Data channel returns the results of the read request
    --
    -- In this example the data checker is always able to accept
    -- more data, so no need to throttle the RREADY signal
    -- */
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1')) then
                axi_rready <= '0';
            -- accept/acknowledge rdata/rresp with axi_rready by the master
            -- when M_AXI_RVALID is asserted by slave
            else
                if (axi_rready = '1') then
                    axi_rready <= '0';
                elsif (M_AXI_RVALID = '1') then
                    axi_rready <= '1';
                end if;
            end if;
        end if;
    end process;


    ----------------------------------
    -- design throttling
    ----------------------------------

    MASTER_EXECUTION_PROC:process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0') then
                -- reset condition
                -- All the signals are ed default values under reset condition
                mst_exec_state          <= IDLE;
                start_single_burst_read <= '0';
            else
                -- state transition
                case (mst_exec_state) is
                    when IDLE =>
                        -- This state is responsible to initiate
                        -- AXI transaction when init_txn_pulse is asserted
                        if (table_txn_active = '0' and num_table_rows_to_fetch > 0) then
                            mst_exec_state <= INIT_READ;
                        else
                            mst_exec_state <= IDLE;
                        end if;

                    when INIT_READ =>
                        -- This state is responsible to issue start_single_read pulse to
                        -- initiate a read transaction. Read transactions will be
                        -- issued until burst_read_active signal is asserted.
                        -- read controller
                        if (reads_done = '1') then
                            mst_exec_state <= IDLE;
                        else
                            mst_exec_state <= INIT_READ;

                            if (axi_arvalid = '0' and burst_read_active = '0' and start_single_burst_read = '0') then
                                start_single_burst_read <= '1';
                            else
                                start_single_burst_read <= '0'; --Negate to generate a pulse
                            end if;
                        end if;

                    when others =>
                        mst_exec_state <= IDLE;
                end case  ;
            end if;
        end if;
    end process;

    -- burst_read_active signal is asserted when there is a burst write transaction
    -- is initiated by the assertion of start_single_burst_read. start_single_burst_read
    -- signal remains asserted until the burst read is accepted by the master
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or (num_table_rows_to_fetch = 0 and new_table_data_pulse = '1')) then
                burst_read_active <= '0';
            --The burst_read_active is asserted when a write burst transaction is initiated
            else
                if (start_single_burst_read = '1')then
                    burst_read_active <= '1';
                elsif (M_AXI_RVALID = '1' and axi_rready = '1' and M_AXI_RLAST = '1') then
                    burst_read_active <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Check for last read completion.

    -- This logic is to qualify the last read count with the final read
    -- response. This demonstrates how to confirm that a read has been
    -- committed.
    process(M_AXI_ACLK)
    begin
        if (rising_edge(M_AXI_ACLK)) then
            if (M_AXI_ARESETN = '0' or new_table_data_pulse = '1') then
                reads_done <= '0';
            --The reads_done should be associated with a rready response
            else
                if (M_AXI_RVALID = '1' and axi_rready = '1' and (read_index = std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN-1, C_M_AXI_DATA_WIDTH+3)) or read_index = std_logic_vector(unsigned(num_words_to_fetch) - 1))) then
                    reads_done <= '1';
                else
                    reads_done <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Add user logic here

    -- User logic ends

end implementation;
