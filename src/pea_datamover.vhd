library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity pea_datamover is
    generic (
        C_BUFFER_SELECT_LENGTH      : integer   := 1;
        C_NUM_BUFFERS               : integer   := 2;
        C_BUFFER_ADDR_WIDTH         : integer   := 4;
        C_BURST_LEN                 : integer   := 16;
        C_DATA_WIDTH                : integer   := 32
    );
    port (
        debug_out                   : out std_logic_vector(C_DATA_WIDTH-1 downto 0);

        axi_aclk                    : in  std_logic;
        axi_aresetn                 : in  std_logic;

        write_buffer_ready          : out std_logic;
        read_buffer_ready           : in  std_logic;
        write_buffer_free           : in  std_logic;
        read_buffer_free            : out std_logic;

        write_buffer_select         : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        write_buffer_addr           : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        write_buffer_data           : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
        write_buffer_pe_id          : out std_logic_vector(15 downto 0);
        write_buffer_length         : out std_logic_vector(31 downto 0);
        write_buffer_last           : out std_logic;
        write_buffer_wren           : out std_logic;

        read_buffer_select          : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        read_buffer_addr            : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        read_buffer_data            : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
        read_buffer_pe_id           : in  std_logic_vector(15 downto 0);
        read_buffer_length          : in  std_logic_vector(31 downto 0);
        read_buffer_prog            : in  std_logic;
        read_buffer_last            : in  std_logic;
        read_buffer_rden            : out std_logic
    );
end pea_datamover;

architecture arch_imp of pea_datamover is

    type state is ( IDLE_READ,   -- This state initiates a read transaction
                    INIT_READ,   -- This state initializes read transaction,
                                 -- once reads are done, the state machine
                                 -- changes state to IDLE_WRITE
                    IDLE_WRITE,  -- This state initiates a write transaction
                    INIT_WRITE); -- This state initializes write transaction
                                 -- once writes are done, the state machine
                                 -- changes state to IDLE_READ
    signal mst_exec_state : state;

    signal num_read_buffers_ready   : integer;
    signal num_write_buffers_free   : integer;

    signal reads_done               : std_logic;
    signal writes_done              : std_logic;

    signal start_single_burst_read  : std_logic;
    signal start_single_burst_write : std_logic;

    signal burst_read_active        : std_logic;
    signal burst_write_active       : std_logic;

    signal read_data_count          : integer;
    signal write_data_count         : integer;

    -- TODO: connect to the AXI stream interface of the PE arrays
    signal slave_rready             : std_logic;

    -- buffers
    signal read_buffer_addr_buf     : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal write_buffer_addr_buf    : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal read_buffer_select_buf   : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal write_buffer_select_buf  : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);

    type mem_type is array ((2**C_BUFFER_ADDR_WIDTH)-1 downto 0) of std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal mem : mem_type;

    signal mem_read_addr            : integer;
    signal mem_write_addr           : integer;

    signal mem_length               : std_logic_vector(31 downto 0);
    signal mem_pe_id                : std_logic_vector(15 downto 0);
    signal mem_last                 : std_logic;

    signal write_buffer_ready_ff        : std_logic;
    signal write_buffer_ready_ff_prev   : std_logic;
    signal read_buffer_free_ff          : std_logic;
    signal read_buffer_free_ff_prev     : std_logic;

    signal debug_0_out_buf          : std_logic_vector((C_DATA_WIDTH/4)-1 downto 0);
    signal debug_1_out_buf          : std_logic_vector((C_DATA_WIDTH/4)-1 downto 0);
    signal debug_2_out_buf          : std_logic_vector((C_DATA_WIDTH/4)-1 downto 0);
    signal debug_3_out_buf          : std_logic_vector((C_DATA_WIDTH/4)-1 downto 0);

begin

    read_buffer_addr    <= read_buffer_addr_buf;
    write_buffer_addr   <= write_buffer_addr_buf;
    read_buffer_select  <= read_buffer_select_buf;
    write_buffer_select <= write_buffer_select_buf;

    -- TODO: remove
    slave_rready        <= '1';
    read_buffer_rden    <= '1';

    debug_out           <= debug_3_out_buf & debug_2_out_buf & debug_1_out_buf & debug_0_out_buf;

    DEBUG_PROC: process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                debug_0_out_buf <= (others => '0');
                debug_1_out_buf <= (others => '0');
                debug_2_out_buf <= (others => '0');
                debug_3_out_buf <= (others => '0');
            else
                debug_0_out_buf <= std_logic_vector(to_unsigned(num_write_buffers_free, C_DATA_WIDTH/4));
                debug_1_out_buf <= std_logic_vector(to_unsigned(num_read_buffers_ready, C_DATA_WIDTH/4));

                if (reads_done = '1') then
                    debug_2_out_buf <= std_logic_vector(unsigned(debug_2_out_buf) + 1);
                end if;

                if (writes_done = '1') then
                    debug_3_out_buf <= std_logic_vector(unsigned(debug_3_out_buf) + 1);
                end if;
            end if;
        end if;
    end process;


    read_buffer_free <= read_buffer_free_ff_prev and (not read_buffer_free_ff);

    -- buffering the pulse that indicates that one of the read data buffers is free
    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                read_buffer_free_ff         <= '0';
                read_buffer_free_ff_prev    <= '0';
                num_read_buffers_ready      <= 0;
            else
                read_buffer_free_ff_prev    <= read_buffer_free_ff;

                if (reads_done = '1') then
                    read_buffer_free_ff <= '1';

                    if (read_buffer_ready = '0') then
                        if (num_read_buffers_ready > 0) then
                            num_read_buffers_ready <= num_read_buffers_ready - 1;
                        end if;
                    end if;
                elsif (read_buffer_ready = '1') then
                    read_buffer_free_ff <= '0';

                    if (num_read_buffers_ready < C_NUM_BUFFERS) then
                        num_read_buffers_ready <= num_read_buffers_ready + 1;
                    end if;
                else
                    read_buffer_free_ff <= '0';
                end if;
            end if;
        end if;
    end process;


    write_buffer_ready <= write_buffer_ready_ff_prev and (not write_buffer_ready_ff);

    -- buffering the pulse that indicates that one of the read data buffers has data available
    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                write_buffer_ready_ff        <= '0';
                write_buffer_ready_ff_prev   <= '0';
                num_write_buffers_free       <= C_NUM_BUFFERS;
            else
                write_buffer_ready_ff_prev   <= write_buffer_ready_ff;

                if (writes_done = '1') then
                    write_buffer_ready_ff <= '1';

                    if (write_buffer_free = '0') then
                        if (num_write_buffers_free > 0) then
                            num_write_buffers_free <= num_write_buffers_free - 1;
                        end if;
                    end if;
                elsif (write_buffer_free = '1') then
                    write_buffer_ready_ff <= '0';

                    if (num_write_buffers_free < C_NUM_BUFFERS) then
                        num_write_buffers_free <= num_write_buffers_free + 1;
                    end if;
                else
                    write_buffer_ready_ff <= '0';
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                mem_write_addr <= 0;
            else
                if (reads_done = '1') then
                    mem_write_addr <= 0;

                    mem(mem_write_addr) <= read_buffer_data;
                    mem_length          <= read_buffer_length;
                    mem_pe_id           <= read_buffer_pe_id;
                    mem_last            <= read_buffer_last;
                elsif (burst_read_active = '1' and read_data_count < C_BURST_LEN) then
                    mem_write_addr      <= mem_write_addr + 1;

                    mem(mem_write_addr) <= read_buffer_data;
                    mem_length          <= read_buffer_length;
                    mem_pe_id           <= read_buffer_pe_id;
                    mem_last            <= read_buffer_last;
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                mem_read_addr           <= 0;
                write_buffer_wren       <= '0';
                write_buffer_addr_buf   <= (others => '0');
            else
                if (writes_done = '1') then
                    mem_read_addr           <= 0;

                    write_buffer_data       <= mem(mem_read_addr);
                    write_buffer_length     <= mem_length;
                    write_buffer_pe_id      <= mem_pe_id;
                    write_buffer_last       <= mem_last;
                    write_buffer_wren       <= '1';
                    write_buffer_addr_buf   <= std_logic_vector(unsigned(write_buffer_addr_buf) + 1);
                elsif (burst_write_active = '1' and write_data_count < C_BURST_LEN) then
                    mem_read_addr <= mem_read_addr + 1;

                    write_buffer_data       <= mem(mem_read_addr);
                    write_buffer_length     <= mem_length;
                    write_buffer_pe_id      <= mem_pe_id;
                    write_buffer_last       <= mem_last;
                    write_buffer_wren       <= '1';

                    if (write_data_count > 1) then
                        write_buffer_addr_buf   <= std_logic_vector(unsigned(write_buffer_addr_buf) + 1);
                    end if;
                else
                    write_buffer_wren   <= '0';
                    write_buffer_addr_buf   <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                burst_read_active <= '0';
            else
                if (start_single_burst_read = '1') then
                    burst_read_active <= '1';
                elsif (reads_done = '1') then
                    burst_read_active <= '0';
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                burst_write_active <= '0';
            else
                if (start_single_burst_write = '1') then
                    burst_write_active <= '1';
                elsif (writes_done = '1') then
                    burst_write_active <= '0';
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                reads_done              <= '0';
                read_buffer_addr_buf    <= (others => '0');
                read_data_count         <= 0;
            else
                reads_done <= '0';

                if (start_single_burst_read = '1') then
                    read_data_count         <= read_data_count + 1;
                    read_buffer_addr_buf    <= std_logic_vector(unsigned(read_buffer_addr_buf) + 1);
                elsif (slave_rready = '1' and burst_read_active = '1' and reads_done = '0') then
                    if (read_data_count < C_BURST_LEN - 1) then
                        read_data_count         <= read_data_count + 1;
                        read_buffer_addr_buf    <= std_logic_vector(unsigned(read_buffer_addr_buf) + 1);
                    else
                        reads_done              <= '1';
                        read_buffer_addr_buf    <= (others => '0');
                        read_data_count         <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                writes_done         <= '0';
                write_data_count    <= 0;
            else
                if (start_single_burst_write = '1') then
                    write_data_count <= write_data_count + 1;
                    writes_done      <= '0';
                elsif (burst_write_active = '1' and writes_done = '0') then
                    if (write_data_count < C_BURST_LEN - 1) then
                        write_data_count    <= write_data_count + 1;
                        writes_done         <= '0';
                    else
                        writes_done         <= '1';
                        write_data_count    <= 0;
                    end if;
                else
                    writes_done         <= '0';
                end if;
            end if;
        end if;
    end process;


    READ_DATA_PROC: process(axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (axi_aresetn = '0') then
                read_buffer_select_buf   <= (others => '0');
                mst_exec_state           <= IDLE_READ;
                start_single_burst_read  <= '0';
                write_buffer_select_buf  <= std_logic_vector(to_unsigned(C_NUM_BUFFERS - 1, C_BUFFER_SELECT_LENGTH));
                start_single_burst_write <= '0';
            else
                case (mst_exec_state) is
                    when IDLE_READ =>
                        if (num_read_buffers_ready > 0) then
                            mst_exec_state <= INIT_READ;
                        else
                            mst_exec_state <= IDLE_READ;
                        end if;

                    when INIT_READ =>
                        -- This state is responsible to issue start_single_burst_read pulse to
                        -- initiate a read transaction. Read transactions will be
                        -- issued until burst_read_active signal is asserted.
                        -- read controller
                        if (reads_done = '1') then
                            if (unsigned(read_buffer_select_buf) >= C_NUM_BUFFERS - 1) then
                                read_buffer_select_buf <= (others => '0');
                            else
                                read_buffer_select_buf <= std_logic_vector(unsigned(read_buffer_select_buf) + 1);
                            end if;

                            if (read_buffer_prog = '0') then
                                mst_exec_state <= IDLE_WRITE;
                            else
                                mst_exec_state <= IDLE_READ;
                            end if;
                        else
                            mst_exec_state <= INIT_READ;

                            -- wait if the module is bursting data
                            if (burst_read_active = '0' and start_single_burst_read = '0') then
                                start_single_burst_read <= '1';
                            else
                                start_single_burst_read <= '0'; --Negate to generate a pulse
                            end if;
                        end if;

                    when IDLE_WRITE =>
                        if (num_write_buffers_free > 0) then
                            if (unsigned(write_buffer_select_buf) >= C_NUM_BUFFERS - 1) then
                                write_buffer_select_buf <= (others => '0');
                            else
                                write_buffer_select_buf <= std_logic_vector(unsigned(write_buffer_select_buf) + 1);
                            end if;

                            mst_exec_state <= INIT_WRITE;
                        else
                            mst_exec_state <= IDLE_WRITE;
                        end if;

                    when INIT_WRITE =>
                        -- This state is responsible to issue start_single_burst_write pulse to
                        -- initiate a read transaction. Write transactions will be
                        -- issued until burst_write_active signal is asserted.
                        -- write controller
                        if (writes_done = '1') then
                            mst_exec_state <= IDLE_READ;
                        else
                            mst_exec_state <= INIT_WRITE;

                            -- wait if the module is bursting data
                            if (burst_write_active = '0' and start_single_burst_write = '0') then
                                start_single_burst_write <= '1';
                            else
                                start_single_burst_write <= '0'; --Negate to generate a pulse
                            end if;
                        end if;

                    when others =>
                        mst_exec_state <= IDLE_READ;

                end case;
            end if;
        end if;
    end process;

end arch_imp;
