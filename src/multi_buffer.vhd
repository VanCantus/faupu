library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity multi_buffer is
    generic (
        DATA_WIDTH              : integer   := 32;
        ADDR_WIDTH              : integer   := 4;
        BUFFER_SELECT_LENGTH    : integer   := 1;
        NUM_BUFFERS             : integer   := 2
    );
    port (
        clk             : in  std_logic;
        resetn          : in  std_logic;

        -- BRAM Port A
        a_buffer_select : in  std_logic_vector(BUFFER_SELECT_LENGTH-1 downto 0);
        a_pe_arr_id_in  : in  std_logic_vector(15 downto 0);
        a_length_in     : in  std_logic_vector(31 downto 0);
        a_prog_in       : in  std_logic;
        a_last_in       : in  std_logic;
        a_wr            : in  std_logic;
        a_addr          : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        a_din           : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        --a_dout          : out std_logic_vector(DATA_WIDTH-1 downto 0);
        --a_pe_arr_id_out : out std_logic_vector(15 downto 0);
        --a_length_out    : out std_logic_vector(31 downto 0);
        --a_prog_out      : out std_logic;
        --a_last_out      : out std_logic;

        -- BRAM Port B
        b_buffer_select : in  std_logic_vector(BUFFER_SELECT_LENGTH-1 downto 0);
        --b_pe_arr_id_in  : in  std_logic_vector(15 downto 0);
        --b_length_in     : in  std_logic_vector(31 downto 0);
        --b_prog_in       : in  std_logic;
        --b_last_in       : in  std_logic;
        --b_wr            : in  std_logic;
        b_addr          : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        --b_din           : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        b_dout          : out std_logic_vector(DATA_WIDTH-1 downto 0);
        b_pe_arr_id_out : out std_logic_vector(15 downto 0);
        b_length_out    : out std_logic_vector(31 downto 0);
        b_prog_out      : out std_logic;
        b_last_out      : out std_logic
    );
end multi_buffer;

architecture behavioural of multi_buffer is

    component bram is
        generic (
            DATA_WIDTH  : integer;
            ADDR_WIDTH  : integer
        );
        port (
            -- Port A
            a_clk   : in  std_logic;
            a_wr    : in  std_logic;
            a_addr  : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            a_din   : in  std_logic_vector(DATA_WIDTH-1 downto 0);
            a_dout  : out std_logic_vector(DATA_WIDTH-1 downto 0);

            -- Port B
            b_clk   : in  std_logic;
            b_wr    : in  std_logic;
            b_addr  : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            b_din   : in  std_logic_vector(DATA_WIDTH-1 downto 0);
            b_dout  : out std_logic_vector(DATA_WIDTH-1 downto 0)
        );
    end component bram;

    type mult_buffer_pe_id_type is array (NUM_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
    signal buf_pe_arr_id   : mult_buffer_pe_id_type;

    type mult_buffer_length_type is array (NUM_BUFFERS-1 downto 0) of std_logic_vector(31 downto 0);
    signal buf_length      : mult_buffer_length_type;

    signal buf_prog        : std_logic_vector(NUM_BUFFERS-1 downto 0);
    signal buf_last        : std_logic_vector(NUM_BUFFERS-1 downto 0);

    type vector_array_type is array(NUM_BUFFERS-1 downto 0) of std_logic_vector(DATA_WIDTH-1 downto 0);

    -- Port A
    signal buf_a_wr     : std_logic_vector(NUM_BUFFERS-1 downto 0);
    --signal buf_a_dout   : vector_array_type;

    -- Port B
    --signal buf_b_wr     : std_logic_vector(NUM_BUFFERS-1 downto 0);
    signal buf_b_dout   : vector_array_type;

    signal a_buffer_select_map  : integer;
    signal b_buffer_select_map  : integer;

begin

    a_buffer_select_map <= conv_integer(a_buffer_select) when unsigned(a_buffer_select) < NUM_BUFFERS else 0;
    b_buffer_select_map <= conv_integer(b_buffer_select) when unsigned(b_buffer_select) < NUM_BUFFERS else 0;

    --a_dout  <= buf_a_dout(a_buffer_select_map) when a_buffer_select_map >= 0 else (others => '0');
    b_dout  <= buf_b_dout(b_buffer_select_map) when b_buffer_select_map >= 0 else (others => '0');

    process(clk)
        variable tmp_addr : std_logic_vector(31 downto 0);
    begin
        if (rising_edge(clk)) then
            if (resetn = '0') then
                buf_prog <= (others => '0');
                buf_last <= (others => '0');

                for i in 0 to NUM_BUFFERS-1 loop
                    buf_pe_arr_id(i) <= (others => '0');
                    buf_length(i)    <= (others => '0');
                end loop;
            else
                if (a_wr = '1') then
                    tmp_addr(ADDR_WIDTH-1 downto 0)     := a_addr;
                    tmp_addr(31 downto ADDR_WIDTH)      := (others => '0');
                    buf_pe_arr_id(a_buffer_select_map)  <= a_pe_arr_id_in;
                    buf_length(a_buffer_select_map)     <= a_length_in;
                    buf_prog(a_buffer_select_map)       <= a_prog_in;
                    buf_last(a_buffer_select_map)       <= a_last_in;
                end if;

                --a_pe_arr_id_out <= buf_pe_arr_id(a_buffer_select_map);
                --a_length_out    <= buf_length(a_buffer_select_map);
                --a_prog_out      <= buf_prog(a_buffer_select_map);
                --a_last_out      <= buf_last(a_buffer_select_map);
            end if;
        end if;
    end process;

    process(clk)
    begin
        if (rising_edge(clk)) then
            if (resetn = '0') then
                b_pe_arr_id_out <= (others => '0');
                b_length_out    <= (others => '0');
                b_prog_out      <= '0';
                b_last_out      <= '0';
            else
                --if (b_wr = '1') then
                --    buf_pe_arr_id(b_buffer_select_map)  := b_pe_arr_id_in;
                --    buf_length(b_buffer_select_map)     := b_length_in;
                --    buf_prog(b_buffer_select_map)       := b_prog_in;
                --    buf_last(b_buffer_select_map)       := b_last_in;
                --end if;

                b_pe_arr_id_out <= buf_pe_arr_id(b_buffer_select_map);
                b_length_out    <= buf_length(b_buffer_select_map);
                b_prog_out      <= buf_prog(b_buffer_select_map);
                b_last_out      <= buf_last(b_buffer_select_map);
            end if;
        end if;
    end process;


    BUFFERS: for I in 0 to NUM_BUFFERS-1 generate
    begin
        buf_a_wr(I) <= a_wr when a_buffer_select_map = I else '0';
        --buf_b_wr(I) <= b_wr when b_buffer_select_map = I else '0';

        BUFFER_INST: bram
            generic map (
                DATA_WIDTH  => DATA_WIDTH,
                ADDR_WIDTH  => ADDR_WIDTH
            )
            port map (
                a_clk   => clk,
                a_wr    => buf_a_wr(I),
                a_addr  => a_addr,
                a_din   => a_din,
                a_dout  => open, --buf_a_dout(I),
                b_clk   => clk,
                b_wr    => '0', --buf_b_wr(I),
                b_addr  => b_addr,
                b_din   => (others => '0'), --b_din,
                b_dout  => buf_b_dout(I)
            );
    end generate BUFFERS;
--TODO: end for generate

end behavioural;
