library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_axis_to_pea is
    generic (
        -- Users to add parameters here

        C_BUFFER_SELECT_LENGTH  : integer   := 1;
        C_NUM_BUFFERS           : integer   := 2;
        C_BUFFER_ADDR_WIDTH     : integer   := 4;
        C_BURST_LEN             : integer   := 16;
        C_PE_ID_WIDTH           : integer   := 16;
        C_NUM_PE_ARRAYS         : integer   := 2;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
        C_M_AXIS_TDATA_WIDTH    : integer   := 32
    );
    port (
        -- Users to add ports here

        debug_out           : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);

        read_buffer_ready   : in  std_logic;
        read_buffer_free    : out std_logic;

        read_buffer_select  : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        read_buffer_addr    : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        read_buffer_data    : in  std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
        read_buffer_pe_id   : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0);
        read_buffer_length  : in  std_logic_vector(31 downto 0);
        read_buffer_prog    : in  std_logic;
        read_buffer_last    : in  std_logic;

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global ports
        M_AXIS_ACLK         : in  std_logic;
        M_AXIS_ARESETN      : in  std_logic;

        -- Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted.
        M_AXIS_TVALID       : out std_logic;
        -- TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
        M_AXIS_TDATA        : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
        -- TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
        M_AXIS_TSTRB        : out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
        -- TLAST indicates the boundary of a packet.
        M_AXIS_TLAST        : out std_logic;
        -- TREADY indicates that the slave can accept a transfer in the current cycle.
        M_AXIS_TREADY       : in  std_logic;
        -- TDEST is the ID of the destination component
        M_AXIS_TDEST        : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0)
    );
end faupu_axis_to_pea;

architecture implementation of faupu_axis_to_pea is

    -- Define the states of state machine
    -- The control state machine oversees the writing of input streaming data to the FIFO,
    -- and outputs the streaming data from the FIFO
    type state is ( IDLE,           -- This is the initial/idle state
                    FETCH_CONFIG, TO_PROG_MODE, TO_EXEC_MODE,
                    SEND_STREAM, SWITCH_BUFFER);   -- In this state the
                                    -- stream data is output through M_AXIS_TDATA
                                    -- State variable
    signal mst_exec_state       : state;

    -- Example design FIFO read pointer
    signal read_pointer         : integer range 0 to C_BURST_LEN;

    -- AXI Stream internal signals
    --wait counter. The master waits for the user defined number of clock cycles before initiating a transfer.
    signal count                : std_logic_vector(1 downto 0);
    --streaming data valid
    signal axis_tvalid          : std_logic;
    --Last of the streaming data
    signal axis_tlast           : std_logic;
    signal txn_en               : std_logic;
    --The master has issued all the streaming data stored in FIFO
    signal txn_done             : std_logic;

    signal read_buffer_free_ff      : std_logic;
    signal read_buffer_free_ff_prev : std_logic;
    signal num_read_buffers_ready   : integer;
    signal read_buffer_select_buf   : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal read_buffer_addr_buf     : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);

    signal fifo_length              : unsigned(31 downto 0);
    signal fifo_last                : std_logic;
    signal fifo_pe_id               : std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

    signal pe_arr_initialized       : std_logic_vector(C_NUM_PE_ARRAYS-1 downto 0);
    signal pe_arr_prog_mode         : std_logic_vector(C_NUM_PE_ARRAYS-1 downto 0);

    signal debug_0_out_buf          : std_logic_vector((C_M_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_1_out_buf          : std_logic_vector((C_M_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_2_out_buf          : std_logic_vector((C_M_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_3_out_buf          : std_logic_vector((C_M_AXIS_TDATA_WIDTH/4)-1 downto 0);

begin
    -- I/O Connections assignments

    M_AXIS_TVALID       <= axis_tvalid;
    M_AXIS_TDATA        <= read_buffer_data;
    M_AXIS_TLAST        <= axis_tlast;
    M_AXIS_TSTRB        <= (others => '1');
    M_AXIS_TDEST        <= fifo_pe_id;

    read_buffer_select  <= read_buffer_select_buf;
    read_buffer_addr    <= read_buffer_addr_buf;

    read_buffer_free    <= read_buffer_free_ff_prev and (not read_buffer_free_ff);

    debug_out           <= debug_3_out_buf & debug_2_out_buf & debug_1_out_buf & debug_0_out_buf;

    DEBUG_PROC: process(M_AXIS_ACLK)
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                debug_0_out_buf <= (others => '0');
                debug_1_out_buf <= (others => '0');
                debug_2_out_buf <= (others => '0');
                debug_3_out_buf <= (others => '0');
            else
                debug_0_out_buf <= std_logic_vector(to_unsigned(num_read_buffers_ready, C_M_AXIS_TDATA_WIDTH/4));

                if (txn_done = '1') then
                    debug_1_out_buf <= std_logic_vector(unsigned(debug_1_out_buf) + 1);
                end if;

                if (mst_exec_state = IDLE) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(1, C_M_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = FETCH_CONFIG) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(2, C_M_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = TO_PROG_MODE) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(3, C_M_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = TO_EXEC_MODE) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(4, C_M_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = SEND_STREAM) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(5, C_M_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = SWITCH_BUFFER) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(6, C_M_AXIS_TDATA_WIDTH/4));
                else
                    debug_2_out_buf <= std_logic_vector(to_unsigned(7, C_M_AXIS_TDATA_WIDTH/4));
                end if;

                debug_3_out_buf <= std_logic_vector(to_unsigned(read_pointer, C_M_AXIS_TDATA_WIDTH/4));
            end if;
        end if;
    end process;

    -- buffering the pulse that indicates that one of the read data buffers is free
    process(M_AXIS_ACLK)
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                read_buffer_free_ff         <= '0';
                read_buffer_free_ff_prev    <= '0';
                num_read_buffers_ready      <= 0;
            else
                read_buffer_free_ff_prev    <= read_buffer_free_ff;

                if (txn_done = '1') then
                    read_buffer_free_ff <= '1';

                    if (read_buffer_ready = '0') then
                        if (num_read_buffers_ready > 0) then
                            num_read_buffers_ready <= num_read_buffers_ready - 1;
                        end if;
                    end if;
                elsif (read_buffer_ready = '1') then
                    read_buffer_free_ff <= '0';

                    if (num_read_buffers_ready < C_NUM_BUFFERS) then
                        num_read_buffers_ready <= num_read_buffers_ready + 1;
                    end if;
                else
                    read_buffer_free_ff <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Control state machine implementation
    process(M_AXIS_ACLK)
        variable pe_id_int  : integer;
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                -- Synchronous reset (active low)
                mst_exec_state          <= IDLE;
                count                   <= (others => '0');
                read_buffer_select_buf  <= (others => '0');
                pe_arr_initialized      <= (others => '0');
                pe_arr_prog_mode        <= (others => '0');
                fifo_length             <= (others => '1');
                fifo_last               <= '0';
                fifo_pe_id              <= (others => '0');
            else
                case (mst_exec_state) is
                    when IDLE =>
                        -- The slave starts accepting tdata when
                        -- there tvalid is asserted to mark the
                        -- presence of valid streaming data
                        if (num_read_buffers_ready > 0) then
                            mst_exec_state  <= FETCH_CONFIG;
                            count           <= (others => '0');
                        else
                            mst_exec_state  <= IDLE;
                        end if;

                    when FETCH_CONFIG =>
                        -- Wait for another clock cycle for the config stuff
                        if (count = std_logic_vector(to_unsigned(1, 2))) then
                            pe_id_int := conv_integer(read_buffer_pe_id);

                            if (pe_arr_initialized(pe_id_int) = '0') then
                                if (read_buffer_prog = '1') then
                                    pe_arr_prog_mode(pe_id_int) <= '1';
                                    mst_exec_state <= TO_PROG_MODE;
                                else
                                    pe_arr_prog_mode(pe_id_int) <= '0';
                                    mst_exec_state <= TO_EXEC_MODE;
                                end if;

                                pe_arr_initialized(pe_id_int) <= '1';
                            elsif (read_buffer_prog = pe_arr_prog_mode(pe_id_int)) then
                                mst_exec_state <= SEND_STREAM;
                            elsif (read_buffer_prog = '1') then
                                mst_exec_state <= TO_PROG_MODE;
                            else
                                mst_exec_state <= TO_EXEC_MODE;
                            end if;

                            if (unsigned(read_buffer_length) > C_BURST_LEN) then
                                fifo_length <= to_unsigned(C_BURST_LEN, 32);
                            else
                                fifo_length <= unsigned(read_buffer_length);
                            end if;
                            fifo_last   <= read_buffer_last;
                            fifo_pe_id  <= read_buffer_pe_id;

                            pe_arr_prog_mode(pe_id_int) <= read_buffer_prog;
                        else
                            count           <= std_logic_vector(unsigned(count) + 1);
                            mst_exec_state  <= FETCH_CONFIG;
                        end if;

                    when TO_PROG_MODE =>
                        -- TODO: set prog mode
                        mst_exec_state <= SEND_STREAM;

                    when TO_EXEC_MODE =>
                        -- TODO: set exec mode
                        mst_exec_state <= SEND_STREAM;

                    when SEND_STREAM =>
                        -- The example design streaming master functionality starts
                        -- when the master drives output tdata from the FIFO and the slave
                        -- has finished storing the S_AXIS_TDATA
                        if (txn_done = '1' and txn_en = '1') then
                            mst_exec_state  <= SWITCH_BUFFER;
                        else
                            mst_exec_state  <= SEND_STREAM;
                        end if;

                    when SWITCH_BUFFER =>
                        if (unsigned(read_buffer_select_buf) = C_NUM_BUFFERS-1) then
                            read_buffer_select_buf <= (others => '0');
                        else
                            read_buffer_select_buf <= std_logic_vector(unsigned(read_buffer_select_buf) + 1);
                        end if;

                        mst_exec_state <= IDLE;

                    when others =>
                        mst_exec_state <= IDLE;

                end case;
            end if;
        end if;
    end process;


    -- tvalid generation
    -- axis_tvalid is asserted when the control state machine's state is SEND_STREAM and
    -- number of output streaming data is less than the C_BURST_LEN.
    --axis_tvalid <= '1' when ((mst_exec_state = SEND_STREAM) and (read_pointer < fifo_length)) else '0';

    -- AXI tlast generation
    -- axis_tlast is asserted number of output streaming data is C_BURST_LEN-1
    -- (0 to C_BURST_LEN-1)

    --process(M_AXIS_ACLK)
    --begin
    --    if (rising_edge(M_AXIS_ACLK)) then
    --        if (M_AXIS_ARESETN = '0') then
    --            axis_tlast <= '0';
    --        else
    --            if (read_pointer = fifo_length-1 and fifo_last = '1' and txn_en = '0') then
    --                axis_tlast <= '1';
    --            else
    --                axis_tlast <= '0';
    --            end if;
    --        end if;
    --    end if;
    --end process;

    --axis_tlast <= txn_done and txn_en and fifo_last when (read_pointer = fifo_length-1) else '0';
    axis_tlast <= txn_done and txn_en when (read_pointer = fifo_length-1) else '0';


    -- read_pointer pointer
    process(M_AXIS_ACLK)
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                read_pointer    <= 0;
                txn_done        <= '0';
            else
                txn_done        <= '0';

                if (mst_exec_state = SEND_STREAM) then
                    if (read_pointer < fifo_length-1) then
                        if (txn_en = '1') then
                            -- read pointer is incremented after every read from the FIFO
                            -- when FIFO read signal is enabled.
                            read_pointer    <= read_pointer + 1;
                        end if;
                    elsif (read_pointer = fifo_length-1) then
                        -- txn_done is asserted when C_BURST_LEN numbers of streaming data
                        -- has been out.
                        if (txn_en = '1') then
                            read_pointer    <= read_pointer + 1;
                        else
                            txn_done        <= '1';
                        end if;
                    elsif (read_pointer = fifo_length) then
                        read_pointer <= 0;
                    end if;
                else
                    read_pointer <= 0;
                end if;
            end if;
        end if;
    end process;

    process(M_AXIS_ACLK)
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                axis_tvalid <= '0';
            else
                if (txn_en = '1' and axis_tlast = '1') then
                    axis_tvalid <= '0';
                elsif (txn_en = '1') then
                    axis_tvalid <= '0';
                elsif (mst_exec_state = SEND_STREAM) then
                    axis_tvalid <= '1';
                else
                    axis_tvalid <= '0';
                end if;
            end if;
        end if;
    end process;


    -- FIFO read enable generation
    txn_en <= M_AXIS_TREADY and axis_tvalid;

    process(M_AXIS_ACLK)
    begin
        if (rising_edge(M_AXIS_ACLK)) then
            if (M_AXIS_ARESETN = '0') then
                read_buffer_addr_buf <= (others => '0');
            else
                if (mst_exec_state = IDLE) then
                    read_buffer_addr_buf <= (others => '0');
                elsif (mst_exec_state = SEND_STREAM) then
                    if (txn_en = '1') then
                        read_buffer_addr_buf <= std_logic_vector(unsigned(read_buffer_addr_buf) + 1);
                    end if;
                end if;
            end if;
        end if;
    end process;

end implementation;
