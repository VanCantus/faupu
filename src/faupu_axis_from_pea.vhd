library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity faupu_axis_from_pea is
    generic (
        -- Users to add parameters here

        C_BUFFER_SELECT_LENGTH  : integer   := 1;
        C_NUM_BUFFERS           : integer   := 2;
        C_BUFFER_ADDR_WIDTH     : integer   := 4;
        C_BURST_LEN             : integer   := 16;
        C_PE_ID_WIDTH           : integer   := 16;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- AXI4Stream sink: Data Width
        C_S_AXIS_TDATA_WIDTH    : integer   := 32
    );
    port (
        -- Users to add ports here

        debug_out           : out std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);

        write_buffer_ready  : out std_logic;
        write_buffer_free   : in  std_logic;

        write_buffer_select : out std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
        write_buffer_addr   : out std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
        write_buffer_data   : out std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        write_buffer_pe_id  : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0);
        write_buffer_length : out std_logic_vector(31 downto 0);
        write_buffer_last   : out std_logic;
        write_buffer_wren   : out std_logic;

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- AXI4Stream sink: Clock
        S_AXIS_ACLK         : in  std_logic;
        -- AXI4Stream sink: Reset
        S_AXIS_ARESETN      : in  std_logic;
        -- Ready to accept data in
        S_AXIS_TREADY       : out std_logic;
        -- Data in
        S_AXIS_TDATA        : in  std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        -- Byte qualifier
        S_AXIS_TSTRB        : in  std_logic_vector((C_S_AXIS_TDATA_WIDTH/8)-1 downto 0);
        -- Indicates boundary of last packet
        S_AXIS_TLAST        : in  std_logic;
        -- Data is in valid
        S_AXIS_TVALID       : in  std_logic;
        -- ID of the PE array from where the data comes
        S_AXIS_TDEST        : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0)
    );
end faupu_axis_from_pea;

architecture arch_imp of faupu_axis_from_pea is

    -- bit_num gives the minimum number of bits needed to address 'C_BURST_LEN' size of FIFO.
    constant bit_num        : integer := clogb2(C_BURST_LEN-1);

    -- Define the states of state machine
    -- The control state machine oversees the writing of input streaming data to the FIFO,
    -- and outputs the streaming data from the FIFO
    type state is ( IDLE,        -- This is the initial/idle state
                    WRITE_BUFFERED_WORD, WAIT_FOR_FURTHER_DATA, WRITE_FIFO, SWITCH_BUFFER); -- In this state FIFO is written with the
                                 -- input stream data S_AXIS_TDATA
    signal mst_exec_state   : state;

    -- FIFO implementation signals
    signal axis_tready      : std_logic;
    -- State variable
    signal  byte_index      : integer;
    -- FIFO write enable
    signal fifo_wren        : std_logic;
    -- FIFO full flag
    --signal fifo_full_flag   : std_logic;
    -- FIFO write pointer
    signal write_pointer    : integer range 0 to C_BURST_LEN;
    -- sink has accepted all the streaming data and stored in FIFO
    signal writes_done      : std_logic;

    type BYTE_FIFO_TYPE is array (0 to (C_BURST_LEN-1)) of std_logic_vector(((C_S_AXIS_TDATA_WIDTH/4)-1)downto 0);

    signal num_write_buffers_free       : integer;
    signal write_buffer_ready_ff        : std_logic;
    signal write_buffer_ready_ff_prev   : std_logic;

    signal write_buffer_select_buf      : std_logic_vector(C_BUFFER_SELECT_LENGTH-1 downto 0);
    signal write_buffer_pe_id_buf       : std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

    signal write_buffer_addr_buf        : std_logic_vector(C_BUFFER_ADDR_WIDTH-1 downto 0);
    signal write_buffer_length_buf      : std_logic_vector(31 downto 0);

    signal buffer_other_pea             : std_logic;
    signal other_pea_data               : std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
    signal other_pea_id                 : std_logic_vector(C_PE_ID_WIDTH-1 downto 0);
    signal other_pea_last               : std_logic;

    signal debug_0_out_buf              : std_logic_vector((C_S_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_1_out_buf              : std_logic_vector((C_S_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_2_out_buf              : std_logic_vector((C_S_AXIS_TDATA_WIDTH/4)-1 downto 0);
    signal debug_3_out_buf              : std_logic_vector((C_S_AXIS_TDATA_WIDTH/4)-1 downto 0);

begin

    -- I/O Connections assignments

    S_AXIS_TREADY       <= axis_tready;
    write_buffer_ready  <= write_buffer_ready_ff_prev and (not write_buffer_ready_ff);
    --write_buffer_wren   <= fifo_wren;
    --write_buffer_addr   <= std_logic_vector(to_unsigned(write_pointer, C_BUFFER_ADDR_WIDTH));
    --write_buffer_data   <= S_AXIS_TDATA;
    --write_buffer_last   <= S_AXIS_TLAST;
    --write_buffer_length <= std_logic_vector(to_unsigned(write_pointer + 1, 32));
    --write_buffer_pe_id  <= write_buffer_pe_id_buf;
    write_buffer_select <= write_buffer_select_buf;
    write_buffer_addr   <= write_buffer_addr_buf;
    write_buffer_length <= write_buffer_length_buf;

    debug_out           <= debug_3_out_buf & debug_2_out_buf & debug_1_out_buf & debug_0_out_buf;

    DEBUG_PROC: process(S_AXIS_ACLK)
    begin
        if (rising_edge(S_AXIS_ACLK)) then
            if (S_AXIS_ARESETN = '0') then
                debug_0_out_buf <= (others => '0');
                debug_1_out_buf <= (others => '0');
                debug_2_out_buf <= (others => '0');
                debug_3_out_buf <= (others => '0');
            else
                debug_0_out_buf <= std_logic_vector(to_unsigned(num_write_buffers_free, C_S_AXIS_TDATA_WIDTH/4));

                if (writes_done = '1') then
                    debug_1_out_buf <= std_logic_vector(unsigned(debug_1_out_buf) + 1);
                end if;

                if (mst_exec_state = IDLE) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(1, C_S_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = WRITE_FIFO) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(2, C_S_AXIS_TDATA_WIDTH/4));
                elsif (mst_exec_state = SWITCH_BUFFER) then
                    debug_2_out_buf <= std_logic_vector(to_unsigned(3, C_S_AXIS_TDATA_WIDTH/4));
                else
                    debug_2_out_buf <= std_logic_vector(to_unsigned(4, C_S_AXIS_TDATA_WIDTH/4));
                end if;

                debug_3_out_buf <= std_logic_vector(to_unsigned(write_pointer, C_S_AXIS_TDATA_WIDTH/4));
            end if;
        end if;
    end process;

    -- buffering the pulse that indicates that one of the read data buffers has data available
    process(S_AXIS_ACLK)
    begin
        if (rising_edge(S_AXIS_ACLK)) then
            if (S_AXIS_ARESETN = '0') then
                write_buffer_ready_ff        <= '0';
                write_buffer_ready_ff_prev   <= '0';
                num_write_buffers_free       <= C_NUM_BUFFERS;
            else
                write_buffer_ready_ff_prev   <= write_buffer_ready_ff;

                if (writes_done = '1') then
                    write_buffer_ready_ff <= '1';

                    if (write_buffer_free = '0') then
                        if (num_write_buffers_free > 0) then
                            num_write_buffers_free <= num_write_buffers_free - 1;
                        end if;
                    end if;
                elsif (write_buffer_free = '1') then
                    write_buffer_ready_ff <= '0';

                    if (num_write_buffers_free < C_NUM_BUFFERS) then
                        num_write_buffers_free <= num_write_buffers_free + 1;
                    end if;
                else
                    write_buffer_ready_ff <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Control state machine implementation
    process(S_AXIS_ACLK)
    begin
        if (rising_edge (S_AXIS_ACLK)) then
            if (S_AXIS_ARESETN = '0') then
                -- Synchronous reset (active low)
                mst_exec_state          <= IDLE;
                write_buffer_select_buf <= (others => '0');
            else
                case (mst_exec_state) is
                    when IDLE =>
                        -- The sink starts accepting tdata when
                        -- there tvalid is asserted to mark the
                        -- presence of valid streaming data
                        if (num_write_buffers_free > 0 and buffer_other_pea = '1') then
                            mst_exec_state          <= WRITE_BUFFERED_WORD;
                            write_buffer_pe_id_buf  <= other_pea_id;
                        elsif (S_AXIS_TVALID = '1' and num_write_buffers_free > 0) then
                            mst_exec_state          <= WRITE_FIFO;
                            write_buffer_pe_id_buf  <= S_AXIS_TDEST;
                        else
                            mst_exec_state <= IDLE;
                        end if;

                    when WRITE_BUFFERED_WORD =>
                        mst_exec_state <= WAIT_FOR_FURTHER_DATA;

                    when WAIT_FOR_FURTHER_DATA =>
                        if (S_AXIS_TVALID = '1') then
                            if (S_AXIS_TDEST = write_buffer_pe_id_buf) then
                                mst_exec_state <= WRITE_FIFO;
                            else
                                mst_exec_state <= SWITCH_BUFFER;
                            end if;
                        end if;

                    when WRITE_FIFO =>
                        -- When the sink has accepted all the streaming input data,
                        -- the interface swiches functionality to a streaming master
                        if (writes_done = '1') then
                            mst_exec_state <= SWITCH_BUFFER;
                        else
                            -- The sink accepts and stores tdata
                            -- into FIFO
                            mst_exec_state <= WRITE_FIFO;
                        end if;

                    when SWITCH_BUFFER =>
                        if (unsigned(write_buffer_select_buf) = C_NUM_BUFFERS-1) then
                            write_buffer_select_buf <= (others => '0');
                        else
                            write_buffer_select_buf <= std_logic_vector(unsigned(write_buffer_select_buf) + 1);
                        end if;

                        mst_exec_state <= IDLE;

                    when others =>
                        mst_exec_state <= IDLE;
                end case;
            end if;
        end if;
    end process;


    fifo_wren <= S_AXIS_TVALID and axis_tready;

    process(S_AXIS_ACLK)
    begin
        if (rising_edge(S_AXIS_ACLK)) then
            if (S_AXIS_ARESETN = '0' or mst_exec_state = IDLE) then
                write_buffer_wren       <= '0';
                write_buffer_addr_buf   <= (others => '0');
                write_buffer_length_buf <= (others => '0');
                write_buffer_last       <= '0';
                write_buffer_data       <= (others => '0');
                write_buffer_pe_id      <= (others => '0');
                buffer_other_pea        <= '0';
                other_pea_data          <= (others => '0');
                other_pea_id            <= (others => '0');
                other_pea_last          <= '0';
            else
                if (mst_exec_state = WRITE_BUFFERED_WORD) then
                    buffer_other_pea        <= '0';

                    write_buffer_wren       <= '1';
                    write_buffer_addr_buf   <= (others => '0');
                    write_buffer_length_buf <= std_logic_vector(to_unsigned(1, 32));
                    write_buffer_last       <= other_pea_last;
                    write_buffer_data       <= other_pea_data;
                    write_buffer_pe_id      <= write_buffer_pe_id_buf;
                elsif (fifo_wren = '1' and S_AXIS_TDEST = write_buffer_pe_id_buf) then
                    write_buffer_wren       <= '1';
                    write_buffer_addr_buf   <= write_buffer_length_buf(C_BUFFER_ADDR_WIDTH-1 downto 0);
                    write_buffer_length_buf <= std_logic_vector(unsigned(write_buffer_length_buf) + 1);
                    write_buffer_last       <= S_AXIS_TLAST;
                    write_buffer_data       <= S_AXIS_TDATA;
                    write_buffer_pe_id      <= write_buffer_pe_id_buf;
                elsif (fifo_wren = '1' and S_AXIS_TDEST /= write_buffer_pe_id_buf) then
                    write_buffer_wren       <= '0';
                    buffer_other_pea        <= '1';
                    other_pea_data          <= S_AXIS_TDATA;
                    other_pea_id            <= S_AXIS_TDEST;
                    other_pea_last          <= S_AXIS_TLAST;
                else
                    write_buffer_wren       <= '0';
                end if;
            end if;
        end if;
    end process;

    -- AXI Streaming Sink
    --
    -- The example design sink is always ready to accept the S_AXIS_TDATA  until
    -- the FIFO is not filled with C_BURST_LEN number of input words.
    axis_tready <= '1' when ((mst_exec_state = WRITE_FIFO) and (write_pointer <= C_BURST_LEN-1) and writes_done = '0') else '0';

    process(S_AXIS_ACLK)
    begin
        if (rising_edge(S_AXIS_ACLK)) then
            if (S_AXIS_ARESETN = '0') then
                write_pointer   <= 0;
                writes_done     <= '0';
            else
                writes_done     <= '0';

                if ((mst_exec_state = WAIT_FOR_FURTHER_DATA) and (S_AXIS_TVALID = '1') and (S_AXIS_TDEST /= write_buffer_pe_id_buf)) then
                    if (S_AXIS_TDEST /= write_buffer_pe_id_buf) then
                        write_pointer   <= 0;
                        writes_done     <= '1';
                    else
                        write_pointer   <= 1;
                    end if;
                elsif (write_pointer <= C_BURST_LEN-1) then
                    if (fifo_wren = '1') then
                        if ((write_pointer = C_BURST_LEN-1) or (S_AXIS_TLAST = '1') or (S_AXIS_TDEST /= write_buffer_pe_id_buf)) then
                            write_pointer   <= 0;
                            writes_done     <= '1';
                        else
                            write_pointer   <= write_pointer + 1;
                        end if;
                    end if;
                end  if;
            end if;
        end if;
    end process;

end arch_imp;
