library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package faupu_dma_pkg is
    function log2(i : integer) return integer;
	function clogb2(bit_depth : integer) return integer;
    function gcd(a : integer; b : integer) return integer;
    function floor2pow2(i : integer) return integer;
end;

package body faupu_dma_pkg is

    function log2(i : integer) return integer is
        variable temp    : integer := i;
        variable ret_val : integer := 0; 
    begin                 
        while temp > 1 loop
            ret_val := ret_val + 1;
            temp    := temp / 2;     
        end loop;

        return ret_val;
    end function;

	-- function called clogb2 that returns an integer which has the
	--value of the ceiling of the log base 2
	function clogb2(bit_depth : integer) return integer is
	 	variable depth  : integer := bit_depth;
	 	variable count  : integer := 1;
	begin
	    for clogb2 in 1 to bit_depth loop  -- Works for up to 32 bit integers
	        if (bit_depth <= 2) then
	            count := 1;
	        else
	            if(depth <= 1) then
	 	            count := count;
	 	        else
	 	            depth := depth / 2;
	                count := count + 1;
	 	        end if;
		    end if;
	    end loop;
	    return(count);
	end;

    function gcd(a : integer; b : integer) return integer is
        variable ta    : integer := a;
        variable tb    : integer := b;
        variable c     : integer;
    begin
        while (ta /= 0) loop
           c  := ta;
           ta := tb mod ta;
           tb := c;
       end loop;

       return(tb);
    end;

    -- returns the nearest power of 2 that is less or equal to i
    function floor2pow2(i : integer) return integer is
        variable temp : integer := 1;
    begin
        while (temp <= i) loop
            temp := temp * 2;
        end loop;

        return (temp / 2);
    end;

end package body;
