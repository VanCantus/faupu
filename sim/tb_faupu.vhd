library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity tb_faupu is
end tb_faupu;

architecture bhv of tb_faupu is

    component faupu_dma is
        generic (
            -- Users to add parameters here

            C_NUM_IOTABLE_ROWS          : integer   := 16;
            DATA_BUFFER_NUM_BUFFERS     : integer   := 2;
            C_PE_ID_WIDTH               : integer   := 16;
            C_NUM_PE_ARRAYS             : integer   := 2;

            -- User parameters ends
            -- Do not modify the parameters beyond this line

            -- Parameters of Axi Slave Bus Interface AXI_SLAVE
            --C_AXI_SLAVE_ID_WIDTH        : integer   := 1;
            --C_AXI_SLAVE_ADDR_WIDTH      : integer   := 8;
            --C_AXI_SLAVE_AWUSER_WIDTH    : integer   := 0;
            --C_AXI_SLAVE_ARUSER_WIDTH    : integer   := 0;
            --C_AXI_SLAVE_WUSER_WIDTH     : integer   := 0;
            --C_AXI_SLAVE_RUSER_WIDTH     : integer   := 0;
            --C_AXI_SLAVE_BUSER_WIDTH     : integer   := 0;

            -- Parameters of Axi Slave Bus Interface AXI_LITE
            C_AXI_LITE_ADDR_WIDTH       : integer   := 5;

            -- Parameters of Axi Master Bus Interface IOTABLE_FILL
            C_AXI_MASTER_BURST_LEN      : integer   := 16;
            C_AXI_MASTER_ID_WIDTH       : integer   := 1;
            C_AXI_MASTER_ADDR_WIDTH     : integer   := 32;
            C_AXI_MASTER_AWUSER_WIDTH   : integer   := 0;
            C_AXI_MASTER_ARUSER_WIDTH   : integer   := 0;
            C_AXI_MASTER_WUSER_WIDTH    : integer   := 0;
            C_AXI_MASTER_RUSER_WIDTH    : integer   := 0;
            C_AXI_MASTER_BUSER_WIDTH    : integer   := 0;

            -- Parameters of Axi Slave Bus Interface S_AXI_INTR
            C_S_AXI_INTR_DATA_WIDTH     : integer   := 32;
            C_S_AXI_INTR_ADDR_WIDTH     : integer   := 5;
            C_NUM_OF_INTR               : integer   := 1;
            C_INTR_SENSITIVITY          : std_logic_vector  := x"FFFFFFFF";
            C_INTR_ACTIVE_STATE         : std_logic_vector  := x"FFFFFFFF";
            C_IRQ_SENSITIVITY           : integer   := 1;
            C_IRQ_ACTIVE_STATE          : integer   := 1
        );
        port (
            -- Users to add ports here

            -- User ports ends
            -- Do not modify the ports beyond this line

            axi_aclk         : in  std_logic;
            axi_aresetn      : in  std_logic;

            -- Ports of Axi Slave Bus Interface CONFIG (AXI Slave Lite)
            dma_config_awaddr       : in  std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
            dma_config_awprot       : in  std_logic_vector(2 downto 0);
            dma_config_awvalid      : in  std_logic;
            dma_config_awready      : out std_logic;
            dma_config_wdata        : in  std_logic_vector(31 downto 0);
            dma_config_wstrb        : in  std_logic_vector((32/8)-1 downto 0);
            dma_config_wvalid       : in  std_logic;
            dma_config_wready       : out std_logic;
            dma_config_bresp        : out std_logic_vector(1 downto 0);
            dma_config_bvalid       : out std_logic;
            dma_config_bready       : in  std_logic;
            dma_config_araddr       : in  std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
            dma_config_arprot       : in  std_logic_vector(2 downto 0);
            dma_config_arvalid      : in  std_logic;
            dma_config_arready      : out std_logic;
            dma_config_rdata        : out std_logic_vector(31 downto 0);
            dma_config_rresp        : out std_logic_vector(1 downto 0);
            dma_config_rvalid       : out std_logic;
            dma_config_rready       : in  std_logic;

            -- Ports of Axi Master Bus Interface IOTABLE_FILL (AXI Master Full)
            iotable_fill_init_axi_txn   : in  std_logic;
            iotable_fill_txn_done   : out std_logic;
            iotable_fill_error      : out std_logic;
            iotable_fill_awid       : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            iotable_fill_awaddr     : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
            iotable_fill_awlen      : out std_logic_vector(7 downto 0);
            iotable_fill_awsize     : out std_logic_vector(2 downto 0);
            iotable_fill_awburst    : out std_logic_vector(1 downto 0);
            iotable_fill_awlock     : out std_logic;
            iotable_fill_awcache    : out std_logic_vector(3 downto 0);
            iotable_fill_awprot     : out std_logic_vector(2 downto 0);
            iotable_fill_awqos      : out std_logic_vector(3 downto 0);
            iotable_fill_awuser     : out std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
            iotable_fill_awvalid    : out std_logic;
            iotable_fill_awready    : in  std_logic;
            iotable_fill_wdata      : out std_logic_vector(31 downto 0);
            iotable_fill_wstrb      : out std_logic_vector((32/8)-1 downto 0);
            iotable_fill_wlast      : out std_logic;
            iotable_fill_wuser      : out std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
            iotable_fill_wvalid     : out std_logic;
            iotable_fill_wready     : in  std_logic;
            iotable_fill_bid        : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            iotable_fill_bresp      : in  std_logic_vector(1 downto 0);
            iotable_fill_buser      : in  std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
            iotable_fill_bvalid     : in  std_logic;
            iotable_fill_bready     : out std_logic;
            iotable_fill_arid       : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            iotable_fill_araddr     : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
            iotable_fill_arlen      : out std_logic_vector(7 downto 0);
            iotable_fill_arsize     : out std_logic_vector(2 downto 0);
            iotable_fill_arburst    : out std_logic_vector(1 downto 0);
            iotable_fill_arlock     : out std_logic;
            iotable_fill_arcache    : out std_logic_vector(3 downto 0);
            iotable_fill_arprot     : out std_logic_vector(2 downto 0);
            iotable_fill_arqos      : out std_logic_vector(3 downto 0);
            iotable_fill_aruser     : out std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
            iotable_fill_arvalid    : out std_logic;
            iotable_fill_arready    : in  std_logic;
            iotable_fill_rid        : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            iotable_fill_rdata      : in  std_logic_vector(31 downto 0);
            iotable_fill_rresp      : in  std_logic_vector(1 downto 0);
            iotable_fill_rlast      : in  std_logic;
            iotable_fill_ruser      : in  std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
            iotable_fill_rvalid     : in  std_logic;
            iotable_fill_rready     : out std_logic;

            -- Ports of Axi Master Bus Interface DATASTREAM (AXI Master Full)
            datastream_init_axi_txn : in  std_logic;
            datastream_txn_done     : out std_logic;
            datastream_error        : out std_logic;
            datastream_awid         : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            datastream_awaddr       : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
            datastream_awlen        : out std_logic_vector(7 downto 0);
            datastream_awsize       : out std_logic_vector(2 downto 0);
            datastream_awburst      : out std_logic_vector(1 downto 0);
            datastream_awlock       : out std_logic;
            datastream_awcache      : out std_logic_vector(3 downto 0);
            datastream_awprot       : out std_logic_vector(2 downto 0);
            datastream_awqos        : out std_logic_vector(3 downto 0);
            datastream_awuser       : out std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
            datastream_awvalid      : out std_logic;
            datastream_awready      : in  std_logic;
            datastream_wdata        : out std_logic_vector(31 downto 0);
            datastream_wstrb        : out std_logic_vector((32/8)-1 downto 0);
            datastream_wlast        : out std_logic;
            datastream_wuser        : out std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
            datastream_wvalid       : out std_logic;
            datastream_wready       : in  std_logic;
            datastream_bid          : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            datastream_bresp        : in  std_logic_vector(1 downto 0);
            datastream_buser        : in  std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
            datastream_bvalid       : in  std_logic;
            datastream_bready       : out std_logic;
            datastream_arid         : out std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            datastream_araddr       : out std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
            datastream_arlen        : out std_logic_vector(7 downto 0);
            datastream_arsize       : out std_logic_vector(2 downto 0);
            datastream_arburst      : out std_logic_vector(1 downto 0);
            datastream_arlock       : out std_logic;
            datastream_arcache      : out std_logic_vector(3 downto 0);
            datastream_arprot       : out std_logic_vector(2 downto 0);
            datastream_arqos        : out std_logic_vector(3 downto 0);
            datastream_aruser       : out std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
            datastream_arvalid      : out std_logic;
            datastream_arready      : in  std_logic;
            datastream_rid          : in  std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
            datastream_rdata        : in  std_logic_vector(31 downto 0);
            datastream_rresp        : in  std_logic_vector(1 downto 0);
            datastream_rlast        : in  std_logic;
            datastream_ruser        : in  std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
            datastream_rvalid       : in  std_logic;
            datastream_rready       : out std_logic;

            -- Ports of AXI Lite Master Bus Interface pea_config
            pea_config_awaddr       : out std_logic_vector(31 downto 0);
            pea_config_awprot       : out std_logic_vector(2 downto 0);
            pea_config_awvalid      : out std_logic;
            pea_config_awready      : in  std_logic;
            pea_config_wdata        : out std_logic_vector(31 downto 0);
            pea_config_wstrb        : out std_logic_vector(32/8-1 downto 0);
            pea_config_wvalid       : out std_logic;
            pea_config_wready       : in  std_logic;
            pea_config_bresp        : in  std_logic_vector(1 downto 0);
            pea_config_bvalid       : in  std_logic;
            pea_config_bready       : out std_logic;
            pea_config_araddr       : out std_logic_vector(31 downto 0);
            pea_config_arprot       : out std_logic_vector(2 downto 0);
            pea_config_arvalid      : out std_logic;
            pea_config_arready      : in  std_logic;
            pea_config_rdata        : in  std_logic_vector(31 downto 0);
            pea_config_rresp        : in  std_logic_vector(1 downto 0);
            pea_config_rvalid       : in  std_logic;
            pea_config_rready       : out std_logic;

            -- Ports of Axi Master Bus Interface to_pea
            to_pea_tvalid           : out std_logic;
            to_pea_tdata            : out std_logic_vector(31 downto 0);
            to_pea_tstrb            : out std_logic_vector((32/8)-1 downto 0);
            to_pea_tlast            : out std_logic;
            to_pea_tready           : in  std_logic;
            to_pea_tdest            : out std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

            -- Ports of Axi Slave Bus Interface from_pea
            from_pea_tready         : out std_logic;
            from_pea_tdata          : in  std_logic_vector(31 downto 0);
            from_pea_tstrb          : in  std_logic_vector((32/8)-1 downto 0);
            from_pea_tlast          : in  std_logic;
            from_pea_tvalid         : in  std_logic;
            from_pea_tdest          : in  std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

            -- Ports of Axi Slave Bus Interface S_AXI_INTR
            s_axi_intr_awaddr       : in  std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
            s_axi_intr_awprot       : in  std_logic_vector(2 downto 0);
            s_axi_intr_awvalid      : in  std_logic;
            s_axi_intr_awready      : out std_logic;
            s_axi_intr_wdata        : in  std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
            s_axi_intr_wstrb        : in  std_logic_vector((C_S_AXI_INTR_DATA_WIDTH/8)-1 downto 0);
            s_axi_intr_wvalid       : in  std_logic;
            s_axi_intr_wready       : out std_logic;
            s_axi_intr_bresp        : out std_logic_vector(1 downto 0);
            s_axi_intr_bvalid       : out std_logic;
            s_axi_intr_bready       : in  std_logic;
            s_axi_intr_araddr       : in  std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
            s_axi_intr_arprot       : in  std_logic_vector(2 downto 0);
            s_axi_intr_arvalid      : in  std_logic;
            s_axi_intr_arready      : out std_logic;
            s_axi_intr_rdata        : out std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
            s_axi_intr_rresp        : out std_logic_vector(1 downto 0);
            s_axi_intr_rvalid       : out std_logic;
            s_axi_intr_rready       : in  std_logic;
            irq                     : out std_logic
        );
    end component;

    component tb_faupu_AXI_FULL_SLAVE is
        generic (
            C_S_AXI_ID_WIDTH        : integer   := 1;
            C_S_AXI_DATA_WIDTH      : integer   := 32;
            C_S_AXI_ADDR_WIDTH      : integer   := 12;
            C_S_AXI_AWUSER_WIDTH    : integer   := 0;
            C_S_AXI_ARUSER_WIDTH    : integer   := 0;
            C_S_AXI_WUSER_WIDTH     : integer   := 0;
            C_S_AXI_RUSER_WIDTH     : integer   := 0;
            C_S_AXI_BUSER_WIDTH     : integer   := 0
        );
        port (
            mem_wren        : out std_logic;
            mem_addr_write  : out std_logic_vector(C_S_AXI_ADDR_WIDTH-(C_S_AXI_DATA_WIDTH/32 + 1)-1 downto 0);
            data_out        : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            mem_rden        : out std_logic;
            mem_addr_read   : out std_logic_vector(C_S_AXI_ADDR_WIDTH-(C_S_AXI_DATA_WIDTH/32 + 1)-1 downto 0);
            data_in         : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_ACLK      : in  std_logic;
            S_AXI_ARESETN   : in  std_logic;
            S_AXI_AWID      : in  std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
            S_AXI_AWADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_AWLEN     : in  std_logic_vector(7 downto 0);
            S_AXI_AWSIZE    : in  std_logic_vector(2 downto 0);
            S_AXI_AWBURST   : in  std_logic_vector(1 downto 0);
            S_AXI_AWLOCK    : in  std_logic;
            S_AXI_AWCACHE   : in  std_logic_vector(3 downto 0);
            S_AXI_AWPROT    : in  std_logic_vector(2 downto 0);
            S_AXI_AWQOS     : in  std_logic_vector(3 downto 0);
            S_AXI_AWREGION  : in  std_logic_vector(3 downto 0);
            S_AXI_AWUSER    : in  std_logic_vector(C_S_AXI_AWUSER_WIDTH-1 downto 0);
            S_AXI_AWVALID   : in  std_logic;
            S_AXI_AWREADY   : out std_logic;
            S_AXI_WDATA     : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_WSTRB     : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
            S_AXI_WLAST     : in  std_logic;
            S_AXI_WUSER     : in  std_logic_vector(C_S_AXI_WUSER_WIDTH-1 downto 0);
            S_AXI_WVALID    : in  std_logic;
            S_AXI_WREADY    : out std_logic;
            S_AXI_BID       : out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
            S_AXI_BRESP     : out std_logic_vector(1 downto 0);
            S_AXI_BUSER     : out std_logic_vector(C_S_AXI_BUSER_WIDTH-1 downto 0);
            S_AXI_BVALID    : out std_logic;
            S_AXI_BREADY    : in  std_logic;
            S_AXI_ARID      : in  std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
            S_AXI_ARADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_ARLEN     : in  std_logic_vector(7 downto 0);
            S_AXI_ARSIZE    : in  std_logic_vector(2 downto 0);
            S_AXI_ARBURST   : in  std_logic_vector(1 downto 0);
            S_AXI_ARLOCK    : in  std_logic;
            S_AXI_ARCACHE   : in  std_logic_vector(3 downto 0);
            S_AXI_ARPROT    : in  std_logic_vector(2 downto 0);
            S_AXI_ARQOS     : in  std_logic_vector(3 downto 0);
            S_AXI_ARREGION  : in  std_logic_vector(3 downto 0);
            S_AXI_ARUSER    : in  std_logic_vector(C_S_AXI_ARUSER_WIDTH-1 downto 0);
            S_AXI_ARVALID   : in  std_logic;
            S_AXI_ARREADY   : out std_logic;
            S_AXI_RID       : out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
            S_AXI_RDATA     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_RRESP     : out std_logic_vector(1 downto 0);
            S_AXI_RLAST     : out std_logic;
            S_AXI_RUSER     : out std_logic_vector(C_S_AXI_RUSER_WIDTH-1 downto 0);
            S_AXI_RVALID    : out std_logic;
            S_AXI_RREADY    : in  std_logic
        );
    end component;

    component tb_faupu_AXI_LITE_MASTER is
        generic (
            C_M_AXI_ADDR_WIDTH  : integer   := 32;
            C_M_AXI_DATA_WIDTH  : integer   := 32
        );
        port (
            write_register  : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-3 downto 0);
            write_data      : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            INIT_AXI_TXN    : in  std_logic;
            ERROR           : out std_logic;
            TXN_DONE        : out std_logic;
            M_AXI_ACLK      : in  std_logic;
            M_AXI_ARESETN   : in  std_logic;
            M_AXI_AWADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_AWPROT    : out std_logic_vector(2 downto 0);
            M_AXI_AWVALID   : out std_logic;
            M_AXI_AWREADY   : in  std_logic;
            M_AXI_WDATA     : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_WSTRB     : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
            M_AXI_WVALID    : out std_logic;
            M_AXI_WREADY    : in  std_logic;
            M_AXI_BRESP     : in  std_logic_vector(1 downto 0);
            M_AXI_BVALID    : in  std_logic;
            M_AXI_BREADY    : out std_logic;
            M_AXI_ARADDR    : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_ARPROT    : out std_logic_vector(2 downto 0);
            M_AXI_ARVALID   : out std_logic;
            M_AXI_ARREADY   : in  std_logic;
            M_AXI_RDATA     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_RRESP     : in  std_logic_vector(1 downto 0);
            M_AXI_RVALID    : in  std_logic;
            M_AXI_RREADY    : out std_logic
        );
    end component;

    component tb_faupu_AXI_LITE_SLAVE is
        generic (
            C_NUM_REGS          : integer   := 16;
            C_S_AXI_DATA_WIDTH  : integer   := 32;
            C_S_AXI_ADDR_WIDTH  : integer   := 4
        );
        port (
            S_AXI_ACLK          : in  std_logic;
            S_AXI_ARESETN       : in  std_logic;
            S_AXI_AWADDR        : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_AWPROT        : in  std_logic_vector(2 downto 0);
            S_AXI_AWVALID       : in  std_logic;
            S_AXI_AWREADY       : out std_logic;
            S_AXI_WDATA         : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_WSTRB         : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
            S_AXI_WVALID        : in  std_logic;
            S_AXI_WREADY        : out std_logic;
            S_AXI_BRESP         : out std_logic_vector(1 downto 0);
            S_AXI_BVALID        : out std_logic;
            S_AXI_BREADY        : in  std_logic;
            S_AXI_ARADDR        : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
            S_AXI_ARPROT        : in  std_logic_vector(2 downto 0);
            S_AXI_ARVALID       : in  std_logic;
            S_AXI_ARREADY       : out std_logic;
            S_AXI_RDATA         : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            S_AXI_RRESP         : out std_logic_vector(1 downto 0);
            S_AXI_RVALID        : out std_logic;
            S_AXI_RREADY        : in  std_logic
        );
    end component tb_faupu_AXI_LITE_SLAVE;

    -- CONSTANTS --

    -- local constants
    constant C_DATA_WIDTH       : integer   := 32;
    constant C_MEM_ADDR_WIDTH   : integer   := 13;
    constant CLK_PERIOD : time  := 10 ns;

    -- FAUPU constants
    constant C_NUM_IOTABLE_ROWS             : integer   := 16;
    constant C_NUM_PE_ARRAYS                : integer   := 2;
    constant DATA_BUFFER_NUM_BUFFERS        : integer   := 2;
    constant C_AXI_LITE_ADDR_WIDTH          : integer   := 5;
    constant C_AXI_MASTER_BURST_LEN         : integer   := 32;
    constant C_AXI_MASTER_ID_WIDTH          : integer   := 1;
    constant C_AXI_MASTER_ADDR_WIDTH        : integer   := 32;
    constant C_AXI_MASTER_AWUSER_WIDTH      : integer   := 0;
    constant C_AXI_MASTER_ARUSER_WIDTH      : integer   := 0;
    constant C_AXI_MASTER_WUSER_WIDTH       : integer   := 0;
    constant C_AXI_MASTER_RUSER_WIDTH       : integer   := 0;
    constant C_AXI_MASTER_BUSER_WIDTH       : integer   := 0;
    constant C_PE_ID_WIDTH                  : integer   := 16;


    -- SIGNALS --

    -- local signals
    signal axi_aclk     : std_logic := '0';
    signal axi_aresetn  : std_logic := '0';

    signal a_wr         : std_logic;
    signal b_wr         : std_logic;
    signal c_wr         : std_logic;
    signal d_wr         : std_logic;
    signal a_addr       : std_logic_vector(C_MEM_ADDR_WIDTH-1 downto 0);
    signal b_addr       : std_logic_vector(C_MEM_ADDR_WIDTH-1 downto 0);
    signal c_addr       : std_logic_vector(C_MEM_ADDR_WIDTH-1 downto 0);
    signal d_addr       : std_logic_vector(C_MEM_ADDR_WIDTH-1 downto 0);
    signal a_din        : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal b_din        : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal c_din        : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal d_din        : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal a_dout       : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal b_dout       : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal c_dout       : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal d_dout       : std_logic_vector(C_DATA_WIDTH-1 downto 0);

    signal dma_config_write_register        : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-3 downto 0) := (others => '0');
    signal dma_config_write_data            : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0');
    signal dma_config_init_axi_txn          : std_logic := '0';
    signal dma_config_error                 : std_logic;
    signal dma_config_txn_done              : std_logic;

    signal irq_write_register               : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-3 downto 0) := (others => '0');
    signal irq_write_data                   : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0');
    signal irq_init_axi_txn                 : std_logic := '0';
    signal irq_error                        : std_logic;
    signal irq_txn_done                     : std_logic;

    signal iotab_fill_slave_mem_wren        : std_logic;
    signal iotab_fill_slave_mem_addr_write  : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal iotab_fill_slave_data_out        : std_logic_vector(31 downto 0);
    signal iotab_fill_slave_mem_rden        : std_logic;
    signal iotab_fill_slave_mem_addr_read   : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal iotab_fill_slave_data_in         : std_logic_vector(31 downto 0);

    signal datastream_slave_mem_wren        : std_logic := '0';
    signal datastream_slave_mem_addr_write  : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0) := (others => '0');
    signal datastream_slave_data_out        : std_logic_vector(31 downto 0);
    signal datastream_slave_mem_rden        : std_logic := '0';
    signal datastream_slave_mem_addr_read   : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0) := (others => '0');
    signal datastream_slave_data_in         : std_logic_vector(31 downto 0);

    -- Shared memory
    type mem_type is array ((2**C_MEM_ADDR_WIDTH)-1 downto 0) of std_logic_vector(31 downto 0);
    shared variable mem : mem_type;

    -- FAUPU AXI Lite signals
    signal dma_config_awaddr            : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
    signal dma_config_awprot            : std_logic_vector(2 downto 0);
    signal dma_config_awvalid           : std_logic;
    signal dma_config_awready           : std_logic;
    signal dma_config_wdata             : std_logic_vector(31 downto 0);
    signal dma_config_wstrb             : std_logic_vector((32/8)-1 downto 0);
    signal dma_config_wvalid            : std_logic;
    signal dma_config_wready            : std_logic;
    signal dma_config_bresp             : std_logic_vector(1 downto 0);
    signal dma_config_bvalid            : std_logic;
    signal dma_config_bready            : std_logic;
    signal dma_config_araddr            : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
    signal dma_config_arprot            : std_logic_vector(2 downto 0);
    signal dma_config_arvalid           : std_logic;
    signal dma_config_arready           : std_logic;
    signal dma_config_rdata             : std_logic_vector(31 downto 0);
    signal dma_config_rresp             : std_logic_vector(1 downto 0);
    signal dma_config_rvalid            : std_logic;
    signal dma_config_rready            : std_logic;

    -- FAUPU IO Table Fill signals (AXI Master Full)
    signal iotable_fill_init_axi_txn    : std_logic;
    signal iotable_fill_txn_done    : std_logic;
    signal iotable_fill_error       : std_logic;
    signal iotable_fill_awid        : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal iotable_fill_awaddr      : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal iotable_fill_awlen       : std_logic_vector(7 downto 0);
    signal iotable_fill_awsize      : std_logic_vector(2 downto 0);
    signal iotable_fill_awburst     : std_logic_vector(1 downto 0);
    signal iotable_fill_awlock      : std_logic;
    signal iotable_fill_awcache     : std_logic_vector(3 downto 0);
    signal iotable_fill_awprot      : std_logic_vector(2 downto 0);
    signal iotable_fill_awqos       : std_logic_vector(3 downto 0);
    signal iotable_fill_awuser      : std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
    signal iotable_fill_awvalid     : std_logic;
    signal iotable_fill_awready     : std_logic;
    signal iotable_fill_wdata       : std_logic_vector(31 downto 0);
    signal iotable_fill_wstrb       : std_logic_vector((32/8)-1 downto 0);
    signal iotable_fill_wlast       : std_logic;
    signal iotable_fill_wuser       : std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
    signal iotable_fill_wvalid      : std_logic;
    signal iotable_fill_wready      : std_logic;
    signal iotable_fill_bid         : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal iotable_fill_bresp       : std_logic_vector(1 downto 0);
    signal iotable_fill_buser       : std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
    signal iotable_fill_bvalid      : std_logic;
    signal iotable_fill_bready      : std_logic;
    signal iotable_fill_arid        : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal iotable_fill_araddr      : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal iotable_fill_arlen       : std_logic_vector(7 downto 0);
    signal iotable_fill_arsize      : std_logic_vector(2 downto 0);
    signal iotable_fill_arburst     : std_logic_vector(1 downto 0);
    signal iotable_fill_arlock      : std_logic;
    signal iotable_fill_arcache     : std_logic_vector(3 downto 0);
    signal iotable_fill_arprot      : std_logic_vector(2 downto 0);
    signal iotable_fill_arqos       : std_logic_vector(3 downto 0);
    signal iotable_fill_aruser      : std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
    signal iotable_fill_arvalid     : std_logic;
    signal iotable_fill_arready     : std_logic;
    signal iotable_fill_rid         : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal iotable_fill_rdata       : std_logic_vector(31 downto 0);
    signal iotable_fill_rresp       : std_logic_vector(1 downto 0);
    signal iotable_fill_rlast       : std_logic;
    signal iotable_fill_ruser       : std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
    signal iotable_fill_rvalid      : std_logic;
    signal iotable_fill_rready      : std_logic;

    signal datastream_init_axi_txn  : std_logic;
    signal datastream_txn_done      : std_logic;
    signal datastream_error         : std_logic;
    signal datastream_awid          : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal datastream_awaddr        : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal datastream_awlen         : std_logic_vector(7 downto 0);
    signal datastream_awsize        : std_logic_vector(2 downto 0);
    signal datastream_awburst       : std_logic_vector(1 downto 0);
    signal datastream_awlock        : std_logic;
    signal datastream_awcache       : std_logic_vector(3 downto 0);
    signal datastream_awprot        : std_logic_vector(2 downto 0);
    signal datastream_awqos         : std_logic_vector(3 downto 0);
    signal datastream_awuser        : std_logic_vector(C_AXI_MASTER_AWUSER_WIDTH-1 downto 0);
    signal datastream_awvalid       : std_logic;
    signal datastream_awready       : std_logic;
    signal datastream_wdata         : std_logic_vector(31 downto 0);
    signal datastream_wstrb         : std_logic_vector((32/8)-1 downto 0);
    signal datastream_wlast         : std_logic;
    signal datastream_wuser         : std_logic_vector(C_AXI_MASTER_WUSER_WIDTH-1 downto 0);
    signal datastream_wvalid        : std_logic;
    signal datastream_wready        : std_logic;
    signal datastream_bid           : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal datastream_bresp         : std_logic_vector(1 downto 0);
    signal datastream_buser         : std_logic_vector(C_AXI_MASTER_BUSER_WIDTH-1 downto 0);
    signal datastream_bvalid        : std_logic;
    signal datastream_bready        : std_logic;
    signal datastream_arid          : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal datastream_araddr        : std_logic_vector(C_AXI_MASTER_ADDR_WIDTH-1 downto 0);
    signal datastream_arlen         : std_logic_vector(7 downto 0);
    signal datastream_arsize        : std_logic_vector(2 downto 0);
    signal datastream_arburst       : std_logic_vector(1 downto 0);
    signal datastream_arlock        : std_logic;
    signal datastream_arcache       : std_logic_vector(3 downto 0);
    signal datastream_arprot        : std_logic_vector(2 downto 0);
    signal datastream_arqos         : std_logic_vector(3 downto 0);
    signal datastream_aruser        : std_logic_vector(C_AXI_MASTER_ARUSER_WIDTH-1 downto 0);
    signal datastream_arvalid       : std_logic;
    signal datastream_arready       : std_logic;
    signal datastream_rid           : std_logic_vector(C_AXI_MASTER_ID_WIDTH-1 downto 0);
    signal datastream_rdata         : std_logic_vector(31 downto 0);
    signal datastream_rresp         : std_logic_vector(1 downto 0);
    signal datastream_rlast         : std_logic;
    signal datastream_ruser         : std_logic_vector(C_AXI_MASTER_RUSER_WIDTH-1 downto 0);
    signal datastream_rvalid        : std_logic;
    signal datastream_rready        : std_logic;

    signal pea_config_awaddr        : std_logic_vector(31 downto 0);
    signal pea_config_awprot        : std_logic_vector(2 downto 0);
    signal pea_config_awvalid       : std_logic;
    signal pea_config_awready       : std_logic;
    signal pea_config_wdata         : std_logic_vector(31 downto 0);
    signal pea_config_wstrb         : std_logic_vector(32/8-1 downto 0);
    signal pea_config_wvalid        : std_logic;
    signal pea_config_wready        : std_logic;
    signal pea_config_bresp         : std_logic_vector(1 downto 0);
    signal pea_config_bvalid        : std_logic;
    signal pea_config_bready        : std_logic;
    signal pea_config_araddr        : std_logic_vector(31 downto 0);
    signal pea_config_arprot        : std_logic_vector(2 downto 0);
    signal pea_config_arvalid       : std_logic;
    signal pea_config_arready       : std_logic;
    signal pea_config_rdata         : std_logic_vector(31 downto 0);
    signal pea_config_rresp         : std_logic_vector(1 downto 0);
    signal pea_config_rvalid        : std_logic;
    signal pea_config_rready        : std_logic;

    signal to_pea_tvalid            : std_logic;
    signal to_pea_tdata             : std_logic_vector(31 downto 0);
    signal to_pea_tstrb             : std_logic_vector((32/8)-1 downto 0);
    signal to_pea_tlast             : std_logic;
    signal to_pea_tready            : std_logic;
    signal to_pea_tdest             : std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

    signal from_pea_tready          : std_logic;
    signal from_pea_tdata           : std_logic_vector(31 downto 0);
    signal from_pea_tstrb           : std_logic_vector((32/8)-1 downto 0);
    signal from_pea_tlast           : std_logic;
    signal from_pea_tvalid          : std_logic;
    signal from_pea_tdest           : std_logic_vector(C_PE_ID_WIDTH-1 downto 0);

    signal s_axi_intr_awaddr        : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
    signal s_axi_intr_awprot        : std_logic_vector(2 downto 0);
    signal s_axi_intr_awvalid       : std_logic;
    signal s_axi_intr_awready       : std_logic;
    signal s_axi_intr_wdata         : std_logic_vector(31 downto 0);
    signal s_axi_intr_wstrb         : std_logic_vector((32/8)-1 downto 0);
    signal s_axi_intr_wvalid        : std_logic;
    signal s_axi_intr_wready        : std_logic;
    signal s_axi_intr_bresp         : std_logic_vector(1 downto 0);
    signal s_axi_intr_bvalid        : std_logic;
    signal s_axi_intr_bready        : std_logic;
    signal s_axi_intr_araddr        : std_logic_vector(C_AXI_LITE_ADDR_WIDTH-1 downto 0);
    signal s_axi_intr_arprot        : std_logic_vector(2 downto 0);
    signal s_axi_intr_arvalid       : std_logic;
    signal s_axi_intr_arready       : std_logic;
    signal s_axi_intr_rdata         : std_logic_vector(31 downto 0);
    signal s_axi_intr_rresp         : std_logic_vector(1 downto 0);
    signal s_axi_intr_rvalid        : std_logic;
    signal s_axi_intr_rready        : std_logic;
    signal irq                      : std_logic;

begin

    SIM_PROC: process
    begin
        wait until axi_aresetn = '1';
        wait for CLK_PERIOD;

        irq_write_register          <= std_logic_vector(to_unsigned(0, C_AXI_LITE_ADDR_WIDTH-2));
        irq_write_data              <= std_logic_vector(to_unsigned(1, 32));
        irq_init_axi_txn            <= '1';
        wait for CLK_PERIOD;
        irq_init_axi_txn            <= '0';

        wait until irq_txn_done = '1';
        wait for CLK_PERIOD;

        irq_write_register          <= std_logic_vector(to_unsigned(1, C_AXI_LITE_ADDR_WIDTH-2));
        irq_write_data              <= std_logic_vector(to_unsigned(1, 32));
        irq_init_axi_txn            <= '1';
        wait for CLK_PERIOD;
        irq_init_axi_txn            <= '0';

        wait until irq_txn_done = '1';
        wait for CLK_PERIOD;

        dma_config_write_register <= std_logic_vector(to_unsigned(1, C_AXI_LITE_ADDR_WIDTH-2));
        dma_config_write_data     <= std_logic_vector(to_unsigned(0, 32));
        dma_config_init_axi_txn   <= '1';
        wait for CLK_PERIOD;
        dma_config_init_axi_txn   <= '0';

        wait until dma_config_txn_done = '1';
        wait for CLK_PERIOD;

        dma_config_write_register <= std_logic_vector(to_unsigned(2, C_AXI_LITE_ADDR_WIDTH-2));
        dma_config_write_data     <= std_logic_vector(to_unsigned(2, 32));
        dma_config_init_axi_txn   <= '1';
        wait for CLK_PERIOD;
        dma_config_init_axi_txn   <= '0';

        wait until dma_config_txn_done = '1';
        wait for CLK_PERIOD;

        dma_config_write_register <= std_logic_vector(to_unsigned(0, C_AXI_LITE_ADDR_WIDTH-2));
        dma_config_write_data     <= std_logic_vector(to_unsigned(1, 32));
        dma_config_init_axi_txn   <= '1';
        wait for CLK_PERIOD;
        dma_config_init_axi_txn   <= '0';

        wait until irq = '1';
        wait for CLK_PERIOD * 150;

        irq_write_register          <= std_logic_vector(to_unsigned(3, C_AXI_LITE_ADDR_WIDTH-2));
        irq_write_data              <= std_logic_vector(to_unsigned(1, 32));
        irq_init_axi_txn            <= '1';
        wait for CLK_PERIOD;
        irq_init_axi_txn            <= '0';

        wait until irq_txn_done = '1';
        --dma_config_write_register <= std_logic_vector(to_unsigned(0, C_AXI_LITE_ADDR_WIDTH-2));
        --dma_config_write_data     <= "00000000000000000000000010000010";
        --dma_config_init_axi_txn   <= '1';
        --wait for CLK_PERIOD;
        --dma_config_init_axi_txn   <= '0';

        --wait for CLK_PERIOD * 20;

        --dma_config_write_register <= std_logic_vector(to_unsigned(3, C_AXI_LITE_ADDR_WIDTH-2));
        --dma_config_write_data     <= std_logic_vector(to_unsigned(3, 32));
        --dma_config_init_axi_txn   <= '1';
        --wait for CLK_PERIOD;
        --dma_config_init_axi_txn   <= '0';

        --wait for CLK_PERIOD * 20;

        --dma_config_write_register <= std_logic_vector(to_unsigned(3, C_AXI_LITE_ADDR_WIDTH-2));
        --dma_config_write_data     <= std_logic_vector(to_unsigned(5, 32));
        --dma_config_init_axi_txn   <= '1';
        --wait for CLK_PERIOD;
        --dma_config_init_axi_txn   <= '0';

        --wait for CLK_PERIOD * 20;

        --dma_config_write_register <= std_logic_vector(to_unsigned(3, C_AXI_LITE_ADDR_WIDTH-2));
        --dma_config_write_data     <= std_logic_vector(to_unsigned(6, 32));
        --dma_config_init_axi_txn   <= '1';
        --wait for CLK_PERIOD;
        --dma_config_init_axi_txn   <= '0';

        -- don't forget the wait statement here!
        wait;
    end process;

    INIT_PROC: process(axi_aclk)
        variable i : integer;
        variable j : integer;
    begin
        if rising_edge(axi_aclk) then
            if (axi_aresetn = '0') then
                for i in 0 to 4096 loop
                    mem(i) := (others => '0');
                end loop;

                mem(0)(15 downto  0)    := std_logic_vector(to_unsigned(1, 16));
                mem(0)(31 downto 16)    := std_logic_vector(to_unsigned(0, 16));
                mem(1)                  := std_logic_vector(to_unsigned(1024*4, 32));
                mem(2)                  := std_logic_vector(to_unsigned(20, 32));
                mem(3)(15 downto  0)    := std_logic_vector(to_unsigned(20, 16));
                mem(3)(31 downto 16)    := std_logic_vector(to_unsigned(4, 16));
                mem(4)                  := std_logic_vector(to_unsigned(2048*4, 32));
                mem(5)                  := std_logic_vector(to_unsigned(20, 32));
                mem(6)(15 downto  0)    := std_logic_vector(to_unsigned(20, 16));
                mem(6)(31 downto 16)    := std_logic_vector(to_unsigned(4, 16));

                mem(8) := (others => '0');
                --mem(8)(15 downto  0)    := std_logic_vector(to_unsigned(7, 16));
                --mem(8)(31 downto 16)    := std_logic_vector(to_unsigned(1, 16));
                --mem(9)                  := std_logic_vector(to_unsigned(384*4, 32));
                --mem(10)                 := std_logic_vector(to_unsigned(16, 32));
                --mem(11)                 := std_logic_vector(to_unsigned(16, 32));
                --mem(11)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(11)(31 downto 16)   := std_logic_vector(to_unsigned(8, 16));
                --mem(12)                 := std_logic_vector(to_unsigned(512*4 + 16*4, 32));
                --mem(13)                 := std_logic_vector(to_unsigned(16*2, 32));
                --mem(14)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(14)(31 downto 16)   := std_logic_vector(to_unsigned(8, 16));
                --mem(8)(15 downto  0)    := std_logic_vector(to_unsigned(1, 16));
                --mem(8)(31 downto 16)    := std_logic_vector(to_unsigned(0, 16));
                --mem(9)                  := std_logic_vector(to_unsigned(256*4 + 16*4, 32));
                --mem(10)                 := std_logic_vector(to_unsigned(32, 32));
                --mem(11)(15 downto  0)   := std_logic_vector(to_unsigned(14, 16));
                --mem(11)(31 downto 16)   := std_logic_vector(to_unsigned(5, 16));
                --mem(12)                 := std_logic_vector(to_unsigned(512*4 + 16*4, 32));
                --mem(13)                 := std_logic_vector(to_unsigned(32, 32));
                --mem(14)(15 downto  0)   := std_logic_vector(to_unsigned(14, 16));
                --mem(14)(31 downto 16)   := std_logic_vector(to_unsigned(5, 16));

                --mem(16) := (others => '0');
                --mem(16)(15 downto  0)   := std_logic_vector(to_unsigned(1, 16));
                --mem(16)(31 downto 16)   := std_logic_vector(to_unsigned(1, 16));
                --mem(17)                 := std_logic_vector(to_unsigned(256*4 + 4*32*4, 32));
                --mem(18)                 := std_logic_vector(to_unsigned(16*2, 32));
                --mem(19)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(19)(31 downto 16)   := std_logic_vector(to_unsigned(4, 16));
                --mem(20)                 := std_logic_vector(to_unsigned(512*4 + 4*32*4, 32));
                --mem(21)                 := std_logic_vector(to_unsigned(16*2, 32));
                --mem(22)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(22)(31 downto 16)   := std_logic_vector(to_unsigned(4, 16));

                --mem(24)(15 downto  0)   := std_logic_vector(to_unsigned(1, 16));
                --mem(24)(31 downto 16)   := std_logic_vector(to_unsigned(0, 16));
                --mem(25)                 := std_logic_vector(to_unsigned(256*4 + 16*4 + 4*32*4, 32));
                --mem(26)                 := std_logic_vector(to_unsigned(16*2, 32));
                --mem(27)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(27)(31 downto 16)   := std_logic_vector(to_unsigned(4, 16));
                --mem(28)                 := std_logic_vector(to_unsigned(512*4 + 16*4 + 4*32*4, 32));
                --mem(29)                 := std_logic_vector(to_unsigned(16*2, 32));
                --mem(30)(15 downto  0)   := std_logic_vector(to_unsigned(16, 16));
                --mem(30)(31 downto 16)   := std_logic_vector(to_unsigned(4, 16));

                --mem(32)                 := (others => '0');
                --mem(8)(15 downto  0)  := std_logic_vector(to_unsigned(1, 16));
                --mem(8)(31 downto 16)  := std_logic_vector(to_unsigned(1, 16));
                --mem(9)  := std_logic_vector(to_unsigned(264*4, 32));
                --mem(10)  := std_logic_vector(to_unsigned(20, 32));
                --mem(11)(15 downto  0)  := std_logic_vector(to_unsigned(8, 16));
                --mem(11)(31 downto 16)  := std_logic_vector(to_unsigned(8, 16));
                --mem(12)  := std_logic_vector(to_unsigned(520*4, 32));
                --mem(13)  := std_logic_vector(to_unsigned(20, 32));
                --mem(14)(15 downto  0)  := std_logic_vector(to_unsigned(8, 16));
                --mem(14)(31 downto 16)  := std_logic_vector(to_unsigned(8, 16));

                --mem(16)(15 downto  0)  := std_logic_vector(to_unsigned(1, 16));
                --mem(16)(31 downto 16)  := std_logic_vector(to_unsigned(0, 16));
                --mem(17)  := std_logic_vector(to_unsigned(272*4, 32));
                --mem(18)  := std_logic_vector(to_unsigned(20, 32));
                --mem(19)(15 downto  0)  := std_logic_vector(to_unsigned(4, 16));
                --mem(19)(31 downto 16)  := std_logic_vector(to_unsigned(4, 16));
                --mem(20)  := std_logic_vector(to_unsigned(528*4, 32));
                --mem(21)  := std_logic_vector(to_unsigned(20, 32));
                --mem(22)(15 downto  0)  := std_logic_vector(to_unsigned(4, 16));
                --mem(22)(31 downto 16)  := std_logic_vector(to_unsigned(4, 16));

                j := 1;
                for i in 1024 to (1024 + (256 * 4) - 1) loop
                    mem(i)  := std_logic_vector(to_unsigned(j, 32));
                    j       := j + 1;
                end loop;
            end if;
        end if;
    end process;

    CLK_PROC: process
    begin
        axi_aclk <= not axi_aclk;
        wait for CLK_PERIOD/2;
    end process;

    RESET_PROC: process
    begin
        axi_aresetn <= '0';
        wait for CLK_PERIOD*3;
        axi_aresetn <= '1';
        wait;
    end process;

    -- Port A
    a_wr    <= iotab_fill_slave_mem_wren;
    a_addr  <= iotab_fill_slave_mem_addr_write(C_MEM_ADDR_WIDTH+1 downto 2);
    a_din   <= iotab_fill_slave_data_in;
    RAM_PORT_A_PROC: process (axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (a_wr = '1') then
                mem(conv_integer(a_addr)) := a_din;
            end if;
            a_dout <= mem(conv_integer(a_addr));
        end if;
    end process;

    -- Port B
    b_wr                        <= '0';
    b_addr                      <= iotab_fill_slave_mem_addr_read(C_MEM_ADDR_WIDTH+1 downto 2);
    iotab_fill_slave_data_out   <= b_dout;
    RAM_PORT_B_PROC: process (axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (b_wr = '1') then
                mem(conv_integer(b_addr)) := b_din;
            end if;
            b_dout <= mem(conv_integer(b_addr));
        end if;
    end process;

    -- Port C
    c_wr    <= datastream_slave_mem_wren;
    c_addr  <= datastream_slave_mem_addr_write(C_MEM_ADDR_WIDTH+1 downto 2);
    c_din   <= datastream_slave_data_in;
    RAM_PORT_C_PROC: process (axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (c_wr = '1') then
                mem(conv_integer(c_addr)) := c_din;
            end if;
            c_dout <= mem(conv_integer(c_addr));
        end if;
    end process;

    -- Port D
    d_wr                        <= '0';
    d_addr                      <= datastream_slave_mem_addr_read(C_MEM_ADDR_WIDTH+1 downto 2);
    datastream_slave_data_out   <= d_dout;
    RAM_PORT_D_PROC: process (axi_aclk)
    begin
        if (rising_edge(axi_aclk)) then
            if (d_wr = '1') then
                mem(conv_integer(d_addr)) := d_din;
            end if;
            d_dout <= mem(conv_integer(d_addr));
            --d_dout(C_MEM_ADDR_WIDTH-1 downto 0) <= d_addr;
            --d_dout(31 downto C_MEM_ADDR_WIDTH) <= (others => '0');
        end if;
    end process;

-- component instantiation
    faupu_dma_inst: faupu_dma
        generic map (
            C_NUM_IOTABLE_ROWS          => C_NUM_IOTABLE_ROWS,
            DATA_BUFFER_NUM_BUFFERS     => DATA_BUFFER_NUM_BUFFERS,
            C_PE_ID_WIDTH               => C_PE_ID_WIDTH,
            C_NUM_PE_ARRAYS             => C_NUM_PE_ARRAYS,
            C_AXI_LITE_ADDR_WIDTH       => C_AXI_LITE_ADDR_WIDTH,
            C_AXI_MASTER_BURST_LEN      => C_AXI_MASTER_BURST_LEN,
            C_AXI_MASTER_ID_WIDTH       => C_AXI_MASTER_ID_WIDTH,
            C_AXI_MASTER_ADDR_WIDTH     => C_AXI_MASTER_ADDR_WIDTH,
            C_AXI_MASTER_AWUSER_WIDTH   => C_AXI_MASTER_AWUSER_WIDTH,
            C_AXI_MASTER_ARUSER_WIDTH   => C_AXI_MASTER_ARUSER_WIDTH,
            C_AXI_MASTER_WUSER_WIDTH    => C_AXI_MASTER_WUSER_WIDTH,
            C_AXI_MASTER_RUSER_WIDTH    => C_AXI_MASTER_RUSER_WIDTH,
            C_AXI_MASTER_BUSER_WIDTH    => C_AXI_MASTER_BUSER_WIDTH,
            C_S_AXI_INTR_DATA_WIDTH	    => 32,
            C_S_AXI_INTR_ADDR_WIDTH	    => C_AXI_LITE_ADDR_WIDTH
        )
        port map (
            axi_aclk         => axi_aclk,
            axi_aresetn      => axi_aresetn,
            dma_config_awaddr       => dma_config_awaddr,
            dma_config_awprot       => dma_config_awprot,
            dma_config_awvalid      => dma_config_awvalid,
            dma_config_awready      => dma_config_awready,
            dma_config_wdata        => dma_config_wdata,
            dma_config_wstrb        => dma_config_wstrb,
            dma_config_wvalid       => dma_config_wvalid,
            dma_config_wready       => dma_config_wready,
            dma_config_bresp        => dma_config_bresp,
            dma_config_bvalid       => dma_config_bvalid,
            dma_config_bready       => dma_config_bready,
            dma_config_araddr       => dma_config_araddr,
            dma_config_arprot       => dma_config_arprot,
            dma_config_arvalid      => dma_config_arvalid,
            dma_config_arready      => dma_config_arready,
            dma_config_rdata        => dma_config_rdata,
            dma_config_rresp        => dma_config_rresp,
            dma_config_rvalid       => dma_config_rvalid,
            dma_config_rready       => dma_config_rready,
            iotable_fill_init_axi_txn => iotable_fill_init_axi_txn,
            iotable_fill_txn_done   => iotable_fill_txn_done,
            iotable_fill_error      => iotable_fill_error,
            iotable_fill_awid       => iotable_fill_awid,
            iotable_fill_awaddr     => iotable_fill_awaddr,
            iotable_fill_awlen      => iotable_fill_awlen,
            iotable_fill_awsize     => iotable_fill_awsize,
            iotable_fill_awburst    => iotable_fill_awburst,
            iotable_fill_awlock     => iotable_fill_awlock,
            iotable_fill_awcache    => iotable_fill_awcache,
            iotable_fill_awprot     => iotable_fill_awprot,
            iotable_fill_awqos      => iotable_fill_awqos,
            iotable_fill_awuser     => iotable_fill_awuser,
            iotable_fill_awvalid    => iotable_fill_awvalid,
            iotable_fill_awready    => iotable_fill_awready,
            iotable_fill_wdata      => iotable_fill_wdata,
            iotable_fill_wstrb      => iotable_fill_wstrb,
            iotable_fill_wlast      => iotable_fill_wlast,
            iotable_fill_wuser      => iotable_fill_wuser,
            iotable_fill_wvalid     => iotable_fill_wvalid,
            iotable_fill_wready     => iotable_fill_wready,
            iotable_fill_bid        => iotable_fill_bid,
            iotable_fill_bresp      => iotable_fill_bresp,
            iotable_fill_buser      => iotable_fill_buser,
            iotable_fill_bvalid     => iotable_fill_bvalid,
            iotable_fill_bready     => iotable_fill_bready,
            iotable_fill_arid       => iotable_fill_arid,
            iotable_fill_araddr     => iotable_fill_araddr,
            iotable_fill_arlen      => iotable_fill_arlen,
            iotable_fill_arsize     => iotable_fill_arsize,
            iotable_fill_arburst    => iotable_fill_arburst,
            iotable_fill_arlock     => iotable_fill_arlock,
            iotable_fill_arcache    => iotable_fill_arcache,
            iotable_fill_arprot     => iotable_fill_arprot,
            iotable_fill_arqos      => iotable_fill_arqos,
            iotable_fill_aruser     => iotable_fill_aruser,
            iotable_fill_arvalid    => iotable_fill_arvalid,
            iotable_fill_arready    => iotable_fill_arready,
            iotable_fill_rid        => iotable_fill_rid,
            iotable_fill_rdata      => iotable_fill_rdata,
            iotable_fill_rresp      => iotable_fill_rresp,
            iotable_fill_rlast      => iotable_fill_rlast,
            iotable_fill_ruser      => iotable_fill_ruser,
            iotable_fill_rvalid     => iotable_fill_rvalid,
            iotable_fill_rready     => iotable_fill_rready,
            datastream_init_axi_txn => datastream_init_axi_txn,
            datastream_txn_done     => datastream_txn_done,
            datastream_error        => datastream_error,
            datastream_awid         => datastream_awid,
            datastream_awaddr       => datastream_awaddr,
            datastream_awlen        => datastream_awlen,
            datastream_awsize       => datastream_awsize,
            datastream_awburst      => datastream_awburst,
            datastream_awlock       => datastream_awlock,
            datastream_awcache      => datastream_awcache,
            datastream_awprot       => datastream_awprot,
            datastream_awqos        => datastream_awqos,
            datastream_awuser       => datastream_awuser,
            datastream_awvalid      => datastream_awvalid,
            datastream_awready      => datastream_awready,
            datastream_wdata        => datastream_wdata,
            datastream_wstrb        => datastream_wstrb,
            datastream_wlast        => datastream_wlast,
            datastream_wuser        => datastream_wuser,
            datastream_wvalid       => datastream_wvalid,
            datastream_wready       => datastream_wready,
            datastream_bid          => datastream_bid,
            datastream_bresp        => datastream_bresp,
            datastream_buser        => datastream_buser,
            datastream_bvalid       => datastream_bvalid,
            datastream_bready       => datastream_bready,
            datastream_arid         => datastream_arid,
            datastream_araddr       => datastream_araddr,
            datastream_arlen        => datastream_arlen,
            datastream_arsize       => datastream_arsize,
            datastream_arburst      => datastream_arburst,
            datastream_arlock       => datastream_arlock,
            datastream_arcache      => datastream_arcache,
            datastream_arprot       => datastream_arprot,
            datastream_arqos        => datastream_arqos,
            datastream_aruser       => datastream_aruser,
            datastream_arvalid      => datastream_arvalid,
            datastream_arready      => datastream_arready,
            datastream_rid          => datastream_rid,
            datastream_rdata        => datastream_rdata,
            datastream_rresp        => datastream_rresp,
            datastream_rlast        => datastream_rlast,
            datastream_ruser        => datastream_ruser,
            datastream_rvalid       => datastream_rvalid,
            datastream_rready       => datastream_rready,
            pea_config_awaddr       => pea_config_awaddr,
            pea_config_awprot       => pea_config_awprot,
            pea_config_awvalid      => pea_config_awvalid,
            pea_config_awready      => pea_config_awready,
            pea_config_wdata        => pea_config_wdata,
            pea_config_wstrb        => pea_config_wstrb,
            pea_config_wvalid       => pea_config_wvalid,
            pea_config_wready       => pea_config_wready,
            pea_config_bresp        => pea_config_bresp,
            pea_config_bvalid       => pea_config_bvalid,
            pea_config_bready       => pea_config_bready,
            pea_config_araddr       => pea_config_araddr,
            pea_config_arprot       => pea_config_arprot,
            pea_config_arvalid      => pea_config_arvalid,
            pea_config_arready      => pea_config_arready,
            pea_config_rdata        => pea_config_rdata,
            pea_config_rresp        => pea_config_rresp,
            pea_config_rvalid       => pea_config_rvalid,
            pea_config_rready       => pea_config_rready,
            to_pea_tvalid           => to_pea_tvalid,
            to_pea_tdata            => to_pea_tdata,
            to_pea_tstrb            => to_pea_tstrb,
            to_pea_tlast            => to_pea_tlast,
            to_pea_tready           => to_pea_tready,
            to_pea_tdest            => to_pea_tdest,
            from_pea_tready         => to_pea_tready,   -- pipe signal
            from_pea_tdata          => to_pea_tdata,    -- pipe signal
            from_pea_tstrb          => to_pea_tstrb,    -- pipe signal
            from_pea_tlast          => to_pea_tlast,    -- pipe signal
            from_pea_tvalid         => to_pea_tvalid,   -- pipe signal
            from_pea_tdest          => to_pea_tdest,    -- pipe signal
            s_axi_intr_awaddr       => s_axi_intr_awaddr,
            s_axi_intr_awprot       => s_axi_intr_awprot,
            s_axi_intr_awvalid      => s_axi_intr_awvalid,
            s_axi_intr_awready      => s_axi_intr_awready,
            s_axi_intr_wdata        => s_axi_intr_wdata,
            s_axi_intr_wstrb        => s_axi_intr_wstrb,
            s_axi_intr_wvalid       => s_axi_intr_wvalid,
            s_axi_intr_wready       => s_axi_intr_wready,
            s_axi_intr_bresp        => s_axi_intr_bresp,
            s_axi_intr_bvalid       => s_axi_intr_bvalid,
            s_axi_intr_bready       => s_axi_intr_bready,
            s_axi_intr_araddr       => s_axi_intr_araddr,
            s_axi_intr_arprot       => s_axi_intr_arprot,
            s_axi_intr_arvalid      => s_axi_intr_arvalid,
            s_axi_intr_arready      => s_axi_intr_arready,
            s_axi_intr_rdata        => s_axi_intr_rdata,
            s_axi_intr_rresp        => s_axi_intr_rresp,
            s_axi_intr_rvalid       => s_axi_intr_rvalid,
            s_axi_intr_rready       => s_axi_intr_rready,
            irq                     => irq
        );

    CONFIG_REGISTER_MASTER_LITE_INST: tb_faupu_AXI_LITE_MASTER
        generic map (
            C_M_AXI_ADDR_WIDTH  => C_AXI_LITE_ADDR_WIDTH,
            C_M_AXI_DATA_WIDTH  => C_DATA_WIDTH
        )
        port map (
            write_register  => dma_config_write_register,
            write_data      => dma_config_write_data,
            INIT_AXI_TXN    => dma_config_init_axi_txn,
            ERROR           => dma_config_error,
            TXN_DONE        => dma_config_txn_done,
            M_AXI_ACLK      => axi_aclk,
            M_AXI_ARESETN   => axi_aresetn,
            M_AXI_AWADDR    => dma_config_awaddr,
            M_AXI_AWPROT    => dma_config_awprot,
            M_AXI_AWVALID   => dma_config_awvalid,
            M_AXI_AWREADY   => dma_config_awready,
            M_AXI_WDATA     => dma_config_wdata,
            M_AXI_WSTRB     => dma_config_wstrb,
            M_AXI_WVALID    => dma_config_wvalid,
            M_AXI_WREADY    => dma_config_wready,
            M_AXI_BRESP     => dma_config_bresp,
            M_AXI_BVALID    => dma_config_bvalid,
            M_AXI_BREADY    => dma_config_bready,
            M_AXI_ARADDR    => dma_config_araddr,
            M_AXI_ARPROT    => dma_config_arprot,
            M_AXI_ARVALID   => dma_config_arvalid,
            M_AXI_ARREADY   => dma_config_arready,
            M_AXI_RDATA     => dma_config_rdata,
            M_AXI_RRESP     => dma_config_rresp,
            M_AXI_RVALID    => dma_config_rvalid,
            M_AXI_RREADY    => dma_config_rready
        );

    IRQ_MASTER_LITE_INST: tb_faupu_AXI_LITE_MASTER
        generic map (
            C_M_AXI_ADDR_WIDTH  => C_AXI_LITE_ADDR_WIDTH,
            C_M_AXI_DATA_WIDTH  => C_DATA_WIDTH
        )
        port map (
            write_register  => irq_write_register,
            write_data      => irq_write_data,
            INIT_AXI_TXN    => irq_init_axi_txn,
            ERROR           => irq_error,
            TXN_DONE        => irq_txn_done,
            M_AXI_ACLK      => axi_aclk,
            M_AXI_ARESETN   => axi_aresetn,
            M_AXI_AWADDR    => s_axi_intr_awaddr,
            M_AXI_AWPROT    => s_axi_intr_awprot,
            M_AXI_AWVALID   => s_axi_intr_awvalid,
            M_AXI_AWREADY   => s_axi_intr_awready,
            M_AXI_WDATA     => s_axi_intr_wdata,
            M_AXI_WSTRB     => s_axi_intr_wstrb,
            M_AXI_WVALID    => s_axi_intr_wvalid,
            M_AXI_WREADY    => s_axi_intr_wready,
            M_AXI_BRESP     => s_axi_intr_bresp,
            M_AXI_BVALID    => s_axi_intr_bvalid,
            M_AXI_BREADY    => s_axi_intr_bready,
            M_AXI_ARADDR    => s_axi_intr_araddr,
            M_AXI_ARPROT    => s_axi_intr_arprot,
            M_AXI_ARVALID   => s_axi_intr_arvalid,
            M_AXI_ARREADY   => s_axi_intr_arready,
            M_AXI_RDATA     => s_axi_intr_rdata,
            M_AXI_RRESP     => s_axi_intr_rresp,
            M_AXI_RVALID    => s_axi_intr_rvalid,
            M_AXI_RREADY    => s_axi_intr_rready
        );

    IOTABLE_FILL_SLAVE_INST: tb_faupu_AXI_FULL_SLAVE
        generic map (
            C_S_AXI_ID_WIDTH        => C_AXI_MASTER_ID_WIDTH,
            C_S_AXI_DATA_WIDTH      => C_DATA_WIDTH,
            C_S_AXI_ADDR_WIDTH      => C_AXI_MASTER_ADDR_WIDTH,
            C_S_AXI_AWUSER_WIDTH    => C_AXI_MASTER_AWUSER_WIDTH,
            C_S_AXI_ARUSER_WIDTH    => C_AXI_MASTER_ARUSER_WIDTH,
            C_S_AXI_WUSER_WIDTH     => C_AXI_MASTER_WUSER_WIDTH,
            C_S_AXI_RUSER_WIDTH     => C_AXI_MASTER_RUSER_WIDTH,
            C_S_AXI_BUSER_WIDTH     => C_AXI_MASTER_BUSER_WIDTH
        )
        port map (
            mem_wren        => iotab_fill_slave_mem_wren,
            mem_addr_write  => iotab_fill_slave_mem_addr_write,
            data_out        => iotab_fill_slave_data_out,
            mem_rden        => iotab_fill_slave_mem_rden,
            mem_addr_read   => iotab_fill_slave_mem_addr_read,
            data_in         => iotab_fill_slave_data_in,
            S_AXI_ACLK      => axi_aclk,
            S_AXI_ARESETN   => axi_aresetn,
            S_AXI_AWID      => iotable_fill_awid,
            S_AXI_AWADDR    => iotable_fill_awaddr,
            S_AXI_AWLEN     => iotable_fill_awlen,
            S_AXI_AWSIZE    => iotable_fill_awsize,
            S_AXI_AWBURST   => iotable_fill_awburst,
            S_AXI_AWLOCK    => iotable_fill_awlock,
            S_AXI_AWCACHE   => iotable_fill_awcache,
            S_AXI_AWPROT    => iotable_fill_awprot,
            S_AXI_AWQOS     => iotable_fill_awqos,
            S_AXI_AWREGION  => x"0",
            S_AXI_AWUSER    => iotable_fill_awuser,
            S_AXI_AWVALID   => iotable_fill_awvalid,
            S_AXI_AWREADY   => iotable_fill_awready,
            S_AXI_WDATA     => iotable_fill_wdata,
            S_AXI_WSTRB     => iotable_fill_wstrb,
            S_AXI_WLAST     => iotable_fill_wlast,
            S_AXI_WUSER     => iotable_fill_wuser,
            S_AXI_WVALID    => iotable_fill_wvalid,
            S_AXI_WREADY    => iotable_fill_wready,
            S_AXI_BID       => iotable_fill_bid,
            S_AXI_BRESP     => iotable_fill_bresp,
            S_AXI_BUSER     => iotable_fill_buser,
            S_AXI_BVALID    => iotable_fill_bvalid,
            S_AXI_BREADY    => iotable_fill_bready,
            S_AXI_ARID      => iotable_fill_arid,
            S_AXI_ARADDR    => iotable_fill_araddr,
            S_AXI_ARLEN     => iotable_fill_arlen,
            S_AXI_ARSIZE    => iotable_fill_arsize,
            S_AXI_ARBURST   => iotable_fill_arburst,
            S_AXI_ARLOCK    => iotable_fill_arlock,
            S_AXI_ARCACHE   => iotable_fill_arcache,
            S_AXI_ARPROT    => iotable_fill_arprot,
            S_AXI_ARQOS     => iotable_fill_arqos,
            S_AXI_ARREGION  => x"0",
            S_AXI_ARUSER    => iotable_fill_aruser,
            S_AXI_ARVALID   => iotable_fill_arvalid,
            S_AXI_ARREADY   => iotable_fill_arready,
            S_AXI_RID       => iotable_fill_rid,
            S_AXI_RDATA     => iotable_fill_rdata,
            S_AXI_RRESP     => iotable_fill_rresp,
            S_AXI_RLAST     => iotable_fill_rlast,
            S_AXI_RUSER     => iotable_fill_ruser,
            S_AXI_RVALID    => iotable_fill_rvalid,
            S_AXI_RREADY    => iotable_fill_rready
        );

    DATASTREAM_SLAVE_INST: tb_faupu_AXI_FULL_SLAVE
        generic map (
            C_S_AXI_ID_WIDTH        => C_AXI_MASTER_ID_WIDTH,
            C_S_AXI_DATA_WIDTH      => C_DATA_WIDTH,
            C_S_AXI_ADDR_WIDTH      => C_AXI_MASTER_ADDR_WIDTH,
            C_S_AXI_AWUSER_WIDTH    => C_AXI_MASTER_AWUSER_WIDTH,
            C_S_AXI_ARUSER_WIDTH    => C_AXI_MASTER_ARUSER_WIDTH,
            C_S_AXI_WUSER_WIDTH     => C_AXI_MASTER_WUSER_WIDTH,
            C_S_AXI_RUSER_WIDTH     => C_AXI_MASTER_RUSER_WIDTH,
            C_S_AXI_BUSER_WIDTH     => C_AXI_MASTER_BUSER_WIDTH
        )
        port map (
            mem_wren        => datastream_slave_mem_wren,
            mem_addr_write  => datastream_slave_mem_addr_write,
            data_out        => datastream_slave_data_out,
            mem_rden        => datastream_slave_mem_rden,
            mem_addr_read   => datastream_slave_mem_addr_read,
            data_in         => datastream_slave_data_in,
            S_AXI_ACLK      => axi_aclk,
            S_AXI_ARESETN   => axi_aresetn,
            S_AXI_AWID      => datastream_awid,
            S_AXI_AWADDR    => datastream_awaddr,
            S_AXI_AWLEN     => datastream_awlen,
            S_AXI_AWSIZE    => datastream_awsize,
            S_AXI_AWBURST   => datastream_awburst,
            S_AXI_AWLOCK    => datastream_awlock,
            S_AXI_AWCACHE   => datastream_awcache,
            S_AXI_AWPROT    => datastream_awprot,
            S_AXI_AWQOS     => datastream_awqos,
            S_AXI_AWREGION  => x"0",
            S_AXI_AWUSER    => datastream_awuser,
            S_AXI_AWVALID   => datastream_awvalid,
            S_AXI_AWREADY   => datastream_awready,
            S_AXI_WDATA     => datastream_wdata,
            S_AXI_WSTRB     => datastream_wstrb,
            S_AXI_WLAST     => datastream_wlast,
            S_AXI_WUSER     => datastream_wuser,
            S_AXI_WVALID    => datastream_wvalid,
            S_AXI_WREADY    => datastream_wready,
            S_AXI_BID       => datastream_bid,
            S_AXI_BRESP     => datastream_bresp,
            S_AXI_BUSER     => datastream_buser,
            S_AXI_BVALID    => datastream_bvalid,
            S_AXI_BREADY    => datastream_bready,
            S_AXI_ARID      => datastream_arid,
            S_AXI_ARADDR    => datastream_araddr,
            S_AXI_ARLEN     => datastream_arlen,
            S_AXI_ARSIZE    => datastream_arsize,
            S_AXI_ARBURST   => datastream_arburst,
            S_AXI_ARLOCK    => datastream_arlock,
            S_AXI_ARCACHE   => datastream_arcache,
            S_AXI_ARPROT    => datastream_arprot,
            S_AXI_ARQOS     => datastream_arqos,
            S_AXI_ARREGION  => x"0",
            S_AXI_ARUSER    => datastream_aruser,
            S_AXI_ARVALID   => datastream_arvalid,
            S_AXI_ARREADY   => datastream_arready,
            S_AXI_RID       => datastream_rid,
            S_AXI_RDATA     => datastream_rdata,
            S_AXI_RRESP     => datastream_rresp,
            S_AXI_RLAST     => datastream_rlast,
            S_AXI_RUSER     => datastream_ruser,
            S_AXI_RVALID    => datastream_rvalid,
            S_AXI_RREADY    => datastream_rready
        );

    PEA_CONFIG_SLAVE: tb_faupu_AXI_LITE_SLAVE
        generic map (
            C_NUM_REGS          => 16,
            C_S_AXI_DATA_WIDTH  => C_DATA_WIDTH,
            C_S_AXI_ADDR_WIDTH  => 32
        )
        port map (
            S_AXI_ACLK          => axi_aclk,
            S_AXI_ARESETN       => axi_aresetn,
            S_AXI_AWADDR        => pea_config_awaddr,
            S_AXI_AWPROT        => pea_config_awprot,
            S_AXI_AWVALID       => pea_config_awvalid,
            S_AXI_AWREADY       => pea_config_awready,
            S_AXI_WDATA         => pea_config_wdata,
            S_AXI_WSTRB         => pea_config_wstrb,
            S_AXI_WVALID        => pea_config_wvalid,
            S_AXI_WREADY        => pea_config_wready,
            S_AXI_BRESP         => pea_config_bresp,
            S_AXI_BVALID        => pea_config_bvalid,
            S_AXI_BREADY        => pea_config_bready,
            S_AXI_ARADDR        => pea_config_araddr,
            S_AXI_ARPROT        => pea_config_arprot,
            S_AXI_ARVALID       => pea_config_arvalid,
            S_AXI_ARREADY       => pea_config_arready,
            S_AXI_RDATA         => pea_config_rdata,
            S_AXI_RRESP         => pea_config_rresp,
            S_AXI_RVALID        => pea_config_rvalid,
            S_AXI_RREADY        => pea_config_rready
        );

end bhv;
