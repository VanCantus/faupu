library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.faupu_dma_pkg.all;

entity tb_mastertest_v1_0_AXI_MASTER is
end tb_mastertest_v1_0_AXI_MASTER;

architecture bhv of tb_mastertest_v1_0_AXI_MASTER is

    component mastertest_v1_0_AXI_MASTER is
        generic (
            C_TABLE_ROWS            : integer   := 16;
            C_M_AXI_BURST_LEN       : integer   := 16;
            C_M_AXI_ID_WIDTH        : integer   := 1;
            C_M_AXI_ADDR_WIDTH      : integer   := 32;
            C_M_AXI_DATA_WIDTH      : integer   := 32;
            C_M_AXI_AWUSER_WIDTH    : integer   := 0;
            C_M_AXI_ARUSER_WIDTH    : integer   := 0;
            C_M_AXI_WUSER_WIDTH     : integer   := 0;
            C_M_AXI_RUSER_WIDTH     : integer   := 0;
            C_M_AXI_BUSER_WIDTH     : integer   := 0
        );
        port (
            table_filled        : out std_logic;
            table_txns_done       : in  std_logic;
            read_enable         : in  std_logic;
            read_ack            : out std_logic;
            read_start_addr     : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            read_len            : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            write_enable        : in  std_logic;
            write_ack           : out std_logic;
            write_start_addr    : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            write_len           : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            table_write_enable  : out std_logic;
            table_write_addr    : out std_logic_vector(31 downto 0);
            table_write_data    : out std_logic_vector(31 downto 0);
            TXNS_DONE           : out std_logic;
            M_AXI_ACLK          : in  std_logic;
            M_AXI_ARESETN       : in  std_logic;
            M_AXI_AWID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_AWADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_AWLEN         : out std_logic_vector(7 downto 0);
            M_AXI_AWSIZE        : out std_logic_vector(2 downto 0);
            M_AXI_AWBURST       : out std_logic_vector(1 downto 0);
            M_AXI_AWLOCK        : out std_logic;
            M_AXI_AWCACHE       : out std_logic_vector(3 downto 0);
            M_AXI_AWPROT        : out std_logic_vector(2 downto 0);
            M_AXI_AWQOS         : out std_logic_vector(3 downto 0);
            M_AXI_AWUSER        : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
            M_AXI_AWVALID       : out std_logic;
            M_AXI_AWREADY       : in  std_logic;
            M_AXI_WDATA         : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_WSTRB         : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
            M_AXI_WLAST         : out std_logic;
            M_AXI_WUSER         : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
            M_AXI_WVALID        : out std_logic;
            M_AXI_WREADY        : in  std_logic;
            M_AXI_BID           : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_BRESP         : in  std_logic_vector(1 downto 0);
            M_AXI_BUSER         : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
            M_AXI_BVALID        : in  std_logic;
            M_AXI_BREADY        : out std_logic;
            M_AXI_ARID          : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_ARADDR        : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
            M_AXI_ARLEN         : out std_logic_vector(7 downto 0);
            M_AXI_ARSIZE        : out std_logic_vector(2 downto 0);
            M_AXI_ARBURST       : out std_logic_vector(1 downto 0);
            M_AXI_ARLOCK        : out std_logic;
            M_AXI_ARCACHE       : out std_logic_vector(3 downto 0);
            M_AXI_ARPROT        : out std_logic_vector(2 downto 0);
            M_AXI_ARQOS         : out std_logic_vector(3 downto 0);
            M_AXI_ARUSER        : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
            M_AXI_ARVALID       : out std_logic;
            M_AXI_ARREADY       : in  std_logic;
            M_AXI_RID           : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
            M_AXI_RDATA         : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
            M_AXI_RRESP         : in  std_logic_vector(1 downto 0);
            M_AXI_RLAST         : in  std_logic;
            M_AXI_RUSER         : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
            M_AXI_RVALID        : in  std_logic;
            M_AXI_RREADY        : out std_logic
        );
    end component;

    component inout_table is
        generic(
            C_ROWS          : integer   := 16
        );
        port(
            -- the clock
            clk             : in  std_logic;

            -- reset (active low)
            resetn          : in  std_logic;

            -- if we = '1', then the table will be filled
            we              : in  std_logic;
            ADDR_IN         : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
            DATA_IN         : in  std_logic_vector(31 downto 0);

            -- if re = '1', data will be read from the table 
            re              : in  std_logic;
            ADDR_OUT        : in  std_logic_vector(log2(C_ROWS*8*4)-1 downto 0);
            DATA_OUT        : out std_logic_vector(31 downto 0);

            -- the row of the table entries, that should be read
            ROW             : in  std_logic_vector(28 downto 0);

            -- output of the table row that is refered to with ROW
            R_CONFIG                    : out std_logic_vector(31 downto 0);
            R_IMG_START_ADDR            : out std_logic_vector(31 downto 0);
            R_IMG_WIDTH_WRITE_WIDTH     : out std_logic_vector(31 downto 0);
            R_PARTITION_START_XY        : out std_logic_vector(31 downto 0);
            R_PARTITION_WIDTH_HEIGHT    : out std_logic_vector(31 downto 0);
            R_WRITE_START_ADDR          : out std_logic_vector(31 downto 0);
            R_WRITE_START_XY            : out std_logic_vector(31 downto 0);
            R_WRITE_DIMENSIONS          : out std_logic_vector(31 downto 0)
        );
    end component;

    constant CLK_PERIOD : time := 10 ns;

    constant BURST_LEN                      : integer   := 16;

    constant C_TABLE_ROWS                   : integer   := 4;
    -- ensure that the table entries are dividable by the burst length
    constant C_M_AXI_BURST_LEN              : integer   := gcd(C_TABLE_ROWS*8, floor2pow2(BURST_LEN));
    constant C_M_AXI_ID_WIDTH               : integer   := 1;
    constant C_M_AXI_ADDR_WIDTH             : integer   := 32;
    constant C_M_AXI_DATA_WIDTH             : integer   := 32;
    constant C_M_AXI_AWUSER_WIDTH           : integer   := 1;
    constant C_M_AXI_ARUSER_WIDTH           : integer   := 1;
    constant C_M_AXI_WUSER_WIDTH            : integer   := 1;
    constant C_M_AXI_RUSER_WIDTH            : integer   := 1;
    constant C_M_AXI_BUSER_WIDTH            : integer   := 1;

    constant TABLE_ADDR_WIDTH               : integer   := log2(C_TABLE_ROWS*8*4);

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
    constant C_TRANSACTIONS_NUM             : integer := clogb2(C_M_AXI_BURST_LEN - 1);

    signal axi_aclk                         : std_logic := '0';
    signal axi_aresetn                      : std_logic := '0';

    signal axi_master_table_filled          : std_logic;
    signal axi_master_table_txns_done       : std_logic;
    signal axi_master_read_enable           : std_logic;
    signal axi_master_read_ack              : std_logic;
    signal axi_master_read_start_addr       : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_master_read_len              : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_master_write_enable          : std_logic;
    signal axi_master_write_ack             : std_logic;
    signal axi_master_write_start_addr      : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_master_write_len             : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_master_table_write_enable    : std_logic;
    signal axi_master_table_write_addr      : std_logic_vector(31 downto 0);
    signal axi_master_table_write_data      : std_logic_vector(31 downto 0);
    signal axi_master_txns_done             : std_logic;
    signal axi_master_awid                  : std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
    signal axi_master_awaddr                : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_master_awlen                 : std_logic_vector(7 downto 0);
    signal axi_master_awsize                : std_logic_vector(2 downto 0);
    signal axi_master_awburst               : std_logic_vector(1 downto 0);
    signal axi_master_awlock                : std_logic;
    signal axi_master_awcache               : std_logic_vector(3 downto 0);
    signal axi_master_awprot                : std_logic_vector(2 downto 0);
    signal axi_master_awqos                 : std_logic_vector(3 downto 0);
    signal axi_master_awuser                : std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
    signal axi_master_awvalid               : std_logic;
    signal axi_master_awready               : std_logic;
    signal axi_master_wdata                 : std_logic_vector(31 downto 0);
    signal axi_master_wstrb                 : std_logic_vector((32/8)-1 downto 0);
    signal axi_master_wlast                 : std_logic;
    signal axi_master_wuser                 : std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
    signal axi_master_wvalid                : std_logic;
    signal axi_master_wready                : std_logic;
    signal axi_master_bid                   : std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
    signal axi_master_bresp                 : std_logic_vector(1 downto 0);
    signal axi_master_buser                 : std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
    signal axi_master_bvalid                : std_logic;
    signal axi_master_bready                : std_logic;
    signal axi_master_arid                  : std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
    signal axi_master_araddr                : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_master_arlen                 : std_logic_vector(7 downto 0);
    signal axi_master_arsize                : std_logic_vector(2 downto 0);
    signal axi_master_arburst               : std_logic_vector(1 downto 0);
    signal axi_master_arlock                : std_logic;
    signal axi_master_arcache               : std_logic_vector(3 downto 0);
    signal axi_master_arprot                : std_logic_vector(2 downto 0);
    signal axi_master_arqos                 : std_logic_vector(3 downto 0);
    signal axi_master_aruser                : std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
    signal axi_master_arvalid               : std_logic;
    signal axi_master_arready               : std_logic;
    signal axi_master_rid                   : std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
    signal axi_master_rdata                 : std_logic_vector(31 downto 0);
    signal axi_master_rresp                 : std_logic_vector(1 downto 0);
    signal axi_master_rlast                 : std_logic;
    signal axi_master_ruser                 : std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
    signal axi_master_rvalid                : std_logic;
    signal axi_master_rready                : std_logic;

    signal iotab_row                        : std_logic_vector(28 downto 0);
    signal iotab_config                     : std_logic_vector(31 downto 0);
    signal iotab_img_start_addr             : std_logic_vector(31 downto 0);
    signal iotab_img_width_write_width      : std_logic_vector(31 downto 0);
    signal iotab_partition_start_xy         : std_logic_vector(31 downto 0);
    signal iotab_partition_width_height     : std_logic_vector(31 downto 0);
    signal iotab_write_start_addr           : std_logic_vector(31 downto 0);
    signal iotab_write_start_xy             : std_logic_vector(31 downto 0);
    signal iotab_write_dimensions           : std_logic_vector(31 downto 0);


    type TABLE_TYPE is array(1023 downto 0) of std_logic_vector(31 downto 0);
    signal table : TABLE_TYPE;

    signal table_idx : integer := 0;


    constant ADDR_LSB       : integer := (C_M_AXI_DATA_WIDTH/32) + 1;
    constant low            : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');

    signal axi_araddr       : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_arv_arr_flag : std_logic;
    signal axi_arlen_cntr   : std_logic_vector(7 downto 0);
    signal ar_wrap_size     : integer;
    signal ar_wrap_en       : std_logic;
    signal mem_data_out     : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal read_ack_prev    : std_logic;

begin

    UUT: mastertest_v1_0_AXI_MASTER 
        generic map (
            C_TABLE_ROWS            => C_TABLE_ROWS,
            C_M_AXI_BURST_LEN       => C_M_AXI_BURST_LEN,
            C_M_AXI_ID_WIDTH        => C_M_AXI_ID_WIDTH,
            C_M_AXI_ADDR_WIDTH      => C_M_AXI_ADDR_WIDTH,
            C_M_AXI_DATA_WIDTH      => C_M_AXI_DATA_WIDTH,
            C_M_AXI_AWUSER_WIDTH    => C_M_AXI_AWUSER_WIDTH,
            C_M_AXI_ARUSER_WIDTH    => C_M_AXI_ARUSER_WIDTH,
            C_M_AXI_WUSER_WIDTH     => C_M_AXI_WUSER_WIDTH,
            C_M_AXI_RUSER_WIDTH     => C_M_AXI_RUSER_WIDTH,
            C_M_AXI_BUSER_WIDTH     => C_M_AXI_BUSER_WIDTH
        )
        port map (
            table_filled        => axi_master_table_filled,
            table_txns_done     => axi_master_table_txns_done,
            read_enable         => axi_master_read_enable,
            read_ack            => axi_master_read_ack,
            read_start_addr     => axi_master_read_start_addr,
            read_len            => axi_master_read_len,
            write_enable        => axi_master_write_enable,
            write_ack           => axi_master_write_ack,
            write_start_addr    => axi_master_write_start_addr,
            write_len           => axi_master_write_len,
            table_write_enable  => axi_master_table_write_enable,
            table_write_addr    => axi_master_table_write_addr,
            table_write_data    => axi_master_table_write_data,
            TXNS_DONE           => axi_master_txns_done,
            M_AXI_ACLK          => axi_aclk,
            M_AXI_ARESETN       => axi_aresetn,
            M_AXI_AWID          => axi_master_awid,
            M_AXI_AWADDR        => axi_master_awaddr,
            M_AXI_AWLEN         => axi_master_awlen,
            M_AXI_AWSIZE        => axi_master_awsize,
            M_AXI_AWBURST       => axi_master_awburst,
            M_AXI_AWLOCK        => axi_master_awlock,
            M_AXI_AWCACHE       => axi_master_awcache,
            M_AXI_AWPROT        => axi_master_awprot,
            M_AXI_AWQOS         => axi_master_awqos,
            M_AXI_AWUSER        => axi_master_awuser,
            M_AXI_AWVALID       => axi_master_awvalid,
            M_AXI_AWREADY       => axi_master_awready,
            M_AXI_WDATA         => axi_master_wdata,
            M_AXI_WSTRB         => axi_master_wstrb,
            M_AXI_WLAST         => axi_master_wlast,
            M_AXI_WUSER         => axi_master_wuser,
            M_AXI_WVALID        => axi_master_wvalid,
            M_AXI_WREADY        => axi_master_wready,
            M_AXI_BID           => axi_master_bid,
            M_AXI_BRESP         => axi_master_bresp,
            M_AXI_BUSER         => axi_master_buser,
            M_AXI_BVALID        => axi_master_bvalid,
            M_AXI_BREADY        => axi_master_bready,
            M_AXI_ARID          => axi_master_arid,
            M_AXI_ARADDR        => axi_master_araddr,
            M_AXI_ARLEN         => axi_master_arlen,
            M_AXI_ARSIZE        => axi_master_arsize,
            M_AXI_ARBURST       => axi_master_arburst,
            M_AXI_ARLOCK        => axi_master_arlock,
            M_AXI_ARCACHE       => axi_master_arcache,
            M_AXI_ARPROT        => axi_master_arprot,
            M_AXI_ARQOS         => axi_master_arqos,
            M_AXI_ARUSER        => axi_master_aruser,
            M_AXI_ARVALID       => axi_master_arvalid,
            M_AXI_ARREADY       => axi_master_arready,
            M_AXI_RID           => axi_master_rid,
            M_AXI_RDATA         => axi_master_rdata,
            M_AXI_RRESP         => axi_master_rresp,
            M_AXI_RLAST         => axi_master_rlast,
            M_AXI_RUSER         => axi_master_ruser,
            M_AXI_RVALID        => axi_master_rvalid,
            M_AXI_RREADY        => axi_master_rready
        );

    IO_TAB: inout_table
        generic map (
            C_ROWS  => C_TABLE_ROWS
        )
        port map (
            clk                         => axi_aclk,
            resetn                      => axi_aresetn,
            we                          => axi_master_table_write_enable,
            ADDR_IN                     => axi_master_table_write_addr(TABLE_ADDR_WIDTH-1 downto 0),
            DATA_IN                     => axi_master_table_write_data,
            re                          => '0',
            ADDR_OUT                    => (others => '0'),
            DATA_OUT                    => open,
            ROW                         => iotab_row,
            R_CONFIG                    => iotab_config,            
            R_IMG_START_ADDR            => iotab_img_start_addr,
            R_IMG_WIDTH_WRITE_WIDTH     => iotab_img_width_write_width,
            R_PARTITION_START_XY        => iotab_partition_start_xy,
            R_PARTITION_WIDTH_HEIGHT    => iotab_partition_width_height,
            R_WRITE_START_ADDR          => iotab_write_start_addr,
            R_WRITE_START_XY            => iotab_write_start_xy,
            R_WRITE_DIMENSIONS          => iotab_write_dimensions
        );

    CLK_PROC: process
    begin
        axi_aclk <= not axi_aclk;
        wait for CLK_PERIOD/2;
    end process;

    RESET_PROC: process
    begin
        axi_aresetn <= '0';
        wait for CLK_PERIOD*3;
        axi_aresetn <= '1';
        wait;
    end process;

    INIT_PROC: process(axi_aclk)
        variable i : integer;
    begin
        if rising_edge(axi_aclk) then
            if (axi_aresetn = '0') then
                axi_master_write_enable     <= '0';
                axi_master_write_start_addr <= (others => '0');
                axi_master_write_len        <= (others => '0');
                axi_master_awready          <= '0';
                axi_master_wready           <= '0';
                axi_master_bid              <= (others => '0');
                axi_master_bresp            <= (others => '0');
                axi_master_buser            <= (others => '0');
                axi_master_bvalid           <= '0';
                axi_master_rid              <= (others => '0');
                axi_master_ruser            <= (others => '0');
                
                for i in 0 to 1023 loop
                    table(i) <= (others => '0');
                end loop;

                table(0)  <= std_logic_vector(to_unsigned(1, 32));
                table(1)  <= std_logic_vector(to_unsigned(2, 32));
                table(2)  <= std_logic_vector(to_unsigned(3, 32));
                table(3)  <= std_logic_vector(to_unsigned(4, 32));
                table(4)  <= std_logic_vector(to_unsigned(5, 32));
                table(5)  <= std_logic_vector(to_unsigned(6, 32));
                table(6)  <= std_logic_vector(to_unsigned(3, 32));
                table(7)  <= std_logic_vector(to_unsigned(23, 32));
                table(8)  <= std_logic_vector(to_unsigned(2, 32));
                table(9)  <= std_logic_vector(to_unsigned(3, 32));
                table(10) <= std_logic_vector(to_unsigned(4, 32));
                table(11) <= std_logic_vector(to_unsigned(5, 32));
                table(12) <= std_logic_vector(to_unsigned(6, 32));
                table(13) <= std_logic_vector(to_unsigned(7, 32));
                table(14) <= std_logic_vector(to_unsigned(1, 32));
                table(15) <= std_logic_vector(to_unsigned(12, 32));
                table(16) <= std_logic_vector(to_unsigned(3, 32));
                table(17) <= std_logic_vector(to_unsigned(4, 32));
                table(18) <= std_logic_vector(to_unsigned(5, 32));
                table(19) <= std_logic_vector(to_unsigned(6, 32));
                table(20) <= std_logic_vector(to_unsigned(7, 32));
                table(21) <= std_logic_vector(to_unsigned(8, 32));
                table(22) <= std_logic_vector(to_unsigned(1, 32));
                table(23) <= std_logic_vector(to_unsigned(81, 32));
                table(24) <= std_logic_vector(to_unsigned(4, 32));
                table(25) <= std_logic_vector(to_unsigned(5, 32));
                table(26) <= std_logic_vector(to_unsigned(6, 32));
                table(27) <= std_logic_vector(to_unsigned(7, 32));
                table(28) <= std_logic_vector(to_unsigned(8, 32));
                table(29) <= std_logic_vector(to_unsigned(9, 32));
                table(30) <= std_logic_vector(to_unsigned(1, 32));
            end if;
        end if;
    end process;

    process
    begin
        axi_master_table_txns_done <= '0';
        wait for 1000 ns;
        axi_master_table_txns_done <= '1';
        wait for 10 ns;
        axi_master_table_txns_done <= '0';
        wait for 1000 ns;
        axi_master_table_txns_done <= '1';
        wait for 10 ns;
        axi_master_table_txns_done <= '0';
        wait;
    end process;

    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then
            if axi_aresetn = '0' then
                axi_master_read_enable <= '0';
                axi_master_read_start_addr <= (others => '0');
                axi_master_read_len <= std_logic_vector(to_unsigned(8, 32));
                read_ack_prev <= '1';
            elsif axi_master_read_ack = '1' then
                axi_master_read_enable <= '0';
                axi_master_read_start_addr <= std_logic_vector(unsigned(axi_master_read_start_addr) + to_unsigned(4, 32));
                axi_master_read_len <= std_logic_vector(to_unsigned(8, 32));
                read_ack_prev <= '1';
            elsif read_ack_prev = '1' then
                axi_master_read_enable <= '1';
                read_ack_prev <= '0';
            end if;
        end if;
    end process;

    -- Implement axi_arready generation

    -- axi_arready is asserted for one S_AXI_ACLK clock cycle when
    -- S_AXI_ARVALID is asserted. axi_awready is 
    -- de-asserted when reset (active low) is asserted. 
    -- The read address is also latched when S_AXI_ARVALID is 
    -- asserted. axi_araddr is reset to zero on reset assertion.
    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then 
            if axi_aresetn = '0' then
                axi_master_arready <= '0';
                axi_arv_arr_flag <= '0';
            else
                if (axi_master_arready = '0' and axi_master_arvalid = '1' and axi_arv_arr_flag = '0') then
                    axi_master_arready <= '1';
                    axi_arv_arr_flag <= '1';
                elsif (axi_master_rvalid = '1' and axi_master_rready = '1' and (axi_arlen_cntr = axi_master_arlen)) then 
                -- preparing to accept next address after current read completion
                    axi_arv_arr_flag <= '0';
                else
                    axi_master_arready <= '0';
                end if;
            end if;
        end if;         
    end process;

    ar_wrap_size <= ((C_M_AXI_DATA_WIDTH)/8 * to_integer(unsigned(axi_master_arlen))); 
    ar_wrap_en   <= '1' when (((axi_master_araddr AND std_logic_vector(to_unsigned(ar_wrap_size, C_M_AXI_ADDR_WIDTH))) XOR std_logic_vector(to_unsigned(ar_wrap_size, C_M_AXI_ADDR_WIDTH))) = low) else '0';

    -- Implement axi_araddr latching

    --This process is used to latch the address when both 
    --S_AXI_ARVALID and S_AXI_RVALID are valid. 
    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then 
            if axi_aresetn = '0' then
                axi_araddr <= (others => '0');
                axi_arlen_cntr <= (others => '0');
                axi_master_rlast <= '0';
            else
                if (axi_master_arready = '0' and axi_master_arvalid = '1' and axi_arv_arr_flag = '0') then
                    -- address latching 
                    axi_araddr <= axi_master_araddr(C_M_AXI_ADDR_WIDTH-1 downto 0); ---- start address of transfer
                    axi_arlen_cntr <= (others => '0');
                    axi_master_rlast <= '0';
                elsif ((axi_arlen_cntr < axi_master_arlen) and axi_master_rvalid = '1' and axi_master_rready = '1') then     
                    axi_arlen_cntr <= std_logic_vector(unsigned(axi_arlen_cntr) + 1);
                    axi_master_rlast <= '0';      
             
                    case (axi_master_arburst) is
                        when "00" =>  -- fixed burst
                            -- The read address for all the beats in the transaction are fixed
                            axi_araddr <= axi_araddr;      --for arsize = 4 bytes (010)
                        when "01" =>  --incremental burst
                            -- The read address for all the beats in the transaction are increments by awsize
                            axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB) <= std_logic_vector(unsigned(axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB)) + 1); --araddr aligned to 4 byte boundary
                            axi_araddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
                        when "10" =>  --Wrapping burst
                            -- The read address wraps when the address reaches wrap boundary 
                            if (ar_wrap_en = '1') then   
                                axi_araddr <= std_logic_vector(unsigned(axi_araddr) - (to_unsigned(ar_wrap_size, C_M_AXI_ADDR_WIDTH)));
                            else 
                                axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB) <= std_logic_vector(unsigned(axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB)) + 1); --araddr aligned to 4 byte boundary
                                axi_araddr(ADDR_LSB-1 downto 0)  <= (others => '0');  ----for awsize = 4 bytes (010)
                            end if;
                        when others => --reserved (incremental burst for example)
                            axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB) <= std_logic_vector (unsigned(axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto ADDR_LSB)) + 1);--for arsize = 4 bytes (010)
                            axi_araddr(ADDR_LSB-1 downto 0) <= (others => '0');
                    end case;         
                elsif ((axi_arlen_cntr = axi_master_arlen) and axi_master_rlast = '0' and axi_arv_arr_flag = '1') then
                  axi_master_rlast <= '1';
                elsif (axi_master_rready = '1') then
                  axi_master_rlast <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Implement axi_arvalid generation

    -- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
    -- S_AXI_ARVALID and axi_arready are asserted. The slave registers 
    -- data are available on the axi_rdata bus at this instance. The 
    -- assertion of axi_rvalid marks the validity of read data on the 
    -- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
    -- is deasserted on reset (active low). axi_rresp and axi_rdata are 
    -- cleared to zero on reset (active low).  
    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then
            if axi_aresetn = '0' then
                axi_master_rvalid <= '0';
                axi_master_rresp  <= "00";
            else
                if (axi_arv_arr_flag = '1') then -- and axi_master_rvalid = '0') then
                    axi_master_rvalid <= '1';
                    axi_master_rresp  <= "00"; -- 'OKAY' response
                --elsif (axi_master_rvalid = '1' and axi_master_rready = '1') then
                else
                    axi_master_rvalid <= '0';
                end  if;      
            end if;
        end if;
    end process;

    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then
            if (axi_arv_arr_flag = '1') then 
                mem_data_out <= table(to_integer(unsigned(axi_araddr(C_M_AXI_ADDR_WIDTH-1 downto 2))));
            end if;
        end if;
    end process;

    axi_master_rdata <= mem_data_out when (axi_master_rvalid = '1') else (others => '0');

end bhv;
