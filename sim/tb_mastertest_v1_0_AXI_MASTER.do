vlib work
vcom -novopt -work work faupu_dma_pkg.vhd
vcom -novopt -work work inout_table.vhd
vcom -novopt -work work mastertest_v1_0_AXI_MASTER.vhd
vcom -novopt -work work tb_mastertest_v1_0_AXI_MASTER.vhd

vsim work.tb_mastertest_v1_0_AXI_MASTER

add wave sim:/tb_mastertest_v1_0_AXI_MASTER/axi_aclk
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/axi_aresetn
add wave -hex sim:/tb_mastertest_v1_0_AXI_MASTER/axi_master_*

add wave sim:/tb_mastertest_v1_0_AXI_MASTER/axi_arv_arr_flag
add wave -hex sim:/tb_mastertest_v1_0_AXI_MASTER/axi_araddr
add wave -hex sim:/tb_mastertest_v1_0_AXI_MASTER/mem_data_out
add wave -dec sim:/tb_mastertest_v1_0_AXI_MASTER/uut/table_data_count
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/uut/table_data_valid
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/uut/start_single_burst_read
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/uut/reg_table_filled
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/uut/read_txns_done
add wave sim:/tb_mastertest_v1_0_AXI_MASTER/uut/table_txns_done_prev
add wave -dec sim:/tb_mastertest_v1_0_AXI_MASTER/uut/reg_table_write_addr
add wave -dec sim:/tb_mastertest_v1_0_AXI_MASTER/IO_TAB/table

