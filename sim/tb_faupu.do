vlib work
vcom -novopt -work work ../bram_table.vhd
vcom -novopt -work work ../faupu_dma_pkg.vhd
vcom -novopt -work work ../inout_table.vhd
vcom -novopt -work work ../bram.vhd
vcom -novopt -work work ../multi_buffer.vhd
vcom -novopt -work work ../pea_datamover.vhd
vcom -novopt -work work ../faupu_dma_irq_v1_0_S_AXI_INTR.vhd
vcom -novopt -work work ../faupu_dma_config.vhd
vcom -novopt -work work ../faupu_iotable_fill.vhd
vcom -novopt -work work ../faupu_pea_config.vhd
vcom -novopt -work work ../faupu_datastream.vhd
vcom -novopt -work work ../faupu_axis_to_pea.vhd
vcom -novopt -work work ../faupu_axis_from_pea.vhd
vcom -novopt -work work ../faupu_dma.vhd
vcom -novopt -work work tb_faupu_AXI_FULL_SLAVE.vhd
vcom -novopt -work work tb_faupu_AXI_LITE_MASTER.vhd
vcom -novopt -work work tb_faupu_AXI_LITE_SLAVE.vhd
vcom -novopt -work work tb_faupu.vhd

vsim work.tb_faupu
config wave -signalnamewidth 1
view wave

add wave -noupdate -divider -height 32 GENERAL
add wave -radix hexadecimal sim:/tb_faupu/irq
add wave -radix hexadecimal sim:/tb_faupu/axi_aclk
add wave -radix hexadecimal sim:/tb_faupu/axi_aresetn
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/dbaw
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/dbsl
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/iotable_fill_iotable_filled
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/C_AXI_MASTER_BURST_LEN
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/DATA_BUFFER_ADDR_WIDTH
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/DATA_BUFFER_NUM_BUFFERS
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/DATA_BUFFER_SELECT_LENGTH
#add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/*
#*/

add wave -noupdate -divider -height 32 IRQ
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/faupu_dma_irq_v1_0_S_AXI_INTR_inst/*
#*/

add wave -noupdate -divider -height 32 AXI_LITE
add wave -radix hexadecimal sim:/tb_faupu/dma_config_*

add wave -noupdate -divider -height 32 IOTABLE_FILL_LOCAL
add wave -radix hexadecimal sim:/tb_faupu/iotab_fill_*

add wave -noupdate -divider -height 32 IOTABLE_FILL
#add wave -radix hexadecimal sim:/tb_faupu/iotable_fill_*
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/IOTABLE_FILL_INST/*
#*/

add wave -noupdate -divider -height 32 IOTABLE
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/IO_TAB/*
#*/table

add wave -noupdate -divider -height 32 CONFIG_REGISTERS
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/FAUPU_CONFIG_INST/*
#*/slv_reg*

add wave -noupdate -divider -height 32 DATASTREAM
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/DATASTREAM_INST/*
#*/

#add wave -noupdate -divider -height 32 PEA_DATAMOVER
#add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/PEA_DATAMOVER_INST/*
##*/

add wave -noupdate -divider -height 32 OUTPUT_TABLE
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/DATASTREAM_INST/OUTPUT_TABLE/mem

add wave -noupdate -divider -height 32 AXIS_TO_PEA
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/AXIS_TO_PEA_INST/*
#*/

add wave -noupdate -divider -height 32 AXIS_FROM_PEA
add wave -radix hexadecimal sim:/tb_faupu/faupu_dma_inst/AXIS_FROM_PEA_INST/*
#*/

add wave -noupdate -divider -height 32 PEA_REGS
add wave -radix hexadecimal sim:/tb_faupu/pea_config_*
add wave -radix hexadecimal sim:/tb_faupu/PEA_CONFIG_SLAVE/slv_regs

add wave -noupdate -divider -height 32 MEMORY
add wave -radix hexadecimal sim:/tb_faupu/mem

add wave -noupdate -divider -height 32 WRITE_MULTI_BUFFER
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/*
#*/a_buffer_select_map
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/b_buffer_select_map
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/buf_pe_arr_id
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/buf_length
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/buf_last
add wave -radix hexadecimal -label mem(0) sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/BUFFERS(0)/BUFFER_INST/mem
add wave -radix hexadecimal -label mem(1) sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/BUFFERS(1)/BUFFER_INST/mem
#add wave -radix hexadecimal -label mem(2) sim:/tb_faupu/faupu_dma_inst/WRITE_DATA_BUFFER/BUFFERS(2)/BUFFER_INST/mem

add wave -noupdate -divider -height 32 READ_MULTI_BUFFER
add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/*
#*/a_buffer_select_map
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/b_buffer_select_map
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/buf_pe_arr_id
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/buf_length
#add wave -radix decimal sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/buf_last
add wave -radix hexadecimal -label mem(0) sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/BUFFERS(0)/BUFFER_INST/mem
add wave -radix hexadecimal -label mem(1) sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/BUFFERS(1)/BUFFER_INST/mem
#add wave -radix hexadecimal -label mem(2) sim:/tb_faupu/faupu_dma_inst/READ_DATA_BUFFER/BUFFERS(2)/BUFFER_INST/mem

run 15000 ns
